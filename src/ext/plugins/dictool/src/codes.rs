// ref: RFC 2229

pub const CRLF: &'static str = "\r\n";

// n databases present
pub const CODE_DATABASE_LIST: u32 = 110;
// n strategies available
pub const CODE_STRATEGY_LIST: u32 = 111;
// database information follows
pub const CODE_DATABASE_INFO: u32 = 112;
// help text follows
pub const CODE_HELP: u32 = 113;
// server information follows
pub const CODE_SERVER_INFO: u32 = 114;

// n definitions
pub const CODE_DEFINITIONS_FOUND: u32 = 150;
// word database name
pub const CODE_DEFINITION_FOLLOWS: u32 = 151;
// n matches
pub const CODE_MATCHES_FOUND: u32 = 152;

// status information here
pub const CODE_STATUS: u32 = 210;

// text msg-id
pub const CODE_HELLO: u32 = 220;
// Closing Connection
pub const CODE_GOODBYE: u32 = 221;

// Authentication Successful
pub const CODE_AUTH_OK: u32 = 230;

// ok
pub const CODE_OK: u32 = 250;

// server unavailable
pub const CODE_TEMPORARILY_UNAVAILABLE: u32 = 420;
// server shutting down
pub const CODE_SHUTTING_DOWN: u32 = 421;

// syntax, command not recognized
pub const CODE_SYNTAX_ERROR: u32 = 500;
// syntax, illegal parameters
pub const CODE_ILLEGAL_PARAM: u32 = 501;
// command not implemented
pub const CODE_COMMAND_NOT_IMPLEMENTED: u32 = 502;
// parameter not implemented
pub const CODE_PARAM_NOT_IMPLEMENTED: u32 = 503;

// access denied
pub const CODE_ACCESS_DENIED: u32 = 530;
// authentication denied
pub const CODE_AUTH_DENIED: u32 = 531;
// unknown authentication mechanism
pub const CODE_UNKNOWN_MECH: u32 = 532;

// invalid database
pub const CODE_INVALID_DB: u32 = 550;
// invalid strategy
pub const CODE_INVALID_STRATEGY: u32 = 551;
// no match
pub const CODE_NO_MATCH: u32 = 552;
// no databases
pub const CODE_NO_DATABASES: u32 = 554;
// no strategies
pub const CODE_NO_STRATEGIES: u32 = 555;
