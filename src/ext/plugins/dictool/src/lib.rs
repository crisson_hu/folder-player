pub mod codes;
pub mod asyncnet;
pub mod util;

use util::VERBOSE_DEBUG;
use std::io;
use std::net::TcpStream;
use std::net::{SocketAddr, ToSocketAddrs};
use std::string::String;
use std::ffi::CString;
use std::os::raw::c_char;


fn open_dictd_server() -> io::Result<TcpStream> {
    let mut addrs = vec![SocketAddr::from(([127, 0, 0, 1], 2628))];
    let dict_org = "dict.org:2628";
    if let Ok(mut dict_org) = dict_org.to_socket_addrs() {
        if let Some(dict_org) = dict_org.next() {
            unsafe {
                if VERBOSE_DEBUG {
                    println!("\n------ dict.org ------");
                    println!("{:?}", dict_org);
                }
            }
            addrs.insert(0, dict_org);
        } else {
            println!("\n------ dict.org ------");
            println!("Error: Cannot find next hostname.");
        }
    } else {
        println!("\n------ dict.org ------");
        println!("Error: Cannot resolve hostname.");
    }

    TcpStream::connect(&addrs[..])
}

#[no_mangle]
pub extern "C" fn get_response() -> *mut c_char
{
    let mut s: String = String::new();

    if let Ok(stream) = open_dictd_server() {
        let hello = format!("{}{}", codes::CODE_HELLO, codes::CRLF);
        unsafe {
            if VERBOSE_DEBUG {
                println!("\n------ stream ------");
                println!("{:#?}", stream);

                println!("\n------ write ------");
                println!("{:?}", hello);
            }
        }

        asyncnet::net_write(&stream, hello);
        s = asyncnet::net_read(stream);
        unsafe {
            if VERBOSE_DEBUG {
                println!("\n------ read ------");
                println!("{:?}", s);
            }
        }
    } else {
        println!("Cannot connect to server....");
    }

    CString::new(s).expect("CString::new() failed").into_raw()
}


#[cfg(test)]
mod tests {
    use crate::get_response;
    use crate::util::VERBOSE_DEBUG;
    #[test]
    fn test_response() {
        unsafe {
            VERBOSE_DEBUG = true;
        }
        get_response();
    }
}
