use std::io::prelude::*;
use std::net::TcpStream;


const MAX_TEXT_LINE_OCTETS: u32 = 6144;

pub fn net_read(mut stream: TcpStream) -> String {
    let mut buf = [0; 1];
    let mut v: Vec<u8> = Vec::new();
    let mut count: u32 = 0;

    stream.set_read_timeout(None).unwrap();
    loop {
        if let Ok(_) = stream.read_exact(&mut buf) {
            let c = buf[0];
            if c != '\r' as u8 && c != '\n' as u8 {
                v.push(c);
            } else {
                break;
            }
        } else {
            break;
        }
        if count >= MAX_TEXT_LINE_OCTETS {
            break;
        }
        count += 1;
    }
    let s = String::from_utf8(v).unwrap();
    s
}

pub fn net_write(mut stream: &TcpStream, w: String) {
    stream.write(&w.into_bytes()).unwrap();
    stream.flush().unwrap();
}
