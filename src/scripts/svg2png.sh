#!/bin/sh

set -e

usage () {
    printf "Usage:\n"
    printf "\t$0 ICON-PATH\n"
}

if [ $# -eq 0 ]; then
    echo "Please provide icon's path"
    usage
    exit 1
fi

iconpath=$1

[ -d ${iconpath} ] || (printf "ERROR: invalid path:\n\t${iconpath}\n" ; usage; exit 1)


# inkscape --export-png=play.png --export-background-opacity=0 -z play.svg
# new command:
# inkscape --export-type=png --export-background-opacity=0 -z play.svg
run () {
    for f in `find ${iconpath} -name '*.svg' -print`; do
        nf=`echo $f | sed 's/svg/png/'`
        cmd="inkscape --export-png=${nf} --export-background-opacity=0 -z ${f}"
        printf "${cmd}\n"
        eval ${cmd}
    done
}

run
