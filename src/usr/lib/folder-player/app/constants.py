class Constants:

    class UiView:
        NORMAL = 1
        VIDEO = 2
        RUN_DICT = 3
        OTHER = 500

    class PlayerState:
        PLAYING = 0
        PAUSED = 1
        STOPPED = 2

    class PlaySched:
        REPEAT_ONCE = 0
        REPEAT_LOOP = 1

        SELECT_CURRENT = 0
        SELECT_ALL = 1

    class Font:

        FONT_SIZE_QUICK_VIEW = 11

    class FileSuffix:

        IMAGES_CAIRO = ['png']
        IMAGES_PIXBUF = ['gif', 'jpg', 'jpeg', 'svg', 'ico']
        IMAGES = IMAGES_CAIRO + IMAGES_PIXBUF
        MUSICS = ['m4a', 'mp3', 'oga', 'ogg', 'wav']
        VIDEOS = ['mp4', 'wmv', 'rm', 'webm', 'mpg', 'mpeg']
