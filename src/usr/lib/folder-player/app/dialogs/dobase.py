from gi.repository import Gdk, Gtk

from tool import log_debug, HotKey
from base import DialogOverlay
from app.icons import IconButton


class DialogOverlayBase(DialogOverlay):

    __gtype_name__ = 'DialogOverlayBase'

    scale_factor = 0.38

    def __init__(self, *args, use_textview=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._use_textview = use_textview
        self._setup_widgets()
        return

    def _setup_widgets(self):
        self.set_property('halign', Gtk.Align.CENTER)
        self.set_property('valign', Gtk.Align.START)
        margin = 6
        self.set_margin_top(margin)
        self.set_margin_end(margin)
        self.set_margin_bottom(margin)
        self.set_margin_start(margin)

        self.title_hb = Gtk.HBox()
        self.add(self.title_hb)

        self._label_ttl = Gtk.Label('')
        self._label_ttl.set_property('margin-right', 15)
        self.title_hb.pack_start(self._label_ttl, False, False, 0)

        self.title_sw = Gtk.ScrolledWindow()
        self.title_hb.pack_start(self.title_sw, False, False, 0)
        self._label_fn = Gtk.Label('....')
        self.title_sw.add(self._label_fn)
        self._label_fn.set_no_show_all(True)

        self._single_button = IconButton().set_icon('dialog/confirm.png')
        self.title_hb.pack_end(self._single_button, False, False, 0)
        self.title_hb.child_set_property(self._single_button, 'expand', False)
        self._single_button.connect('primary-single-click', self.do_action)
        self._single_button.set_property('margin-left', 30)
        self._single_button.set_no_show_all(True)

        self._hbox = Gtk.HBox()
        self.add(self._hbox)

        if self._use_textview:
            self.sw = Gtk.ScrolledWindow()
            self._hbox.add(self.sw)
            self._entry = Gtk.TextView()
            self._entry.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
            self.sw.add(self._entry)
        else:
            self._entry = Gtk.Entry()
            self._hbox.add(self._entry)
        self._entry.connect('key-press-event', self._on_key_press)

        self._cancel = IconButton().set_icon('dialog/cancel.png')
        self._hbox.add(self._cancel)
        self._hbox.child_set_property(self._cancel, 'expand', False)
        self._cancel.connect('primary-single-click', self._close)

        self._confirm = IconButton().set_icon('dialog/confirm.png')
        self._hbox.add(self._confirm)
        self._hbox.child_set_property(self._confirm, 'expand', False)
        self._confirm.connect('primary-single-click', self.do_action)

        self.connect('key-press-event', self._on_key_press)
        self.show_all()
        return self

    def set_title(self, title, filename=None):
        self._label_ttl.set_text('{}{}'.format(
            title,
            ': ' if filename else ''
        ))
        if filename:
            self._label_fn.set_no_show_all(False)
            self._label_fn.set_visible(True)
            self._label_fn.set_text(filename)
        return

    def set_entry_text(self, text, longinfo=False):
        if self._use_textview:
            self._entry.get_buffer().set_text(text)
            if longinfo:
                self.sw.set_property('height-request', 370)
                self.sw.set_policy(
                    Gtk.PolicyType.ALWAYS,
                    Gtk.PolicyType.ALWAYS
                )
            else:
                self.sw.set_policy(
                    Gtk.PolicyType.NEVER,
                    Gtk.PolicyType.NEVER
                )
        else:
            self._entry.set_text(text)
        self._entry.grab_focus()
        return self

    def disable_entry(self):
        self._entry.set_visible(False)

        try:
            self._entry.disconnect_by_func(self._on_key_press)
        except TypeError as e:
            pass
        except Exception as e:
            log_debug('unknown error')
        finally:
            pass

        w = self._mainwindow.get_size()[0] * self.scale_factor
        self._hbox.child_set_property(self._cancel, 'padding', w / 4)
        self._hbox.child_set_property(self._confirm, 'padding', w / 4)
        self.grab_focus()
        return self

    def set_action(self, action, single_button=False):
        self._action = action
        if single_button:
            self._cancel.set_no_show_all(True)
            self._cancel.set_visible(False)
            self._confirm.set_no_show_all(True)
            self._confirm.set_visible(False)
            self._single_button.set_no_show_all(False)
            self._single_button.set_visible(True)
        return

    def do_action(self, *args):
        return self._action(*args)

    def grab_focus(self):
        if self._entry.get_visible():
            self._entry.grab_focus()
        else:
            self._cancel.set_can_focus(True)
            self._cancel.grab_focus()
        return True

    def set_pathitem(self, pathitem=None):
        if pathitem is None:
            return
        self._pathitem = pathitem
        return self

    def set_mainview(self, mainview):
        self._mainview = mainview
        self.set_mainwindow(mainview.mainwin if mainview else None)
        w = self._mainwindow.get_size()[0] * self.scale_factor
        self.title_hb.set_property('width-request', w)
        self.title_sw.set_property('width-request', w)
        return self

    def scale_window(self, factor):
        w = self._mainwindow.get_size()[0] * factor
        self.title_hb.set_property('width-request', w)
        self.title_sw.set_property('width-request', w)
        return

    def set_mainwindow(self, mainwindow):
        '''If not calling set_mainview(), call set_mainwindow() instead.'''
        self._mainwindow = mainwindow
        self._mainwindow.mainwin_ui_view_folder.popup_vbox_set_view(self)
        return self

    def _on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        # keys
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        # log_debug(msg)

        if kv == Gdk.KEY_Escape:
            self._close()
            return True
        if kv in (Gdk.KEY_Return, Gdk.KEY_ISO_Enter, Gdk.KEY_KP_Enter,):
            if self._pathitem is not None:
                self.do_action()
                return True
        return False

    def _close(self, *args):
        self._mainwindow.mainwin_ui_view_folder.popup_vbox_set_view()
        return True
