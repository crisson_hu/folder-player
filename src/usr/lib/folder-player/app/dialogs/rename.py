from pathlib import Path
from gi.repository import Gtk, Gdk

from tool import _, log_debug
from .dobase import DialogOverlayBase


class DialogRename(DialogOverlayBase):

    __gtype_name__ = 'DialogRename'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title(_('Rename'))
        self.set_action(self._do_rename)
        return

    def set_filelistitem(self, filelistitem=None):
        self._filelistitem = filelistitem
        return self

    def _do_rename(self, *args):
        import os
        target_str = self._entry.get_text()
        target = Path(target_str)
        if not target_str.startswith(os.path.sep):
            target = self._pathitem.parent / target
        self._pathitem.rename(target)

        if target_str.find(os.path.sep) > -1:
            self._mainview.list_directory()
        else:
            self._filelistitem.set_display_text(target)

        self._close()
        return True

    def show(self):
        super().show()
        self.set_entry_text(self._pathitem.name)
        return
