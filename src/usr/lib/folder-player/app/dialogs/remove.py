from pathlib import Path
from gi.repository import Gtk, Gdk
import os
import stat
import shutil

from tool import _
from .dobase import DialogOverlayBase


class DialogRemove(DialogOverlayBase):

    __gtype_name__ = 'DialogRemove'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_action(self._do_remove)
        return

    def _do_remove(self, *args):
        try:
            if self._pathitem.is_dir():
                # self._pathitem.rmdir()
                shutil.rmtree(str(self._pathitem), onerror=_remove_readonly)
            else:
                self._pathitem.unlink()
        except (PermissionError,):
            pass
        else:
            self._container.remove(self._filelistitem)

        self._close()
        return True

    def set_container(self, container):
        self._container = container
        return self

    def set_filelistitem(self, filelistitem):
        self._filelistitem = filelistitem
        return self

    def show(self):
        super().show()
        self.set_title(_('Remove'), filename=self._pathitem.name)
        self.disable_entry()
        return


def _remove_readonly(func, path, _):
    "Clear the readonly bit and reattempt the removal"
    os.chmod(path, stat.S_IWRITE)
    func(path)
