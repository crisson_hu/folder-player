from pathlib import Path
from gi.repository import Gtk, Gdk

from tool import _, log_debug
from .dobase import DialogOverlayBase


class DialogRFC(DialogOverlayBase):

    __gtype_name__ = 'DialogRFC'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title(_('RFC Url'))
        self.set_action(self.confirm, single_button=True)
        return

    def show(self):
        super().show()
        url = 'https://www.ietf.org/rfc/rfcXXXX.txt'
        self.set_entry_text(url)
        return

    def confirm(self, *args):
        self._close()
        return True
