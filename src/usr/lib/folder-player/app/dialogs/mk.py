from pathlib import Path
from gi.repository import Gtk, Gdk

from tool import _
from .dobase import DialogOverlayBase


class DialogMkdir(DialogOverlayBase):

    __gtype_name__ = 'DialogMkdir'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_action(self._do_mkdir)
        self.set_title(_('Create Folder'))
        return

    def _do_mkdir(self, *args):
        (self._pathitem / self._entry.get_text()) \
            .mkdir(parents=True, exist_ok=True)
        self._mainview.list_directory()
        self._close()
        return True


class DialogMkfile(DialogOverlayBase):

    __gtype_name__ = 'DialogMkfile'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_action(self._do_mkfile)
        self.set_title(_('Create Empty File'))
        return

    def _do_mkfile(self, *args):
        name = str(self._pathitem / self._entry.get_text())
        with open(name, 'w+') as fd:
            fd.write('')
        self._mainview.list_directory()
        self._close()
        return True
