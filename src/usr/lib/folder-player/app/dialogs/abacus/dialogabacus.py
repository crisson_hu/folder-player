from gi.repository import Gtk, Gdk, Poppler, Pango, PangoCairo

from tool import _, log_debug, HotKey
from base import DialogBase
from .hetuluoshu import HetuLuoshu, HETU, LUOSHU
from .yellowemperorcalendar import YellowEmperorCalendar


TITLE_HETULUOSHU = '{}{}'.format(HETU, LUOSHU)
TITLE_ABACUS = _('Abacus')
TITLE_CONSTELATION = _('Constellation')
TITLE_CALENDAR = _('Calendar')
ABACUS_TITLE = '{}/{}/{}/{}'.format(
    TITLE_HETULUOSHU,
    TITLE_ABACUS,
    TITLE_CONSTELATION,
    TITLE_CALENDAR,
)


class DialogAbacus(DialogBase):

    __gtype_name__ = 'DialogAbacus'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title(ABACUS_TITLE)
        self.set_decorated(True)
        self.connect('key-press-event', self._on_key_press)
        self._hetuluoshu = HetuLuoshu().set_widget(self.draw_area)
        self._yecalendar = YellowEmperorCalendar().set_widget(self.draw_area)
        self._setup_widgets()
        return

    def do_draw(self, cr):
        self.ui.draw(cr)
        return True

    @property
    def current_title(self):
        if not hasattr(self, '_current_title'):
            self._current_title = TITLE_HETULUOSHU
        return self._current_title

    @current_title.setter
    def current_title(self, title):
        self._current_title = title
        return

    @property
    def ui(self):
        if not hasattr(self, '_ui'):
            self._ui = self._hetuluoshu
        return self._ui

    @ui.setter
    def ui(self, newui):
        self._ui = newui
        self._ui.switch_cb()
        self.queue_draw()
        return

    @property
    def draw_area(self):
        if not hasattr(self, '_draw_area'):
            self._draw_area = self
        return self._draw_area

    def do_button_press_event(self, ev):
        self.show_menu(ev.x_root, ev.y_root)
        return

    def show_menu(self, x=-1, y=-1):
        if not hasattr(self, '_menu'):
            self._menu = Gtk.Menu()
            for t in (
                    '{}{}'.format(HETU, LUOSHU),
                    TITLE_ABACUS,
                    TITLE_CONSTELATION,
                    TITLE_CALENDAR,
            ):
                mi = Gtk.MenuItem.new_with_label(t)
                mi.show()
                mi.connect('activate', self._do_switch, t)
                self._menu.add(mi)

        if x == -1 or y == -1:
            log_debug(('Error', x, y))
            return False

        self._menu.popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (
                x + 40,
                y - int(self._menu.get_allocated_height() / 3),
                False
            ),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )

        return True

    def _do_switch(self, wdt, title):
        self.current_title = title
        if self.current_title == TITLE_HETULUOSHU:
            self.ui = self._hetuluoshu
        elif self.current_title == TITLE_CALENDAR:
            self.ui = self._yecalendar
        elif self.current_title == TITLE_ABACUS:
            self.ui = self._hetuluoshu
        elif self.current_title == TITLE_CONSTELATION:
            self.ui = self._hetuluoshu
        else:
            self.ui = self._yecalendar
        return True

    def _setup_widgets(self):
        screen = self.get_screen()
        w = screen.get_width()
        h = screen.get_height()
        self.resize(0.7 * w, 0.7 * h)
        return

    def _on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        if kv == Gdk.KEY_Escape:
            self._close()
            return True
        elif k_ctrl and kv in (Gdk.KEY_w, Gdk.KEY_W,):
            self._close()
            return True
        return False

    def _close(self, *args):
        self.close()
        return True
