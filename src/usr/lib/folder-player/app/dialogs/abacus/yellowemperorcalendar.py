import datetime
import calendar
import cairo
from gi.repository import Gtk, Gdk, Pango, PangoCairo

from tool import _, log_debug
from .drawgeometry import DrawGeometry


class YellowEmperorCalendar:

    __gtype_name__ = 'YellowEmperorCalendar'

    def __init__(self, *args, **kwargs):
        return

    def set_widget(self, widget):
        self._widget = widget
        self._geo = DrawGeometry(self._widget)
        return self

    @property
    def font_fmt(self):
        if not hasattr(self, '_font_fmt'):
            self._font_fmt = 'Monospace, Arial'
        return self._font_fmt

    @property
    def pgl(self):
        if not hasattr(self, '_pgl'):
            self._pgl = self._widget.create_pango_layout()
            text = '{}\n\n{}'.format(
                datetime.datetime.now().ctime(),
                self.calendar_year,
            )
            self._pgl.set_text(text, -1)
            desc = Pango.font_description_from_string(self.font_fmt)
            self._pgl.set_font_description(desc)
        return self._pgl

    @property
    def calendar_year(self):
        if not hasattr(self, '_calyear'):
            self._calyear = calendar.TextCalendar(firstweekday=6).formatyear(
                datetime.datetime.now().year, m=4
            )
        return self._calyear

    def draw(self, cr):
        # Default cairo settings.
        cr.set_line_width(1)

        self._geo.update()
        self._draw_bg(cr)
        self._draw_text(cr)
        return

    @property
    def _rgba(self):
        if not hasattr(self, '_rgba'):
            self._rgba = Gdk.RGBA(0x2e/0xff, 0x2e/0xff, 0x2e/0xff, 1)
            self._rgba = Gdk.RGBA(0x5d/0xff, 0x5d/0xff, 0x5d/0xff, 1)
            self._rgba = Gdk.RGBA(0x68/0xff, 0x83/0xff, 0x8b/0xff, 1)
        return self._rgba

    @property
    def border_rgba(self):
        if not hasattr(self, '_border_rgba'):
            self._border_rgba = Gdk.RGBA(0x5d/0xff, 0x5d/0xff, 0x5d/0xff, 1)
            self._border_rgba = Gdk.RGBA(0x68/0xff, 0x83/0xff, 0x8b/0xff, 1)
        return self._border_rgba

    @property
    def bg_rgba(self):
        if not hasattr(self, '_bg_rgba'):
            self._bg_rgba = Gdk.RGBA(0xfa/0xff, 0xfa/0xff, 0xff/0xff, 1)
            self._bg_rgba = Gdk.RGBA(0xff/0xff, 0xff/0xff, 0xfa/0xff, 1)
        return self._bg_rgba

    def _draw_bg(self, cr):
        cr.save()
        cr.rectangle(0, 0, self._geo.w, self._geo.h)
        Gdk.cairo_set_source_rgba(cr, self.bg_rgba)
        cr.fill()
        cr.restore()
        return

    def switch_cb(self):
        w = self._geo.w
        h = self._geo.h
        (lw, lh) = self.pgl.get_pixel_size()
        lw *= 1.1
        lh *= 1.1
        if lw > w or lh > h:
            self._widget.get_window().resize(
                lw if lw > w else w,
                lh if lh > h else h
            )
        return True

    def _draw_text(self, cr):
        w = self._geo.w
        h = self._geo.h
        (lw, lh) = self.pgl.get_pixel_size()
        ctx = self.pgl.get_context()
        ctx.set_base_gravity(Pango.Gravity.AUTO)

        cr.save()
        x = w / 2 - lw / 2
        y = h / 2 - lh / 2
        cr.move_to(x, y)
        PangoCairo.show_layout(cr, self.pgl)
        cr.restore()
        return
