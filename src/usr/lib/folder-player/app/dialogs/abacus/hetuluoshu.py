import cairo
from gi.repository import Gtk, Gdk, Pango, PangoCairo

from tool import _, log_debug
from .drawgeometry import DrawGeometry


HETU = _('HeTu')
HETU = '河圖'
LUOSHU = _('LuoShu')
LUOSHU = '洛書'


class HetuLuoshu:

    __gtype_name__ = 'HetuLuoshu'

    def __init__(self, *args, **kwargs):
        return

    def set_widget(self, widget):
        self._widget = widget
        self._geo = DrawGeometry(self._widget)
        return self

    @property
    def font_fmt(self):
        if not hasattr(self, '_font_fmt'):
            self._font_fmt = 'Arial 50'
        return self._font_fmt

    @property
    def pgl_hetu(self):
        if not hasattr(self, '_pgl_hetu'):
            self._pgl_hetu = self._widget.create_pango_layout()
            self._pgl_hetu.set_text(HETU, -1)
            desc = Pango.font_description_from_string(self.font_fmt)
            self._pgl_hetu.set_font_description(desc)
        return self._pgl_hetu

    @property
    def pgl_luoshu(self):
        if not hasattr(self, '_pgl_luoshu'):
            self._pgl_luoshu = self._widget.create_pango_layout()
            self._pgl_luoshu.set_text(LUOSHU, -1)
            desc = Pango.font_description_from_string(self.font_fmt)
            self._pgl_luoshu.set_font_description(desc)
        return self._pgl_luoshu

    def switch_cb(self):
        return True

    def draw(self, cr):
        # Default cairo settings.
        cr.set_line_width(1)

        self._geo.update()
        self._draw_bg(cr)
        self._draw_text(cr)
        self._draw_hetu(cr)
        self._draw_luoshu(cr)
        return

    @property
    def hetu_rgba(self):
        if not hasattr(self, '_hetu_rgba'):
            rgba = Gdk.RGBA(0x2e/0xff, 0x2e/0xff, 0x2e/0xff, 1)
            rgba = Gdk.RGBA(0x84/0xff, 0x70/0xff, 0xff/0xff, 1)
            rgba = Gdk.RGBA(0x94/0xff, 0x00/0xff, 0xd3/0xff, 1)
            self._hetu_rgba = rgba
        return self._hetu_rgba

    @property
    def luoshu_rgba(self):
        if not hasattr(self, '_luoshu_rgba'):
            rgba = Gdk.RGBA(0x2e/0xff, 0x2e/0xff, 0x2e/0xff, 1)
            rgba = Gdk.RGBA(0xee/0xff, 0xdd/0xff, 0x82/0xff, 1)
            rgba = Gdk.RGBA(0xda/0xff, 0xa5/0xff, 0x20/0xff, 1)
            self._luoshu_rgba = rgba
        return self._luoshu_rgba

    @property
    def border_rgba(self):
        if not hasattr(self, '_border_rgba'):
            self._border_rgba = Gdk.RGBA(0x5d/0xff, 0x5d/0xff, 0x5d/0xff, 1)
            self._border_rgba = Gdk.RGBA(0x68/0xff, 0x83/0xff, 0x8b/0xff, 1)
        return self._border_rgba

    @property
    def bg_rgba(self):
        if not hasattr(self, '_bg_rgba'):
            self._bg_rgba = Gdk.RGBA(0xff/0xff, 0xff/0xff, 0xfa/0xff, 1)
            self._bg_rgba = Gdk.RGBA(0xfa/0xff, 0xfa/0xff, 0xff/0xff, 1)
        return self._bg_rgba

    def _draw_bg(self, cr):
        cr.save()
        cr.rectangle(0, 0, self._geo.w, self._geo.h)
        Gdk.cairo_set_source_rgba(cr, self.bg_rgba)
        cr.fill()
        cr.restore()
        return

    def _draw_text(self, cr):
        ctx = self.pgl_hetu.get_context()
        ctx.set_base_gravity(Pango.Gravity.EAST)
        angle = 1.571
        (lw, lh) = self.pgl_hetu.get_pixel_size()
        x = self._geo.w/4
        y = 20 + lw / 2
        cr.save()
        cr.translate(x, y)
        cr.move_to(lh / 2, - lw / 2)
        mtx = cr.get_matrix()
        mtx.rotate(angle)
        cr.set_matrix(mtx)
        Gdk.cairo_set_source_rgba(cr, self.hetu_rgba)
        PangoCairo.show_layout(cr, self.pgl_hetu)
        cr.restore()

        (lw, lh) = self.pgl_luoshu.get_pixel_size()
        x = self._geo.w * 3 / 4
        y = 20 + lw / 2
        cr.save()
        cr.translate(x, y)
        cr.move_to(lh / 2, - lw / 2)
        mtx = cr.get_matrix()
        mtx.rotate(angle)
        cr.set_matrix(mtx)
        Gdk.cairo_set_source_rgba(cr, self.luoshu_rgba)
        PangoCairo.show_layout(cr, self.pgl_luoshu)
        cr.restore()
        return

    def _d_circle(self, cr, rx, ry, r):
        self._d_dot(cr, rx, ry, r)
        self._d_dot(cr, rx, ry, r / 2, bg=True)
        return

    def _d_dot(self, cr, rx, ry, r, bg=False):
        cr.save()
        if bg:
            Gdk.cairo_set_source_rgba(cr, self.bg_rgba)
        cr.set_line_width(1)
        cr.arc(rx, ry, r, 0, 7)
        cr.stroke()
        cr.arc(rx, ry, r, 0, 7)
        cr.fill()
        cr.restore()
        return

    def _calc_border(self):
        x = 20
        lw, lh = self.pgl_hetu.get_pixel_size()
        y = 20 + lw + 20
        w = self._geo.w / 2 - 20 * 2
        h = self._geo.h - y - 20
        rx = x + w / 2
        ry = y + h / 2
        radius = w / 2 if w < h else h / 2
        return (x, y, w, h, rx, ry, radius)

    def _draw_border(self, cr, x, y, w, h, radius):
        cr.save()
        Gdk.cairo_set_source_rgba(cr, self.border_rgba)
        # debug
        # cr.set_dash([1, 5, 3, 5, 1, 15, 65, 15], 17)
        cr.set_line_width(1)
        cr.set_line_cap(cairo.LINE_CAP_ROUND)
        cr.set_line_join(cairo.LINE_JOIN_ROUND)
        cr.move_to(x, y)
        cr.rectangle(x, y, w, h)
        cr.stroke()

        cr.set_dash([1, 10], 0)
        cr.set_line_width(2)
        cr.set_line_cap(cairo.LINE_CAP_ROUND)
        cr.set_line_join(cairo.LINE_JOIN_ROUND)
        # debug
        # cr.arc(x + w / 2, y + h / 2, radius, 0, 7)
        cr.stroke()
        cr.restore()
        return

    def _draw_hetu(self, cr):
        (x, y, w, h, rx, ry, radius) = self._calc_border()
        self._draw_border(cr, x, y, w, h, radius)

        cr.save()
        # Gdk.cairo_set_source_rgba(cr, self.hetu_rgba)
        self._draw_hetu_4(cr, rx + radius, ry)
        self._draw_hetu_9(cr, rx + radius, ry)
        self._draw_hetu_3(cr, rx - radius, ry)
        self._draw_hetu_8(cr, rx - radius, ry)
        self._draw_hetu_2(cr, rx, ry - radius)
        self._draw_hetu_7(cr, rx, ry - radius)
        self._draw_hetu_1(cr, rx, ry + radius)
        self._draw_hetu_6(cr, rx, ry + radius)
        self._draw_5(cr, rx, ry)
        self._draw_hetu_10(cr, rx, ry)
        cr.restore()
        return

    def _draw_luoshu(self, cr):
        (x, y, w, h, rx, ry, radius) = self._calc_border()
        x = self._geo.w / 2 + x
        rx = self._geo.w / 2 + rx
        self._draw_border(cr, x, y, w, h, radius)

        cr.save()
        # Gdk.cairo_set_source_rgba(cr, self.luoshu_rgba)
        self._draw_luoshu_4(cr, rx - 0.71 * radius, ry - 0.71 * radius)
        self._draw_luoshu_8(cr, rx - 0.71 * radius, ry + 0.71 * radius)
        self._draw_luoshu_2(cr, rx + 0.71 * radius, ry - 0.71 * radius)
        self._draw_luoshu_6(cr, rx + 0.71 * radius, ry + 0.71 * radius)
        self._draw_luoshu_9(cr, rx, ry - radius)
        self._draw_luoshu_3(cr, rx - radius, ry)
        self._draw_luoshu_7(cr, rx + radius, ry)
        self._draw_luoshu_1(cr, rx, ry + radius)
        self._draw_5(cr, rx, ry)
        cr.restore()
        return

    def _draw_luoshu_4(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border / 2
        cr.save()
        cr.translate(x + 1.41 * border, y + 1.41 * border)
        cr.rotate(0.7854)
        cr.rectangle(- 0.5 * gap, - 0.5 * gap, gap, gap)
        cr.stroke()
        for i in range(2):
            self._d_dot(cr, - 0.5 * gap + i * gap, - 0.5 * gap, r)
            self._d_dot(cr, - 0.5 * gap + i * gap, 0.5 * gap, r)
        cr.restore()
        return

    def _draw_luoshu_8(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border / 2
        cr.save()
        cr.translate(x + 1.41 * border, y - 1.41 * border)
        cr.rotate(-0.7854)
        cr.rectangle(- 1.5 * gap, - 0.5 * gap, 3 * gap, gap)
        cr.stroke()
        for i in range(4):
            self._d_dot(cr, - 1.5 * gap + i * gap, - 0.5 * gap, r)
            self._d_dot(cr, - 1.5 * gap + i * gap, 0.5 * gap, r)
        cr.restore()
        return

    def _draw_luoshu_2(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border / 2
        cr.save()
        cr.translate(x - 1.41 * border, y + 1.41 * border)
        cr.rotate(-0.7854)
        cr.move_to(- 0.5 * gap, 0)
        cr.rel_line_to(gap, 0)
        cr.stroke()
        for i in range(2):
            self._d_dot(cr, - 0.5 * gap + i * gap, 0, r)
        cr.restore()
        return

    def _draw_luoshu_6(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border / 2
        cr.save()
        cr.translate(x - 1.41 * border, y - 1.41 * border)
        cr.rotate(0.7854)
        cr.rectangle(- gap, - 0.5 * gap, 2 * gap, gap)
        cr.stroke()
        for i in range(3):
            self._d_dot(cr, - gap + i * gap, - 0.5 * gap, r)
            self._d_dot(cr, - gap + i * gap, 0.5 * gap, r)
        cr.restore()
        return

    def _draw_luoshu_9(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x, y + border)
        cr.move_to(-4 * gap, 0)
        cr.rel_line_to(8 * gap, 0)
        cr.stroke()
        for i in range(9):
            self._d_circle(cr, - 4 * gap + i * gap, 0, r)
        cr.restore()
        return

    def _draw_luoshu_7(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x - border, y)
        cr.move_to(0, - 3 * gap)
        cr.rel_line_to(0, 6 * gap)
        cr.stroke()
        for i in range(7):
            self._d_circle(cr, 0, - 3 * gap + i * gap, r)
        cr.restore()
        return

    def _draw_luoshu_3(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x + border, y)
        cr.move_to(0, - gap)
        cr.rel_line_to(0, 2 * gap)
        cr.stroke()
        for i in range(3):
            self._d_circle(cr, 0, - gap + i * gap, r)
        cr.restore()
        return

    def _draw_luoshu_1(self, cr, x, y):
        r = self._geo.r_dot
        border = self._geo.r_border
        cr.save()
        self._d_circle(cr, x, y - border, r)
        cr.restore()
        return

    def _draw_hetu_4(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x - 2 * border, y)
        cr.move_to(0, - 1.5 * gap)
        cr.rel_line_to(0, 3 * gap)
        cr.stroke()
        for i in range(4):
            self._d_dot(cr, 0, - 1.5 * gap + i * gap, r)
        cr.restore()
        return

    def _draw_hetu_9(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x - border, y)
        cr.move_to(0, - 4 * gap)
        cr.rel_line_to(0, 8 * gap)
        cr.stroke()
        for i in range(9):
            self._d_circle(cr, 0, - 4 * gap + i * gap, r)
        cr.restore()
        return

    def _draw_hetu_3(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x + 2 * border, y)
        cr.move_to(0, - gap)
        cr.rel_line_to(0, 2 * gap)
        cr.stroke()
        for i in range(3):
            self._d_circle(cr, 0, - gap + i * gap, r)
        cr.restore()
        return

    def _draw_hetu_8(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x + border, y)
        cr.move_to(0, - 3.5 * gap)
        cr.rel_line_to(0, 7 * gap)
        cr.stroke()
        for i in range(8):
            self._d_dot(cr, 0, - 3.5 * gap + i * gap, r)
        cr.restore()
        return

    def _draw_hetu_2(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x, y + 2 * border)
        cr.move_to(- 0.5 * gap, 0)
        cr.rel_line_to(gap, 0)
        cr.stroke()
        for i in range(2):
            self._d_dot(cr, - 0.5 * gap + i * gap, 0, r)
        cr.restore()
        return

    def _draw_hetu_7(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x, y + border)
        cr.move_to(- 3 * gap, 0)
        cr.rel_line_to(6 * gap, 0)
        cr.stroke()
        for i in range(7):
            self._d_circle(cr, - 3 * gap + i * gap, 0, r)
        cr.restore()
        return

    def _draw_hetu_1(self, cr, x, y):
        r = self._geo.r_dot
        border = self._geo.r_border
        cr.save()
        self._d_circle(cr, x, y - 2 * border, r)
        cr.restore()
        return

    def _draw_hetu_6(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        cr.translate(x, y - border)
        cr.move_to(- 2.5 * gap, 0)
        cr.rel_line_to(5 * gap, 0)
        cr.stroke()
        for i in range(6):
            self._d_dot(cr, - 2.5 * gap + i * gap, 0, r)
        cr.restore()
        return

    def _draw_5(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        border = self._geo.r_border
        cr.save()
        # draw line
        cr.translate(x, y)
        cr.move_to(- gap, 0)
        cr.rel_line_to(2 * gap, 0)
        cr.move_to(0, - gap)
        cr.rel_line_to(0, 2 * gap)
        cr.stroke()
        # draw circle
        self._d_circle(cr, - gap, 0, r)
        self._d_circle(cr, 0, 0, r)
        self._d_circle(cr, gap, 0, r)
        self._d_circle(cr, 0, - gap, r)
        self._d_circle(cr, 0, gap, r)
        cr.restore()
        return

    def _draw_hetu_10(self, cr, x, y):
        r = self._geo.r_dot
        gap = self._geo.r_gap
        # border = self._geo.r_border
        border = self._geo.r_gap + r
        cr.save()
        cr.rectangle(x - 2 * gap, y - 2 * border, 4 * gap, 4 * border)
        cr.stroke()
        cr.restore()
        for i in range(5):
            self._d_dot(cr, x - 2 * gap + i * gap, y - 2 * border, r)
            self._d_dot(cr, x - 2 * gap + i * gap, y + 2 * border, r)
        return
