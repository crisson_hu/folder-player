class DrawGeometry:

    def __init__(self, widget):
        self._widget = widget
        self.update()
        return

    def update(self):
        self._width = self._widget.get_allocated_width()
        self._height = self._widget.get_allocated_height()

        r = self._width / 2 if self._width / 2 < self._height else self._height
        self._r_border = (r - 40) / 12
        if self._r_border < 20:
            self._r_border = 20
        self._r_gap = self._r_border / 2
        self._r_dot = self._r_gap / 3

        return self

    @property
    def w(self):
        return self._width

    @property
    def h(self):
        return self._height

    @property
    def r_border(self):
        return self._r_border

    @property
    def r_gap(self):
        return self._r_gap

    @property
    def r_dot(self):
        return self._r_dot
