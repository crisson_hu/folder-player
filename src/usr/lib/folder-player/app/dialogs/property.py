from gi.repository import Gtk, Pango, Poppler

from tool import (
    _,
    log_debug,
    FileProp,
    FilePropPdf,
    FilePropZip,
    FilePropMedia,
)
from .dobase import DialogOverlayBase


class DialogProperty(DialogOverlayBase):

    __gtype_name__ = 'DialogProperty'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, use_textview=True, **kwargs)
        self.set_action(self._close, single_button=True)
        self.txt_prop = ''
        self.txt_prop_media = ''
        return

    def show(self):
        self.set_title(_('Property'), filename=self._pathitem.name)
        self.txt_prop = '{}'.format(FileProp.get_stat(self._pathitem))
        furi = 'file://{}'.format(self._pathitem)
        longinfo = False
        if self._pathitem.name.lower().endswith('pdf'):
            longinfo = True
            pdffile = FilePropPdf(Poppler.Document.new_from_file(furi, None))
            self.txt_prop = '{}\n\n\n{}'.format(
                self.txt_prop,
                pdffile.stat_info,
            )
        # elif self._pathitem.name.lower().endswith('zip'):
        #     longinfo = True
        #     f = '{}'.format(self._pathitem)
        #     self.txt_prop = '{}\n\n\n{}'.format(
        #         self.txt_prop,
        #         FilePropZip.get_zip_info(f),
        #     )

        self.prop_media = FilePropMedia().set_uri(furi).do_discovery(
            self.gst_cb
        )

        self.set_entry_text(self.txt_prop, longinfo=longinfo)
        self._entry.set_monospace(True)
        self._entry.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self._entry.modify_font(
            Pango.font_description_from_string('Monospace 11')
        )
        self._entry.set_editable(False)
        self._entry.set_cursor_visible(False)
        return self

    def gst_cb(self):
        self.txt_prop_media = self.prop_media.get_disc_info_tags_str()
        if self.txt_prop_media:
            txt = '{}\n\n\n{}'.format(self.txt_prop, self.txt_prop_media)
            self.set_entry_text(txt, longinfo=True)
        return

    def do_destroy(self, *args):
        self.prop_media.do_discovery_stop()
        super().do_destroy(*args)
        return
