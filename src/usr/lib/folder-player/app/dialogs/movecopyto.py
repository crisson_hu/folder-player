from pathlib import Path
from gi.repository import Gtk, Gdk
import shutil

from tool import _, log_debug
from app.icons import IconButton
from base import DialogBase
from app.menus.menubookmarkop import BookmarkOp
from .dobase import DialogOverlayBase


class DialogMoveCopyTo(DialogOverlayBase):

    __gtype_name__ = 'DialogMoveCopyTo'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pathitem_dest = None
        self._op = BookmarkOp.OP_NONE
        self.set_title('....')
        self.set_action(self._do_op)
        return

    def set_pathitem_dest(self, pathitem_dest):
        self._pathitem_dest = pathitem_dest
        return self

    def show(self):
        super().show()
        pi = (
            self._pathitem if not self._pathitem_dest else
            self._pathitem_dest / self._pathitem.name
        )
        txt = '{}'.format(
            _('Move') if self._op == BookmarkOp.OP_MOVE else
            _('Copy') if self._op == BookmarkOp.OP_COPY else
            _('Unsupported'),
            self._pathitem.name,
        )
        self.set_title(txt, filename=self._pathitem.name)
        self.set_entry_text(str(pi))
        return self

    def set_op(self, op=BookmarkOp.OP_NONE):
        if op not in (
                BookmarkOp.OP_MOVE,
                BookmarkOp.OP_COPY,
        ):
            raise ValueError('unsupported op: ' + op)
        self._op = op
        return self

    def _do_op(self, *args):
        if self._op == BookmarkOp.OP_COPY:
            self._do_copyto(*args)
        elif self._op == BookmarkOp.OP_MOVE:
            self._do_moveto(*args)
        else:
            raise ValueError('unsupported op: ' + self._op)
        return True

    def _do_copyto(self, *args):
        import os
        target_str = self._entry.get_text()
        target = Path(target_str)
        if not target_str.startswith(os.path.sep):
            target = self._pathitem.parent / target
        try:
            if self._pathitem.is_dir():
                shutil.copytree(
                    str(self._pathitem),
                    str(target),
                    symlinks=True
                )
            else:
                shutil.copy(
                    str(self._pathitem),
                    str(target),
                    follow_symlinks=False
                )
            self._mainview.list_directory()
        except Exception as e:
            log_debug('Exception: ' + e)
        self._close()
        return True

    def _do_moveto(self, *args):
        import os
        target_str = self._entry.get_text()
        target = Path(target_str)
        if not target_str.startswith(os.path.sep):
            target = self._pathitem.parent / target
        self._pathitem.rename(target)

        if target_str.find(os.path.sep) > -1:
            self._mainview.list_directory()
        else:
            self._filelistitem.set_display_text(target)
        self._close()
        return True
