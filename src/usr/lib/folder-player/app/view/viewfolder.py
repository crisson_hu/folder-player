from pathlib import Path
from gi.repository import GLib, Gtk, Gdk, Pango

from dbuslite import store_lite
from tool import _, log_debug, HotKey
from base import DialogOverlayBackdrop
from app.constants import Constants
from app.icons import IconButton
from app.plugins.dictclient import DictClient
from app.widgets.flyer import PathInput
from app.widgets.statusbar import StatusBar
from app.widgets.main.mainblank import MainBlank
from app.widgets.main.mainlistaddress import MainListAddress
from app.widgets.main.mainlistbox import MainListBox
from app.widgets.main.mainscrolledwindow import MainScrolledWindow


class ViewFolder(Gtk.Overlay):

    __gtype_name__ = 'ViewFolder'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._setup_widgets()
        return

    def _setup_widgets(self):
        self.main_vbox = Gtk.VBox()
        self.add(self.main_vbox)

        self._main_sw = MainScrolledWindow()
        self._main_sw.set_overlay_scrolling(False)
        self._main_sw.set_kinetic_scrolling(True)
        self._blank = MainBlank()
        self.list_box = MainListBox().set_main_scrolled_window(self._main_sw)
        vbox = Gtk.VBox()
        vbox.add(self.list_box)
        vbox.add(self._blank)
        vbox.show_all()
        vbox.child_set_property(self.list_box, 'expand', False)
        vbox.child_set_property(self._blank, 'expand', True)
        self._main_sw.add(vbox)

        self.list_address = MainListAddress()

        self.main_vbox.add(self.list_address)
        self.main_vbox.child_set_property(self.list_address, 'expand', False)
        self.main_vbox.add(self._main_sw)

        self.statusbar = StatusBar()
        self.main_vbox.add(self.statusbar)
        self.main_vbox.child_set_property(self.statusbar, 'expand', False)

        self.popup_vbox_set_view(view=None)
        self.show_all()

        return

    @property
    def popup_vbox(self):
        if not hasattr(self, '_popup_vbox'):
            self._popup_vbox = DialogOverlayBackdrop()
        return self._popup_vbox

    def popup_vbox_set_view(self, view=None):
        self.main_vbox.set_sensitive(False if view else True)
        if view:
            for c in self.popup_vbox.get_children():
                self.popup_vbox.remove(c)
            self.popup_vbox.add(view)
            self.add_overlay(self.popup_vbox)
            self.popup_vbox.set_visible(True)
            view.grab_focus()
        else:
            for c in self.popup_vbox.get_children():
                self.popup_vbox.remove(c)
            if self.popup_vbox in self.get_children():
                self.remove(self.popup_vbox)
        return

    def selected_item_show_menu(self):
        self.list_box.selected_item_show_menu()
        return

    @property
    def popup_vbox_on(self):
        return not self.main_vbox.get_sensitive()

    def set_mainview(self, mainview):
        self._mainview = mainview
        self._blank.set_mainview(self._mainview)
        self.list_address.set_mainview(self._mainview)
        self.list_box.set_mainview(self._mainview)
        self.statusbar.set_mainview(self._mainview)
        return self

    def change_directory(self, directory=None):
        if directory is None:
            directory = store_lite.last_dir
        store_lite.last_dir = directory
        self.list_address.setup(directory=directory)
        self.list_box.change_directory(directory)
        self.show_all()
        self.list_box.unselect_all()
        self.list_box.scroll_to_item()
        return

    @property
    def player(self):
        return self._mainview.mainapp.player

    def set_play_pause(self, play=False):
        self.list_box.set_play_pause(play=play)
        return

    def player_volume_up(self):
        self.player.volume += 0.1
        return True

    def player_volume_down(self):
        self.player.volume -= 0.1
        return True

    def show_main_menu(self, *args):
        return self.list_address.show_main_menu(*args)

    def on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state

        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        # keys
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        # log_debug(msg)

        if k_alt and kv in (
                ord('0'), ord('1'), ord('2'), ord('3'), ord('4'),
                ord('5'), ord('6'), ord('7'), ord('8'), ord('9'),
        ):
            nth = kv - ord('0')
            items = store_lite.last_longest_dir.split('/')
            nth_pi = '/'.join(items[:nth+1]) + '/'
            # log_debug((nth, nth_pi, items))
            self.change_directory(nth_pi)
            return True
        elif kv == Gdk.KEY_F5 or (
                k_ctrl and kv in (Gdk.KEY_r, Gdk.KEY_R)
        ):
            log_debug('refresh current directory')
            self.change_directory()
            return True
        elif k_alt and kv in (Gdk.KEY_r, Gdk.KEY_R):
            self._mainview.mainwin_ui_switch_view(Constants.UiView.RUN_DICT)
            return True
        elif k_ctrl and kv == Gdk.KEY_Escape:
            return self.show_main_menu(widget, ekey)
        elif kv in (Gdk.KEY_Super_L, Gdk.KEY_Super_R) and any(
                (k_ctrl, k_alt, k_shift)
        ):
            return self.show_main_menu(widget, ekey)
        elif k_alt and HotKey.is_down(kv):
            if self.player.is_playing():
                # log_debug('volume down')
                self.player_volume_down()
                return True
        elif k_alt and HotKey.is_up(kv):
            if self.player.is_playing():
                # log_debug('volume up')
                self.player_volume_up()
                return True
            try:
                d = str(Path(store_lite.last_dir).parent)
            except FileNotFoundError:
                pass
            else:
                log_debug('go to: %s' % d)
                self.change_directory(d)
                return True
        elif kv in (Gdk.KEY_b, Gdk.KEY_B):
            if self.player.is_playing():
                # log_debug('toggle status-bar mode')
                self.statusbar.set_playerui_visible(True)
                return True

        return False
