from gi.repository import Gdk, Gtk

from tool import HotKey, log_debug, get_fmt_day_hour_minute_second
from dbuslite import store_lite
from app.widgets.main.maindraw import MainDraw
from app.widgets.playui.videocontrol import VideoControl


class ViewVideo(Gtk.Overlay):

    __gtype_name__ = 'ViewVideo'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._setup_widgets()
        self.opt_ontop = False
        return

    def _setup_widgets(self):
        self.control_hbox = VideoControl()
        self.add_overlay(self.control_hbox)
        self._video_draw = MainDraw()
        self._video_draw.connect('realize', self._video_draw_xid_setter)
        self._video_draw.last_click_time = 0
        self._video_draw.connect(
            'button-press-event', self.video_control_toggle_show
        )
        self.add(self._video_draw)
        self.control_hbox.set_visible(True)

    def realize(self, *args, **kwargs):
        super().realize(*args, **kwargs)
        self._video_draw.realize()
        return

    def set_handler(self, handler):
        self.control_hbox.set_handler(handler)
        return

    def set_mainview(self, mainview, top_window=None):
        self.control_hbox.set_mainview(mainview)
        self.mainview = mainview
        self.main_window = top_window
        return self

    @property
    def player(self):
        return self.mainview.mainapp.player

    def video_control_toggle_show(self, widget, eventbutton):
        diff = eventbutton.time - self._video_draw.last_click_time
        self._video_draw.last_click_time = eventbutton.time
        if diff < 300:
            return True
        if eventbutton.type in (
                Gdk.EventType._2BUTTON_PRESS,
                Gdk.EventType._3BUTTON_PRESS,
        ):
            return True
        self.video_control_set_visible(not self.control_hbox.get_visible())
        return True

    def video_control_set_visible(self, visible=True):
        self.control_hbox.set_visible(visible)
        return

    def _video_draw_xid_setter(self, *args):
        self._video_draw_xid = self._video_draw.get_window().get_xid()
        return True

    @property
    def video_draw_xid(self):
        if not hasattr(self, '_video_draw_xid'):
            self._video_draw_xid = 0
        return self._video_draw_xid

    def player_stream_update(self):
        self.control_hbox.player_stream_update()
        self.main_window.set_title(
            progress=store_lite.player_stream_progress_label
        )
        return True

    def set_volume(self, volume):
        self.control_hbox.set_volume(volume)
        return True

    def set_volume_mute(self, mute):
        self.control_hbox.set_volume_mute(mute)
        return True

    def set_play_pause(self, play=False):
        self.video_control_set_visible(True)
        self.control_hbox.set_play_pause(play)
        return

    def reset_play_ui(self):
        self.video_control_set_visible(True)
        self.control_hbox.reset()
        return

    def player_volume_up(self):
        self.player.volume += 0.1
        return True

    def player_volume_down(self):
        self.player.volume -= 0.1
        return True

    def on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state

        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        # keys
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        enable_key = False
        # log_debug(msg)

        if kv in (Gdk.KEY_t, Gdk.KEY_T):
            self.opt_ontop = not self.opt_ontop
            self.main_window.set_keep_above(self.opt_ontop)
            log_debug('{}ontop'.format('' if self.opt_ontop else 'NOT '))
            return True
        elif kv in (ord('0'), ord('1'), ord('2'), ord('='),):
            if kv == ord('='):
                winsize = 1
            else:
                winsize = kv - ord('0')
            winsize = 0.5 if winsize == 0 else winsize
            (w, h) = self.player.get_video_size()
            if w > 0 and h > 0:
                new_w = int(w * winsize)
                new_h = int(h * winsize)
                self.main_window.resize(new_w, new_h)
            return True
        elif kv in (ord('+'), ord('-')):
            winscale = 1 + (0.1 if kv == ord('+') else -0.1)
            (w, h) = self.main_window.get_size()
            if w > 0 and h > 0:
                new_w = int(w * winscale)
                new_h = int(h * winscale)
                self.main_window.resize(new_w, new_h)
            return True
        elif k_alt and HotKey.is_down(kv):
            if self.player.is_playing():
                # log_debug('volume down')
                self.player_volume_down()
                return True
        elif k_alt and HotKey.is_up(kv):
            if self.player.is_playing():
                # log_debug('volume up')
                self.player_volume_up()
                return True

        return False
