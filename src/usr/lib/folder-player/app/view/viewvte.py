from gi.repository import GLib, Gtk, Vte


class ViewTerm(Gtk.VBox):

    __gtype_name__ = 'ViewTerm'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def _setup_widgets(self):
        term = Vte.Terminal.new()
        self.term = term
        term.set_word_char_exceptions(self.VTE_ADDITIONAL_WORDCHARS)
        term.connect('child-exited', self.vte_exit)
        term.connect('destroy', self.vte_exit)
        term_pty = Vte.Pty.new_sync(Vte.PtyFlags.DEFAULT)
        term_pty.spawn_async(
            None,
            ['/bin/bash'],
            None,
            GLib.SpawnFlags.DEFAULT,
            None,
            None,
            -1,
            None,
            self.ready)
        term.set_pty(term_pty)
        self.add(self.term)
        return
