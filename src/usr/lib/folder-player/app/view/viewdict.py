from gi.repository import GLib, Gtk, Gdk, Pango

from tool import _, log_debug
from app.icons import IconButton
from app.widgets.flyer import PathInput
from app.plugins.dictclient import DictClient
from app.constants import Constants


class ViewDict(Gtk.VBox):

    __gtype_name__ = 'ViewDict'

    __style_view__ = 'ViewDictView'
    __style_viewbusy__ = 'ViewDictViewBusy'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sig_handler = self.Handler(self)
        self.client = DictClient()
        self.cmd = self._cmd = ''
        return

    def _setup_widgets(self):
        box_top = Gtk.VBox()
        self.add(box_top)
        box_top_intr = Gtk.HBox()
        box_top.add(box_top_intr)

        self.top_input = PathInput()
        box_top_intr.add(self.top_input)
        control_icon = IconButton().set_icon('player/control.png')
        control_icon.set_tooltip_text(_('control'))
        box_top_intr.add(control_icon)
        box_top_intr.child_set_property(control_icon, 'expand', False)
        box_top_intr.child_set_property(control_icon, 'fill', False)
        control_icon.connect(
            'primary-single-click',
            self.mainwin_ui_switch_view_normal
        )
        self.connect('key-press-event', self.sig_handler.on_key_press)
        self.connect('key-release-event', self.sig_handler.on_key_release)

        self.main_sw = Gtk.ScrolledWindow()
        self.main_sw.set_visible(True)
        self.main_sw.set_can_focus(False)
        self.main_sw.set_overlay_scrolling(False)
        self.main_sw.set_kinetic_scrolling(True)
        self.add(self.main_sw)
        self.main_view = Gtk.TextView()
        self.main_view.set_visible(True)
        self.main_view.set_can_focus(False)
        self.main_view.set_cursor_visible(False)
        self.main_view.set_accepts_tab(False)
        self.main_view.connect('button-press-event',
                               self.sig_handler.on_popup_menu)
        self.main_sw.add(self.main_view)

        box_top.child_set_property(self.top_input, 'expand', False)
        box_top.child_set_property(self.top_input, 'fill', True)
        self.child_set_property(box_top, 'expand', False)
        self.child_set_property(box_top, 'fill', True)
        self.child_set_property(self.main_sw, 'expand', True)
        self.child_set_property(self.main_sw, 'fill', True)

        for w in (
                self.top_input,
                control_icon,
                self.main_view,
        ):
            w.get_style_context().add_class(self.__style_view__)
        return

    def show_all(self, *args, **kwargs):
        super().show_all(*args, **kwargs)
        self._reset_cursor()
        return

    def _setup_style(self):
        font = '"Lucida Grande", Arial, sans-serif, Monospace'
        font = 'Monospace'
        des13_bold = Pango.font_description_from_string('%s bold 13' % font)
        des13 = Pango.font_description_from_string('%s 13' % font)
        des_bold = Pango.font_description_from_string('%s bold 11' % font)
        des = Pango.font_description_from_string('%s 11' % font)
        # self.top_input.modify_font(des13_bold)
        self.top_input.modify_font(des_bold)
        # self.top_input.modify_font(des)
        self.main_view.modify_font(des)

        margin = 10
        margin = 20
        self.main_view.set_left_margin(margin)
        self.main_view.set_right_margin(margin)
        self.main_view.set_top_margin(margin)
        self.main_view.set_bottom_margin(margin)

    def set_mainview(self, mainview):
        self._mainview = mainview
        self._setup_widgets()
        self._setup_style()
        return self

    def destroy(self):
        self._mainview.mainapp.destroy()
        return True

    def handle_down(self, page=False):
        if page:
            move = Gtk.ScrollType.PAGE_DOWN
        else:
            move = Gtk.ScrollType.STEP_DOWN
        self.main_sw.do_scroll_child(self.main_sw, move, False)
        return

    def handle_up(self, page=False):
        if page:
            move = Gtk.ScrollType.PAGE_UP
        else:
            move = Gtk.ScrollType.STEP_UP
        self.main_sw.do_scroll_child(self.main_sw, move, False)
        return

    def handle_toggle_fullscreen(self):
        if not hasattr(self, '_fullscreen'):
            self._fullscreen = False
        self._fullscreen = not self._fullscreen
        self.set_fullscreen()
        return

    def _reset_cursor(self, *args):
        win = self.main_view.get_window(Gtk.TextWindowType.TEXT)
        win.set_cursor(Gdk.Cursor(Gdk.CursorType.ARROW))
        return True

    def _set_busy_begin(self):
        self.main_view.get_style_context().add_class(
            self.__style_viewbusy__
        )
        win = self.main_view.get_window(Gtk.TextWindowType.TEXT)
        win.set_cursor(Gdk.Cursor(Gdk.CursorType.WATCH))
        return False

    def _set_busy_end(self):
        self.main_view.get_style_context().remove_class(
            self.__style_viewbusy__
        )
        self._reset_cursor()
        return False

    def handle_activate(self):
        self._cmd = self.top_input.get_text()
        self.cmd = self._cmd.strip()
        if not self.cmd or len(self.cmd) == 0:
            self.main_view.get_buffer().set_text('')
            return
        self._set_busy_begin()
        GLib.idle_add(self._call_client)
        return

    def _call_client(self):
        try:
            msg = self.client.get_response(self.cmd)
        except Exception as e:
            log_debug('Exception: ' + str(e))
            msg = ''
        finally:
            self.main_view.get_buffer().set_text(msg)
            self.check_input_change()
        self._set_busy_end()
        # return False to get called only once
        return False

    def check_input_change(self):
        cmd_text = self.top_input.get_text().strip()
        if cmd_text in (self._cmd, self.cmd):
            focus = True
        else:
            focus = False

        if focus:
            self.main_sw.set_opacity(1)
        else:
            self.main_sw.set_opacity(0.7)

    def set_fullscreen(self):
        if self._fullscreen:
            self.fullscreen()
        else:
            self.unfullscreen()
        return

    def mainwin_ui_switch_view_normal(self, gesture, n_press, x, y):
        self._mainview.mainwin_ui_switch_view(Constants.UiView.NORMAL)
        return True

    class Handler:

        def __init__(self, app):
            self.app = app

        def on_popup_menu(self, *args):
            return True

        def on_key_release(self, widget, ev):
            self.app.check_input_change()
            return False

        def on_key_press(self, widget, ev):
            kv = ev.keyval
            ks = ev.state
            k_ctrl = False
            k_alt = False
            k_shift = False

            if ks & Gdk.ModifierType.CONTROL_MASK:
                k_ctrl = True
            if ks & Gdk.ModifierType.MOD1_MASK:
                k_alt = True
            if ks & Gdk.ModifierType.SHIFT_MASK:
                k_shift = True

            if kv == Gdk.KEY_Control_L or kv == Gdk.KEY_Control_R:
                k_ctrl = True
            if kv == Gdk.KEY_Alt_L or kv == Gdk.KEY_Alt_R:
                k_alt = True
            if kv in (Gdk.KEY_Shift_L, Gdk.KEY_Shift_R,
                      Gdk.KEY_Shift_Lock):
                k_shift = True

            # keys
            msg = ''
            if k_ctrl:
                msg = 'Ctrl-'
            if k_alt:
                msg += 'Alt-'
            if k_shift:
                msg += 'Shift-'
            msg = 'pressed {}{:c} [0x{:x}]'.format(msg, kv, kv)

            if kv in (Gdk.KEY_Return, Gdk.KEY_ISO_Enter, Gdk.KEY_KP_Enter,):
                if k_ctrl:
                    self.app.handle_toggle_fullscreen()
                    return True
                self.app.handle_activate()
                return True
            elif k_ctrl and kv in (Gdk.KEY_w, Gdk.KEY_W):
                log_debug(msg)
                self.app.destroy()
                return True
            elif kv == Gdk.KEY_Up:
                self.app.handle_up()
                return True
            elif kv == Gdk.KEY_Down:
                self.app.handle_down()
                return True
            elif kv == Gdk.KEY_KP_Page_Down or kv == Gdk.KEY_Page_Down:
                self.app.handle_down(page=True)
                return True
            elif kv == Gdk.KEY_KP_Page_Up or kv == Gdk.KEY_Page_Up:
                self.app.handle_up(page=True)
                return True
            elif k_ctrl and kv in (Gdk.KEY_V, Gdk.KEY_v):
                self.app.handle_down(page=True)
                return True
            elif k_alt and kv in (Gdk.KEY_V, Gdk.KEY_v):
                self.app.handle_up(page=True)
                return True
            return False
