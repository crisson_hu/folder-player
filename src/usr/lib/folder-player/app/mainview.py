import os
from pathlib import Path
from gi.repository import Gdk, Gtk

from tool import HotKey, log_debug, _
from app.widgets.main.mainwindow import MainWindow
from dbuslite import store_lite
from app.constants import Constants


class MainView:

    '''The central component that connects others components such as
    player, widgets, etc.'''

    def __init__(self, mainapp=None):
        self.mainapp = mainapp
        self._win = None
        self._setup_ui()
        return

    def main_raise(self):
        if not self.mainwin:
            raise ValueError('Error: not initialized!')
            return
        self.mainwin.show_all()
        self.mainwin.present()
        return

    def ui_destroy(self):
        self._store.save()
        if self._win:
            self._win.destroy()
            self._win = None
        return

    def _setup_ui(self):
        self._store = store_lite
        self._win = MainWindow().setup_signal(handler=self.handler) \
                                .setup(mainview=self)
        return

    @property
    def handler(self):
        if not hasattr(self, '_handler'):
            self._handler = self.Handler(parent=self, mainui=self.mainapp)
        return self._handler

    @property
    def mainwin(self):
        return self._win

    @property
    def mainwin_ui_view(self):
        return self.mainwin.mainwin_ui_view

    def mainwin_ui_switch_view(self, mainwin_ui_view):
        self.mainwin.mainwin_ui_switch_view(mainwin_ui_view)
        return

    @property
    def mainwin_ui_view_folder(self):
        return self.mainwin.mainwin_ui_view_folder

    @property
    def mainwin_ui_view_video(self):
        return self.mainwin.mainwin_ui_view_video

    def get_video_window_xid(self):
        return self.mainwin_ui_view_video.video_draw_xid

    def update_ui_info(self):
        if not self.mainapp.player.uri:
            self.current_item = None
        return

    def player_stream_update(self):
        self.mainwin_ui_view_video.player_stream_update()
        if self.current_item:
            self.current_item.player_stream_update()
        return

    def set_play_pause(self, play=False, stop=False):
        self.mainwin_ui_view_video.set_play_pause(play=play)
        if self.current_item:
            self.current_item.set_play_pause(play)
            if stop:
                self.current_item.reset_play_ui()
        if stop:
            self.mainwin_ui_view_video.reset_play_ui()
        return

    def ui_set_volume(self, volume):
        self.mainwin_ui_view_video.set_volume(volume)
        if self.current_item:
            self.current_item.set_volume(volume)
        return True

    def ui_set_volume_mute(self, mute):
        self.mainwin_ui_view_video.set_volume_mute(mute)
        if self.current_item:
            self.current_item.set_volume_mute(mute)
        return True

    def list_directory(self, directory=None):
        if directory is None:
            directory = self._store.last_dir
        elif not Path(directory).is_dir():
            try:
                directory = str(Path(directory).parent)
            except FileNotFoundError:
                directory = self._store.last_dir
        else:
            directory = str(Path(directory))

        try:
            os.chdir(directory)
        except FileNotFoundError as e:
            log_debug('[FileNotFoundError]: ' + str(e))
            while True:
                d = Path(directory).parent
                if d.exists():
                    os.chdir(d)
                    directory = str(d)
                    break

        self.mainwin.change_directory(directory=directory)
        return

    def selected_item_show_menu(self):
        self.mainwin_ui_view_folder.selected_item_show_menu()
        return

    @property
    def current_item(self):
        if not hasattr(self, '_current_item'):
            self._current_item = None
        return self._current_item

    @current_item.setter
    def current_item(self, item):
        if hasattr(self, '_current_item') and self._current_item:
            self._current_item.set_is_current(False)
            self._current_item.set_play_pause(False)

        if type(item) is str:
            self.mainwin.set_title(item)
            return

        self._current_item = item
        self.mainwin.set_title(
            self.current_item.get_pathitem().name
            if self.current_item
            else None
        )
        if item:
            self._current_item.set_is_current(True)
            self._current_item.set_play_pause(True)

        return

    @current_item.deleter
    def current_item(self):
        self.mainwin.set_title(None)
        self._current_item = None
        del self._current_item

    def play_file(self, item, force_if_paused=False):
        if (
                force_if_paused
                and not self.mainapp.player.is_playing()
                and (
                    self.current_item is None
                    or self.current_item.get_uri() != item.get_uri()
                )
        ):
            self.mainapp.player.player_stop()
        if self.mainapp.play_file(item.get_pathitem()):
            self.current_item = item
            return True
        return False

    def player_play(self):
        if self.current_item:
            self.current_item.set_play_pause(True)
        if self.mainapp.player.player_play():
            return True
        return False

    def player_pause(self):
        if self.current_item:
            self.current_item.set_play_pause(False)
        if self.mainapp.player.player_pause():
            return True
        return False

    def player_toggle_pause(self):
        if not self.mainapp.player.is_playing():
            self.player_play()
        else:
            self.player_pause()
        return True

    def player_volume_up(self):
        self.mainapp.player.volume += 0.1
        return True

    def player_volume_down(self):
        self.mainapp.player.volume -= 0.1
        return True

    def player_volume_tooltip(self, widget):
        volume = '{}: {:.0f}%'.format(
            _('Volume'), self.mainapp.player.volume * 100
        )
        widget.set_tooltip_text(_(volume))
        # FIXME: why `False` ? Doc says it should be `True`.
        return False

    def player_volume_get_mute(self):
        return self.mainapp.player.mute

    def player_volume_toggle_mute(self):
        self.mainapp.player.mute = not self.mainapp.player.mute
        return True

    def player_volume_set_mute(self):
        self.mainapp.player.mute = True
        return True

    def player_volume_set_unmute(self):
        self.mainapp.player.mute = False
        return True

    class Handler:

        def __init__(self, parent=None, mainui=None):
            self.parent = parent
            self.mainui = mainui
            return

        def destroy(self, *args):
            self.mainui.destroy()
            return True

        def on_window_delete(self, widget, ev):
            self.mainui.destroy()
            return True

        def on_window_minimize(self, *args):
            self.parent.mainwin.iconify()
            return True

        def on_volume_tooltip(self, widget, *args):
            return self.parent.player_volume_tooltip(widget=widget)

        def on_player_toggle_pause(self, *args):
            self.parent.player_toggle_pause()
            return True

        def on_player_play(self, *args):
            self.parent.player_play()
            return True

        def on_player_pause(self, *args):
            self.parent.player_pause()
            return True

        def on_switch_to_mainui(self, *args):
            self.parent.mainwin_ui_switch_view(Constants.UiView.NORMAL)
            return True

        def on_player_stop(self, *args):
            if self.mainui.get_state_is_playing():
                log_debug('Still playing, but stop as you requested.')
            self.mainui.player.player_stop(from_user=True)
            self.mainui.player.player_switch_view_normal()
            return True

        def on_player_volume_up(self, *args):
            self.parent.player_volume_up()
            return True

        def on_player_volume_down(self, *args):
            self.parent.player_volume_down()
            return True

        def on_player_volume_toggle_mute(self, *args):
            self.parent.player_volume_toggle_mute()
            return True

        def on_player_volume_set_mute(self, *args):
            self.parent.player_volume_set_mute()
            return True

        def on_player_volume_set_unmute(self, *args):
            self.parent.player_volume_set_unmute()
            return True

        def on_player_seek_backward(self, big_step=True):
            self.mainui.player.player_seek_step_backward(
                step=20 if big_step else 10
            )
            return True

        def on_player_seek_forward(self, big_step=True):
            self.mainui.player.player_seek_step_forward(
                step=20 if big_step else 10
            )
            return True

        def on_key_press(self, widget, ekey):
            if (
                    self.parent.mainwin_ui_view == Constants.UiView.NORMAL and
                    self.parent.mainwin.mainwin_ui_view_folder.popup_vbox_on
            ):
                msg = 'skip mode: overlay popup on'
                # log_debug(msg)
                return False
            if self.parent.mainwin_ui_view == Constants.UiView.RUN_DICT:
                msg = 'skip mode: run dict'
                # log_debug(msg)
                return False

            kv = ekey.keyval
            ks = ekey.state

            (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
            # keys
            msg = '{}{}{}{:c} [0x{:x}]'.format(
                'Ctrl-' if k_ctrl else
                '',
                'Alt-' if k_alt else
                '',
                'Shift-' if k_shift else
                '',
                kv, kv
            )
            enable_key = False
            # log_debug(msg)

            if not any((k_ctrl, k_alt)) and kv in (Gdk.KEY_q, Gdk.KEY_Q):
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    self.on_player_stop()
                    return True
            elif kv in (Gdk.KEY_w, Gdk.KEY_W):
                if k_ctrl:
                    log_debug(msg)
                    if self.parent.mainwin_ui_view == Constants.UiView.VIDEO:
                        self.on_player_stop()
                    elif self.parent.mainwin_ui_view in (
                            Constants.UiView.NORMAL,
                            Constants.UiView.RUN_DICT,
                    ):
                        self.destroy()
                    else:
                        raise Exception('To Be Supported.')
                    return True
            elif k_ctrl and kv in (Gdk.KEY_z, Gdk.KEY_Z):
                self.on_window_minimize()
                return True
            elif kv == Gdk.KEY_Menu:
                log_debug('pressed menu key.')
                if self.parent.mainwin_ui_view == Constants.UiView.NORMAL:
                    self.parent.selected_item_show_menu()
                    return True
            elif kv == Gdk.KEY_Escape:
                if k_ctrl and self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL,
                ):
                    return False
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    if self.mainui.get_state_is_playing():
                        self.on_player_pause()
                    else:
                        self.on_player_stop()
                    return True
            elif kv in (Gdk.KEY_p, Gdk.KEY_P, Gdk.KEY_KP_Space, Gdk.KEY_space):
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    self.on_player_toggle_pause()
                    return True
            elif kv in (Gdk.KEY_m, Gdk.KEY_M):
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    self.on_player_volume_toggle_mute()
                    return True
            elif HotKey.is_right(kv):
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    if self.mainui.get_state_is_playing():
                        # log_debug('seek forward')
                        self.on_player_seek_forward(big_step=k_alt)
                        return True
            elif HotKey.is_left(kv):
                if self.parent.mainwin_ui_view in (
                        Constants.UiView.NORMAL, Constants.UiView.VIDEO
                ):
                    if self.mainui.get_state_is_playing():
                        # log_debug('seek backward')
                        self.on_player_seek_backward(big_step=k_alt)
                        return True
            elif kv == Gdk.KEY_F11 or (
                    k_ctrl and kv in (
                        Gdk.KEY_Return,
                        Gdk.KEY_ISO_Enter,
                        Gdk.KEY_KP_Enter,
                    )
            ):
                self.parent.mainwin.toggle_fullscreen()
                return True

            if self.parent.mainwin_ui_view == Constants.UiView.NORMAL:
                return self.parent.mainwin_ui_view_folder.on_key_press(
                    self.parent.mainwin_ui_view_folder, ekey
                )

            if self.parent.mainwin_ui_view == Constants.UiView.VIDEO:
                return self.parent.mainwin_ui_view_video.on_key_press(
                    self.parent.mainwin_ui_view_video, ekey
                )

            return False
