from pathlib import Path
from gi.repository import Gtk, Gdk, Pango

from app.icons import IconButton, IconType
from app.menus.menufileitem import MenuFileItem
from app.quickview import QuickView
from app.theme import set_selected
from app.widgets.playui.playprogressbar import PlayProgressBar
from app.widgets.playui.playvolume import PlayVolume
from base import WidgetBaseMixin
from tool import HotKey, log_debug, FileType, _


class FilesListItem(Gtk.EventBox, WidgetBaseMixin):

    __gtype_name__ = 'FileListItem'

    STYLE_CLASS_FOLDER = 'folder'
    STYLE_CLASS_FILE = 'file'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self._mainview = None
        self.parent_row = None
        self.parent_row_index = 0
        self.pathitem = None
        self._uri = None
        self._setup_layout()
        return

    def _setup_layout(self):
        self.set_property('height-request', 40)
        self.set_property('margin-left', 1)
        self.set_property('margin-right', 1)
        # self.set_property('margin-left', 0)
        # self.set_property('margin-right', 0)

        self.main_hbox = Gtk.HBox()
        hb2 = Gtk.HBox()
        self.add(self.main_hbox)
        self.main_hbox.add(hb2)
        self.main_hbox.child_set_property(hb2, 'expand', False)
        self.main_hbox.child_set_property(hb2, 'fill', False)
        self.main_hbox.child_set_property(hb2, 'padding', 0)

        # type icon
        self.icon_type = IconType()
        hb2.add(self.icon_type)

        # play/pause
        self.btn_play = IconButton().set_icon('player/play.png')
        self.btn_play.set_no_show_all(True)
        self.btn_play.set_tooltip_text(_('Play'))
        self.btn_pause = IconButton().set_icon('player/pause.png')
        self.btn_pause.set_no_show_all(True)
        self.btn_pause.set_tooltip_text(_('Pause'))
        hb2.add(self.btn_play)
        hb2.add(self.btn_pause)
        self.btn_play.set_property('margin-left', 5)
        self.btn_play.set_property('margin-right', 2)
        self.btn_pause.set_property('margin-left', 5)
        self.btn_pause.set_property('margin-right', 2)

        self.main_vbox = Gtk.VBox()
        self.main_hbox.add(self.main_vbox)
        self.main_vbox.set_property('margin-right', 5)
        self.child = Gtk.Label()
        self.child.set_property('xalign', 0)
        self.child.set_property('yalign', 0.6)
        self.child.set_property('wrap', True)
        self.child.set_property('ellipsize', Pango.EllipsizeMode.END)
        self.childeb = Gtk.EventBox()
        self.childeb.add(self.child)
        self.main_vbox.add(self.childeb)
        self.gesture_multipress = Gtk.GestureMultiPress.new(self.childeb)
        self.gesture_multipress.set_button(0)
        self.gesture_multipress.set_exclusive(True)

        return

    @property
    def progress(self):
        if not hasattr(self, '_progress'):
            # progress bar
            self._progress = PlayProgressBar()
            self._progress.set_no_show_all(True)
            self.main_vbox.add(self._progress)
            self.main_vbox.child_set_property(self._progress, 'expand', False)
        return self._progress

    @property
    def btn_volume(self):
        if not hasattr(self, '_btn_volume'):
            self._btn_volume = PlayVolume()
            self._btn_volume.set_property('margin-right', 20)
            self.main_hbox.add(self._btn_volume)
            self.main_hbox.child_set_property(
                self._btn_volume, 'expand', False
            )
            self.main_hbox.child_set_property(
                self._btn_volume, 'fill', False
            )
            self.main_hbox.child_set_property(
                self._btn_volume, 'padding', 0
            )
        return self._btn_volume

    def set_mainview(self, mainview):
        self._mainview = mainview
        self.btn_volume.set_mainview(mainview)
        return self

    def set_container(self, container):
        self._container = container
        return self

    def check_match(self):
        match = False
        play = False
        if self.match(self._mainview.current_item):
            match = True
            self.set_is_current(current=True)
            self.player_stream_update()
            self.set_volume(self._mainview.current_item.get_volume())
            self._mainview.current_item = self
            play = (
                True if self._mainview.mainapp.player.is_playing() else
                False
            )

        self.set_play_pause(play=play)
        return match

    def set_handler(self, handler):
        self.gesture_multipress.connect('pressed', self._on_mpg_pressed)
        self.btn_play.connect('primary-single-click', self._on_play_pressed)
        self.btn_pause.connect('primary-single-click', handler.on_player_pause)
        self.parent_row.connect('activate', self._on_row_activate)
        self.btn_volume.set_handler(handler)
        return self

    def set_row(self, list_box, row):
        self._list_box = list_box
        self.parent_row = row
        return self

    def get_row(self):
        return self.parent_row

    def set_row_index(self, index):
        self.parent_row_index = index
        return self

    def get_row_index(self):
        return self.parent_row_index

    def set_display_text(self, pathitem):
        fmt = '<span color="#333" size="large">{}{}</span>'
        fmt = '<span size="large">{}{}</span>'
        fmt = '{}{}'
        value = fmt.format(
            pathitem.name,
            FileType.get_file_type_symbol(pathitem)
        )

        self.icon_type.set_display_text(pathitem)

        self.pathitem = pathitem
        # refresh pathitem
        if hasattr(self, '_menu'):
            delattr(self, '_menu')

        self._uri = str(self.pathitem)

        try:
            self.child.set_label(value)
            self.child.set_tooltip_text(value)
        except Exception as e:
            log_debug(e)

        return self

    def match(self, item):
        return self.get_uri() == item.get_uri() if item else False

    def match_pathitem(self, pathitem):
        return self.get_uri() == str(pathitem) if pathitem else False

    def _on_row_activate(self, row):
        self._execute_pi()
        return True

    def on_key_press(self, widget, ekey):
        if not self.pathitem.is_file():
            return False

        kv = ekey.keyval
        ks = ekey.state

        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        # keys
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        # log_debug(msg)
        if k_ctrl or k_alt:
            return False
        elif kv in (Gdk.KEY_v, Gdk.KEY_V):
            win = QuickView() \
                .set_pathitem(self.pathitem) \
                .set_mainview(self._mainview) \
                .show()
            win.is_can_modify = False
            return True
        return False

    def show_menu(self, x=-1, y=-1, device=None):
        if not hasattr(self, '_menu'):
            self._menu = (
                MenuFileItem().set_pathitem(self.pathitem)
                .set_mainview(self._mainview, self._list_box)
                .set_filelistitem(self)
            )
        if x == -1 or y == -1:
            ret, ox, oy = self.child.get_window().get_origin()
            h = self.get_allocated_height()
            x = ox
            y = oy + h

        self._menu.popup(x, y, device=device)

        return True

    def _do_select_row(self, select=True):
        if select:
            self._list_box.select_row(self.parent_row)
        else:
            self._list_box.unselect_row(self.parent_row)
        return

    def _on_mpg_pressed(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_SECONDARY and n_press == 1:
            self._do_select_row(True)
            _screen, gx, gy = gesture.get_device().get_position()
            return self.show_menu(gx + 2, gy + 2, device=gesture.get_device())
        if button != Gdk.BUTTON_PRIMARY or n_press != 2:
            # Only support mouse double click.
            return True
        self._execute_pi()
        return True

    def _on_play_pressed(self, *args):
        self._execute_pi()
        return True

    def _execute_pi(self):
        if self.pathitem.is_dir():
            # Note: don't call self._do_select_row for directory.
            # It would crash because a rows of list-box is under change.
            self._mainview.list_directory(self.get_uri())
        elif self.pathitem.is_file():
            self._do_select_row(True)
            (
                self._mainview.play_file(self)
                or self._mainview.play_file(self, force_if_paused=True)
                or self._mainview.player_play()
            )

        return

    def get_pathitem(self):
        return self.pathitem

    def get_uri(self):
        return self._uri

    def set_selected(self, is_selected):
        try:
            if is_selected:
                self.parent_row.connect('key-press-event', self.on_key_press)
            else:
                self.parent_row.disconnect_by_func(self.on_key_press)
        except TypeError as e:
            pass
        except Exception as e:
            log_debug('unknown error')
        finally:
            pass

        set_selected(self, is_selected)
        for i in (self.progress, self.btn_volume):
            set_selected(i, is_selected)
        return True

    @property
    def is_selected(self):
        if not hasattr(self, '_is_selected'):
            self._is_selected = False
        return self._is_selected

    @is_selected.setter
    def is_selected(self, is_selected):
        self._is_selected = is_selected
        return

    def set_is_current(self, current=False):
        for i in (self.progress, self.btn_volume):
            if current:
                i.set_visible(current)
                i.show_all()
            else:
                i.set_visible(current)
        return True

    def set_volume(self, volume):
        self.btn_volume.set_volume(volume)
        return True

    def set_volume_mute(self, mute):
        self.btn_volume.set_volume_mute(mute)
        return True

    def get_volume(self):
        return self.btn_volume.get_volume()

    def set_play_pause(self, play=False):
        self.btn_play.set_visible(not play)
        self.btn_pause.set_visible(play)
        return True

    def reset_play_ui(self):
        self.progress.set_visible(False)
        return

    def player_stream_update(self):
        self.progress.player_stream_update()
        return
