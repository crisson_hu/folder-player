from datetime import datetime
from pathlib import Path

from tool import log_debug
from .fileslistitem import FilesListItem


class FilesList:

    def __init__(self):
        self._mainview = None
        self._list_box = None
        self._path_items = None
        self._handler = None
        self.pr_tstart = 0
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def set_container(self, list_box=None):
        self._list_box = list_box
        return self

    def set_handler(self, handler=None):
        self._handler = handler
        return self

    def task_stop(self):
        pass

    def task_start(self, directory=None):
        self.pr_tstart = datetime.now()
        if directory is None:
            directory = '.'

        all_items = list(filter(
            lambda x: not str(x.name).startswith('.'),
            Path(directory).glob('*')
        ))
        d = list(
            i.name for i in
            filter(
                Path.is_dir,
                all_items
            )
        )
        d.sort(key=str.lower)
        f = list(
            i.name for i in all_items
            if i.name not in d
        )
        f.sort(key=str.lower)
        self._path_items = list(Path(directory) / x for x in d + f)

        start_index = len(self._list_box.get_children())
        self._list_box.set_match_item(item=None)
        for i in range(len(self._path_items)):
            item = FilesListItem()
            self._list_box.insert(item, -1)

            (
                item
                .set_row(
                    self._list_box,
                    self._list_box.get_row_at_index(i + start_index)
                )
                .set_mainview(self._mainview)
                .set_container(self)
                .set_display_text(self._path_items[i])
                .set_row_index(i + start_index)
                .set_handler(self._handler)
            )

            if item.check_match():
                self._list_box.set_match_item(item=item)

        self.task_start_end()

        return self

    def task_start_end(self):
        d = datetime.now() - self.pr_tstart

        # datetime.timedelta()
        # arguments:
        # -999999999 <= days <= 999999999
        # 0 <= seconds < 3600*24 (the number of seconds in one day)
        # 0 <= microseconds < 1000000
        #
        # Instance attributes:
        # Attribute         Value
        #  days              Between -999999999 and 999999999 inclusive
        #  seconds           Between 0 and 86399 inclusive
        #  microseconds      Between 0 and 999999 inclusive
        ms = '{:06}'.format(d.microseconds)
        log_debug('FilesList: {}.{} sec.'.format(d.seconds, ms[:3]))
        return True

    def delete_item(self, item):
        self._list_box.remove(item)
        return True
