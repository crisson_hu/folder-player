from pathlib import Path

from tool import log_debug, _
from app.icons import IconButton


class ButtonPlayer(IconButton):

    __gtype_name__ = 'ButtonPlayer'
    style_class_pressed = 'ButtonPlayerPressed'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_icon('status/player.png')
        self._set_tooltip()
        self.setup_signal()
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def setup_signal(self, handler=None):
        self._handler = handler
        self.connect('primary-single-click', self._show_menu)
        self.connect('secondary-single-click', self._show_menu)
        return self

    def _set_tooltip(self):
        tip = _('Player Control')
        self.set_tooltip_text(tip)
        self.set_tooltip_markup(tip)
        return self

    def _show_menu(self, gmp, n_press, x, y):
        # screen, rx, ry = gmp.get_device().get_position()
        # self.menu_right.popup(rx - x, ry - y)
        self.toggle_playerui()
        return True

    def toggle_playerui(self):
        visible = True
        style_ctx = self.get_style_context()
        list_cls = style_ctx.list_classes()
        bar = self._mainview.mainwin_ui_view_folder.statusbar
        if self.style_class_pressed in list_cls:
            visible = False
            style_ctx.remove_class(self.style_class_pressed)
            bar.set_playerui_visible(False)
        else:
            visible = True
            style_ctx.add_class(self.style_class_pressed)
            bar.set_playerui_visible(True)
        list_cls = style_ctx.list_classes()
        log_debug(list_cls)
        return visible

    def get_playerui_visible(self):
        list_cls = style_ctx.list_classes()
        return True if self.style_class_pressed in list_cls else False
