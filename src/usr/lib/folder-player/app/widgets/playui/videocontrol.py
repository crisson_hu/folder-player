from pathlib import Path
from gi.repository import Gtk

from dbuslite import store_lite
from tool import _
from app.icons import IconButton
from .playprogressbar import PlayProgressBar
from .playvolume import PlayVolume
from .buttonrepeat import ButtonRepeat
from base import WidgetBaseMixin


class VideoControl(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'VideoControl'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.setup_widgets()
        return

    def setup_widgets(self):
        self.set_property('width-request', 300)

        self.set_property('halign', Gtk.Align.CENTER)
        self.set_property('valign', Gtk.Align.START)

        self.set_hexpand(True)
        self.set_vexpand(False)
        self.btn_play = IconButton().set_icon('player/play.png')
        self.btn_play.set_tooltip_text(_('Play'))
        self.btn_play.set_no_show_all(True)
        self.btn_pause = IconButton().set_icon('player/pause.png')
        self.btn_pause.set_tooltip_text(_('Pause'))
        self.btn_pause.set_no_show_all(True)
        self.progress = PlayProgressBar()
        self.btn_volume = PlayVolume()
        self.btn_volume.set_visible(True)
        self.btn_repeat = ButtonRepeat()
        self.btn_close = IconButton().set_icon('player/close.png')
        self.btn_close.set_tooltip_text(_('Close Video'))

        for i in (
                self.btn_play,
                self.btn_pause,
                self.progress,
                self.btn_volume,
                self.btn_repeat,
                self.btn_close,
        ):
            i.set_property('halign', Gtk.Align.CENTER)
            i.set_property('valign', Gtk.Align.CENTER)
            i.set_hexpand(False)
            i.set_vexpand(False)
            i.set_can_focus(False)
            i.set_property('margin', 0)
            self.add(i)
        self.btn_repeat.set_property('margin-left', 10)
        self.btn_close.set_property('margin-left', 30)
        self.progress.set_hexpand(True)
        self.progress.set_vexpand(True)
        self.progress.set_property('width-request', 300)
        self.child_set_property(self.progress, 'expand', True)
        self.child_set_property(self.progress, 'fill', True)

        return

    def set_volume(self, volume):
        self.btn_volume.set_volume(volume)
        return True

    def set_volume_mute(self, mute):
        self.btn_volume.set_volume_mute(mute)
        return True

    def set_mainview(self, mainview):
        self.btn_volume.set_mainview(mainview)
        return self

    def set_play_pause(self, play=False):
        self.btn_play.set_visible(not play)
        self.btn_pause.set_visible(play)
        return

    def player_stream_update(self):
        self.progress.player_stream_update()
        return

    def set_handler(self, handler):
        self.btn_play.connect('primary-single-click', handler.on_player_play)
        self.btn_pause.connect('primary-single-click', handler.on_player_pause)
        self.btn_close.connect('primary-single-click', handler.on_player_stop)
        self.btn_volume.set_handler(handler)
        return

    def reset(self):
        self.player_stream_update()
        return
