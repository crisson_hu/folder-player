from gi.repository import Gdk, Gtk, PangoCairo

from tool import get_fmt_day_hour_minute_second
from dbuslite import store_lite
from base import WidgetBaseMixin


class PlayProgressBarNew(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'PlayProgressBarNew'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_size_request(2, 20)
        self.dr = self.Draw(self)
        return

    def do_draw(self, cr):
        context = self.get_style_context()
        context.save()
        self.dr.draw_bg(cr)
        self.dr.draw_text(cr)
        context.restore()
        return True

    def player_stream_update(self):
        self.queue_draw()
        return

    def set_visible(self, visible):
        super().set_visible(visible)
        return

    def set_no_show_all(self, no_show=True):
        super().set_no_show_all(no_show)
        return

    def set_value(self):
        self.queue_draw()
        return

    def set_range(self):
        self.queue_draw()
        return

    class Draw:

        def __init__(self, widget):
            self.widget = widget
            self.pango_layout = self.widget.create_pango_layout()
            # from gi.repository import Pango
            # desc = Pango.font_description_from_string('Monospace 11')
            # self.pango_layout.set_font_description(desc)
            self.rgba_finished = Gdk.RGBA(1/15, 10/15, 1/15, 1)
            self.rgba_remaining = Gdk.RGBA(15/15, 10/15, 1/15, 1)
            return

        def draw_bg(self, cr):
            width = self.widget.get_allocated_width()
            height = self.widget.get_allocated_height()
            duration = store_lite.player_stream_total_length
            position = store_lite.player_stream_current_position
            width_finished = (
                int(width * position / duration) if duration > 0 else
                0
            )

            cr.save()
            cr.rectangle(0, 0, width_finished, height)
            Gdk.cairo_set_source_rgba(cr, self.rgba_finished)
            cr.fill()
            cr.rectangle(width_finished, 0, width, height)
            Gdk.cairo_set_source_rgba(cr, self.rgba_remaining)
            cr.fill()
            cr.restore()
            return

        def draw_text(self, cr):
            cr.save()
            self.pango_layout.set_text(
                store_lite.player_stream_progress_label
            )
            cr.move_to(5, 0)
            # rgba = self.widget.get_style_context().get_color(
            #     self.widget.get_state_flags()
            # )
            rgba = Gdk.RGBA(15/15, 15/15, 15/15, 1)
            Gdk.cairo_set_source_rgba(cr, rgba)
            PangoCairo.show_layout(cr, self.pango_layout)
            cr.restore()
            return


class PlayProgressBarOld(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'PlayProgressBarOld'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()

        self.pb = Gtk.Scale()
        self.pb.set_sensitive(False)
        self.pb.set_draw_value(False)
        self.pb.set_has_origin(True)
        self.pb.set_digits(0)
        self.pb.set_value_pos(Gtk.PositionType.RIGHT)
        self.add(self.pb)
        self.child_set_property(self.pb, 'expand', True)

        self.lbl_cur = Gtk.Label()
        self.add(self.lbl_cur)
        self.child_set_property(self.lbl_cur, 'expand', False)
        self.lbl_max = Gtk.Label()
        self.add(self.lbl_max)
        self.child_set_property(self.lbl_max, 'expand', False)

        return

    def player_stream_update(self):
        self.set_range()
        self.set_value()
        return

    def set_visible(self, visible):
        for w in (self.pb, self.lbl_cur, self.lbl_max):
            w.set_visible(visible)
        return

    def set_no_show_all(self, no_show=True):
        for w in (self.pb, self.lbl_cur, self.lbl_max):
            w.set_no_show_all(no_show)
        return

    def set_value(self):
        self.pb.set_value(store_lite.player_stream_current_position)
        self.lbl_cur.set_label(
            store_lite.player_stream_current_position_second_fmt
        )
        return

    def set_range(self):
        self.pb.set_range(0, store_lite.player_stream_total_length)
        self.lbl_max.set_label('/{}'.format(
            store_lite.player_stream_total_length_second_fmt
        ))
        return


PlayProgressBar = PlayProgressBarOld
PlayProgressBar = PlayProgressBarNew
