from pathlib import Path
from gi.repository import Gdk, Gtk

from dbuslite import store_lite, Player as PlayerConstant
from tool import _, log_debug
from app.icons import IconButtonRepeatDisable, IconButtonRepeat
from app.menus.menumain import MenuMainLeft, MenuMainRight
from app.menus.menustatusrepeat import MenuStatusRepeat
from base import WidgetBaseMixin


class ButtonRepeat(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'ButtonRepeat'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self._store = store_lite
        self._setup_icons()
        self.update_repeat_mode()
        self._setup_signal()
        return

    def _setup_signal(self, handler=None):
        self.connect('button-press-event', self._show_menu)
        return self

    def _setup_icons(self):
        self._icon_none = IconButtonRepeatDisable().set_icon(
            'status/repeat-none.png'
        )

        inv = '-inv'
        self._icon_one = IconButtonRepeat().set_icon(
            'status/repeat-one{}.png'.format(inv)
        )
        self._icon_selected = IconButtonRepeat().set_icon(
            'status/repeat-selected{}.png'.format(inv)
        )
        self._icon_all = IconButtonRepeat().set_icon(
            'status/repeat-all{}.png'.format(inv)
        )

        for i in (
                self._icon_none,
                self._icon_one,
                self._icon_selected,
                self._icon_all
        ):
            self.add(i)
            i.set_no_show_all(True)
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def _set_tooltip(self, txt):
        tip = '{}: {}'.format(_('Repeat Mode'), txt)
        self.set_tooltip_markup(tip)
        self.set_tooltip_text(tip)
        return

    def update_repeat_mode(self):
        for i in self:
            i.set_visible(False)

        mode = self._store.player_repeat_mode
        icon = (
            self._icon_none if mode == PlayerConstant.REPEAT_NONE else
            self._icon_one if mode == PlayerConstant.REPEAT_CURRENT else
            self._icon_selected if mode == PlayerConstant.REPEAT_SELECTED else
            self._icon_all if mode == PlayerConstant.REPEAT_ALL else
            self._icon_none
        )
        self._set_tooltip(txt=_(mode))
        icon.set_visible(True)
        return icon

    def _show_menu(self, widget, ev):
        screen, rx, ry = ev.get_device().get_position()
        self.menu_repeat.popup(
            rx - ev.x + self.get_allocated_width(),
            ry - ev.y
        )
        return True

    @property
    def menu_repeat(self):
        if not hasattr(self, '_menu_repeat'):
            self._menu_repeat = MenuStatusRepeat().set_button(self)
        return self._menu_repeat
