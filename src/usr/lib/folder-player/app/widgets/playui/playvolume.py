from pathlib import Path
from gi.repository import Gtk

from tool import log_debug
from app.icons import IconButton
from base import WidgetBaseMixin


class PlayVolume(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'PlayVolume'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.btn_volume = IconButton().set_icon('volume/volume.png')
        self.btn_volume_mute = IconButton().set_icon('volume/mute.png')
        self.btn_vup = IconButton().set_icon('volume/arrowup.png')
        self.btn_vdown = IconButton().set_icon('volume/arrowdown.png')

        self.volume_label = Gtk.Label()

        self.vbox_updown = Gtk.VBox()
        self.vbox_updown.add(self.btn_vup)
        self.vbox_updown.add(self.btn_vdown)

        for i in (
                self.btn_volume,
                self.btn_volume_mute,
                self.volume_label,
                self.vbox_updown,
        ):
            i.set_no_show_all(True)
            i.set_property('has-tooltip', True)
            self.add(i)
            self.child_set_property(i, 'expand', False)
            self.child_set_property(i, 'fill', False)
            self.child_set_property(i, 'padding', 0)

        self.set_visible(False)

        self._handler = None
        self._mainview = None

        return

    def set_visible(self, visible=True):
        for i in (
                self.vbox_updown,
                self.btn_vup,
                self.btn_vdown,
                self.volume_label,
        ):
            i.set_visible(visible)

        for i in (
                self.btn_volume,
                self.btn_volume_mute,
        ):
            i.set_visible(False)

        if visible:
            (
                self.btn_volume if not self.mute else
                self.btn_volume_mute
            ).set_visible(visible)

        return

    def set_volume(self, volume=0):
        txt = volume
        if type(volume) in (int, float):
            txt = '{:.1f}'.format(volume)
        self.volume_label.set_text(txt)
        return

    def set_volume_mute(self, mute=False):
        self.mute = mute
        return

    def get_volume(self):
        return self.volume_label.get_text()

    def set_handler(self, handler):
        self._handler = handler

        for i in (
                self.btn_volume,
                self.btn_volume_mute,
                self.btn_vup,
                self.btn_vdown,
        ):
            i.connect('query-tooltip', handler.on_volume_tooltip)

        self.btn_volume.connect('pressed', self._on_volume_set_mute)
        self.btn_volume_mute.connect('pressed', self._on_volume_set_unmute)
        self.btn_vup.connect('pressed', handler.on_player_volume_up)
        self.btn_vdown.connect('pressed', handler.on_player_volume_down)
        return self

    def set_mainview(self, mainview):
        self._mainview = mainview
        return

    @property
    def mute(self):
        return (
            False if not self._mainview else
            self._mainview.player_volume_get_mute()
        )

    @mute.setter
    def mute(self, mute):
        if self.btn_volume.is_visible() or self.btn_volume_mute.is_visible():
            self.btn_volume_mute.set_visible(mute)
            self.btn_volume.set_visible(not mute)
        return

    def _on_volume_set_mute(self, *args):
        self.mute = True
        return self._handler.on_player_volume_set_mute()

    def _on_volume_set_unmute(self, *args):
        self.mute = False
        return self._handler.on_player_volume_set_unmute()
