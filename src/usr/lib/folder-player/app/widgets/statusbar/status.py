from pathlib import Path
from gi.repository import Gtk

from dbuslite import store_lite
from tool import log_debug, _
from base import WidgetBaseMixin
from app.constants import Constants
from app.plugins.datetime.clock import clock
from app.widgets.playui.buttonrepeat import ButtonRepeat
from .buttonmenu import ButtonMenu
from app.widgets.playui.buttonplayer import ButtonPlayer


class Status(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'Status'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self._store = store_lite
        self.set_can_focus(False)
        self.set_can_default(False)
        self.setup()
        return

    def set_container(self, list_box=None):
        self.list_box = list_box
        return self

    def set_last_longest_dir(self, lldir):
        self.lldir = lldir
        return self

    def set_mainview(self, mainview):
        self._mainview = mainview
        for w in (
                self._button_menu,
                self._button_repeat,
                self._button_player,
        ):
            w.set_mainview(self._mainview)
        return self

    def setup(self):
        self.set_property('spacing', 1)

        self._button_menu = ButtonMenu().setup_signal()
        self.add(self._button_menu)
        self._button_menu.set_property('margin', 0)
        self._button_menu.set_property('margin-right', 20)

        self.clock = clock
        self.pack_end(self.clock, False, False, 0)

        hbox = Gtk.HBox()
        self.pack_end(hbox, True, False, 0)
        self._button_repeat = ButtonRepeat()
        hbox.pack_end(self._button_repeat, True, False, 0)
        self._button_player = ButtonPlayer()
        hbox.pack_end(self._button_player, True, False, 0)

        for w in self.get_children():
            self.child_set_property(w, 'expand', False)

        self.child_set_property(hbox, 'expand', True)
        self.child_set_property(hbox, 'fill', False)

        return self
