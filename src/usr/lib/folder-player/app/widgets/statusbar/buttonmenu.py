from pathlib import Path

from tool import log_debug, _
from app.icons import IconButton
from app.menus.menustatusmain import MenuStatusMain


class ButtonMenu(IconButton):

    __gtype_name__ = 'ButtonMenu'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_icon('status/dottedmenu.png')
        self._set_tooltip()
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        self.menu_right.set_mainview(self._mainview)
        return self

    def setup_signal(self, handler=None):
        self._handler = handler
        self.connect('primary-single-click', self._show_menu)
        self.connect('secondary-single-click', self._show_menu)
        return self

    def _set_tooltip(self):
        tip = _('Components')
        self.set_tooltip_text(tip)
        self.set_tooltip_markup(tip)
        return self

    def _show_scope(self, scope_name=None):
        if scope_name is None:
            return True
        return True

    def _show_menu(self, gmp, n_press, x, y):
        screen, rx, ry = gmp.get_device().get_position()
        self.menu_right.popup(
            rx - x,
            ry - y
        )
        return True

    @property
    def menu_right(self):
        if not hasattr(self, '_menu_right'):
            self._menu_right = MenuStatusMain()
        return self._menu_right
