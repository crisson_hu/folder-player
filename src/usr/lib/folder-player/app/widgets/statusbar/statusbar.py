from gi.repository import Gtk

from tool import log_debug
from base import WidgetBaseMixin
from app.constants import Constants
from app.widgets.playui.videocontrol import VideoControl
from .status import Status


class StatusBar(Gtk.VBox, WidgetBaseMixin):

    __gtype_name__ = 'StatusBar'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_property('hexpand', False)
        self.set_property('vexpand', False)
        self._setup()
        self.set_playerui_visible(False)
        return

    def clear(self):
        for i in super().get_children():
            super().remove(i)
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        self._bar_status.set_mainview(self._mainview)
        return self

    def _setup(self):
        self._bar_playerui = VideoControl()
        # self.add(self._bar_playerui)
        self.child_set_property(self._bar_playerui, 'expand', False)

        self._bar_status = Status()
        self.add(self._bar_status)
        self.child_set_property(self._bar_status, 'expand', False)
        return

    def set_playerui_visible(self, visible=True):
        log_debug('visible {}'.format(visible))
        return
        uilist = (self._bar_playerui,)
        for w in uilist:
            w.set_no_show_all(not visible)
            if visible:
                w.show_all()
            w.set_visible(visible)
        return
