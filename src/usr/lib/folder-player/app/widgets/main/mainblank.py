from gi.repository import Gdk, Gtk

from tool import log_debug, _
from dbuslite import store_lite
from base import WidgetBaseMixin
from app.menus.menublank import MenuBlank


class MainBlank(Gtk.Misc, WidgetBaseMixin):

    __gtype_name__ = 'MainBlank'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        event_mask = (
            Gdk.EventMask.POINTER_MOTION_MASK |
            Gdk.EventMask.EXPOSURE_MASK |
            Gdk.EventMask.STRUCTURE_MASK |
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK |
            Gdk.EventMask.KEY_PRESS_MASK |
            Gdk.EventMask.KEY_RELEASE_MASK |
            Gdk.EventMask.TOUCH_MASK |
            Gdk.EventMask.FOCUS_CHANGE_MASK |
            Gdk.EventMask.ENTER_NOTIFY_MASK |
            Gdk.EventMask.LEAVE_NOTIFY_MASK |
            Gdk.EventMask.VISIBILITY_NOTIFY_MASK
        )
        self.set_events(
            self.get_events() | event_mask
        )
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def do_button_press_event(self, ev):
        if ev.button == Gdk.BUTTON_PRIMARY:
            self._mainview._win.unselect_all()
        elif ev.button == Gdk.BUTTON_SECONDARY:
            self.show_menu(ev.x_root, ev.y_root)
        return

    def show_menu(self, x=-1, y=-1):
        if not hasattr(self, '_menu'):
            self._menu = MenuBlank().set_mainblank(self)
        if x == -1 or y == -1:
            log_debug(('Error', x, y))
            return False

        self._menu.popup(x, y)

        return True

    def ops_copy_location(self, *args):
        clipboard = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        log_debug('copied location: ' + store_lite.last_dir)
        clipboard.set_text(store_lite.last_dir, -1)
        return True

    def ops_refresh(self, *args):
        self._mainview.list_directory()
        return True
