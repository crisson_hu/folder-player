import os
from pathlib import Path
from gi.repository import Gdk, Gtk

from dbuslite import store_lite
from tool import log_debug, _
from base import ButtonBaseMixin, WidgetBaseMixin
from app.icons import IconButton
from app.widgets.main.mainmenu import MainMenu
from app.menus.menuaddressbaritem import MenuAddressBarItem
from app.theme import StyleClassConstants, set_prelight


class AddressBar(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'AddressBar'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_can_focus(False)
        self.set_can_default(False)
        self._mainview = None
        self.address = None
        self.lldir = None
        self.address_lldir = None
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def set_last_longest_dir(self, lldir):
        self.lldir = lldir
        return self

    def _clear(self):
        for i in super().get_children():
            super().remove(i)
        return

    def setup(self, directory):
        if directory is None:
            raise ValueError(
                'Param `directory` should be full path, not `None`.'
            )

        self._clear()

        self.address = directory.split('/')
        self.address[0] = '/'
        self.address = list(i for i in self.address if i)

        self.address_lldir = self.address[:]
        if self.lldir.startswith(
                directory if directory.endswith(os.sep) else
                directory + os.sep
        ):
            self.address_lldir += list(
                i for i in self.lldir[len(directory):].split('/')
                if i
            )

        self.set_property('spacing', 1)

        self._setup_menu()

        btn = None
        n_items = len(self.address)
        for index in range(len(self.address_lldir)):
            self._add_address_item(
                AddressBarItemBefore()
                if index < n_items - 1 else
                AddressBarItemCurrent()
                if index == n_items - 1 else
                AddressBarItemAfter(),
                index
            )

        self._setup_signal(self._mainview.handler)

        return self

    def _setup_menu(self):
        self.btn_menu = MainMenu().set_mainview(self._mainview)
        self.add(self.btn_menu)
        self.child_set_property(self.btn_menu, 'expand', False)
        self.btn_menu.set_margin_start(15)
        self.btn_menu.set_margin_end(15)
        return

    def _setup_signal(self, handler=None):
        self._handler = handler
        self.btn_menu.setup_signal(handler=handler)
        return self

    def show_main_menu(self, *args):
        return self.btn_menu.show_main_menu(*args)

    def _add_address_item(self, item, index):
        btn = (
            item
            .set_index(index)
            .set_text(self.address_lldir[index])
            .set_address('/' + '/'.join(self.address_lldir[1:index+1]))
            .set_mainview(self._mainview)
        )
        self.add(btn)
        self.child_set_property(btn, 'expand', False)
        return


class AddressBarItem(Gtk.EventBox, WidgetBaseMixin, ButtonBaseMixin):

    __gtype_name__ = 'AddressBarItem'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.label = Gtk.Label()
        self.add(self.label)
        self.set_can_focus(False)
        self.set_can_default(False)
        self.label.set_property('margin', 0)
        self.label.set_property('margin-left', 15)
        self.label.set_property('margin-right', 15)
        # self.set_property('margin-left', 1)
        # self.set_property('margin-right', 1)
        self.index = -1

        self.gmp = Gtk.GestureMultiPress.new(self)
        self.gmp.set_button(0)
        self.gmp.set_exclusive(True)
        self._ges_sid = self.gmp.connect(
            'pressed', self._on_press, *args
        )

        self.set_cursor_hand1()

        return

    def do_enter_notify_event(self, ev):
        set_prelight(self, True)
        set_prelight(self.label, True)
        return

    def do_leave_notify_event(self, ev):
        set_prelight(self, False)
        set_prelight(self.label, False)
        return

    def set_index(self, index):
        self.index = index
        return self

    def set_text(self, label):
        self.label.set_markup(label)
        self.set_tooltip_markup(label)
        return self

    def set_address(self, address):
        self._address = address
        StyleClassConstants.set_favorite_address_item(
            self,
            fav=(
                True if self._address in store_lite.bookmark else
                False
            )
        )
        return self

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def connect(self, sig_name, cb, *args):
        if sig_name != 'pressed':
            sig_id = super().connect(sig_name, cb, *args)
        else:
            sig_id = self._ges_sid
        return sig_id

    def _on_press(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 1:
            self._mainview.list_directory(self._address)
        elif button == Gdk.BUTTON_SECONDARY and n_press == 1:
            menu = MenuAddressBarItem() \
                   .set_address(self._address) \
                   .set_mainview(self._mainview)

            _s, rx, ry = self.gmp.get_device().get_position()
            menu.popup(
                rx - x,
                ry - y + self.get_allocated_height()
            )
        return True


class AddressBarItemBefore(AddressBarItem):

    __gtype_name__ = 'AddressBarItemBefore'
    __extra_style_classes__ = ['AddressBarItem']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_tooltip_markup(self, tooltip):
        super().set_tooltip_markup(
            '{}"{}"'.format(
                _('Click to switch to directory '),
                tooltip
            )
        )
        return self

    def set_text(self, label):
        super().set_text('<b>{}</b>'.format(label))
        return self


class AddressBarItemCurrent(AddressBarItem):

    __gtype_name__ = 'AddressBarItemCurrent'
    __extra_style_classes__ = ['AddressBarItem']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_tooltip_markup(self, tooltip):
        super().set_tooltip_markup(
            '{}"{}"'.format(
                _('Current Location'),
                tooltip
            )
        )
        return self

    def set_text(self, label):
        super().set_text('<b>{}</b>'.format(label))
        return self


class AddressBarItemAfter(AddressBarItem):

    __gtype_name__ = 'AddressBarItemAfter'
    __extra_style_classes__ = ['AddressBarItem']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_tooltip_markup(self, tooltip):
        super().set_tooltip_markup(
            '{}"{}"'.format(
                _('Click to switch to directory '),
                tooltip
            )
        )
        return self
