from gi.repository import Gtk

from tool import log_debug
from base import WidgetBaseMixin
from ..files import FilesList


class MainListBox(Gtk.ListBox, WidgetBaseMixin):

    __gtype_name__ = 'MainListBox'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.connect('row-selected', self._on_row_selected)
        self._file_item_list = FilesList()
        self._row_item_list = []
        self._row_last_selected = None
        self._current_playing_item = None
        self._match_item = None
        return

    def set_mainview(self, mainview=None):
        self._mainview = mainview
        (
            self._file_item_list
            .set_mainview(mainview)
            .set_container(self)
            .set_handler(mainview.handler)
        )
        return self

    def change_directory(self, directory=None):
        self._refresh_dir(directory)

    def _on_row_selected(self, *args):
        row = self.get_selected_row()
        if not row:
            return True
        item = self._row_item_list[row.get_index()]
        if self._row_last_selected:
            self._row_last_selected.set_selected(False)
        self._row_last_selected = item
        self._row_last_selected.set_selected(True)
        return True

    def unselect_all(self, *args):
        row = self.get_selected_row()
        if not row:
            return True
        if self._row_last_selected is not None:
            self._row_last_selected.set_selected(False)
            self._row_last_selected = None
        super().unselect_all(*args)
        return True

    def selected_item_show_menu(self):
        if self._row_last_selected is None:
            return
        self._row_last_selected.show_menu()
        return

    def set_main_scrolled_window(self, sw):
        self.vsb = sw.get_vscrollbar()
        self.hsb = sw.get_hscrollbar()
        return self

    def set_match_item(self, item=None):
        self._match_item = item
        return self

    def scroll_to_item(self):
        upper = self.vsb.get_adjustment().get_upper()
        lower = self.vsb.get_adjustment().get_lower()
        value = lower + (upper - lower) * (
            0 if self._match_item is None else
            self._match_item.get_row_index() / len(self.get_children())
        )
        self.vsb.set_value(value)
        self.hsb.set_value(value)

        # log_debug((
        #     [
        #         lower,
        #         value,
        #         upper,
        #     ],
        #     [
        #         self._match_item.get_row_index() if self._match_item else
        #         None,
        #         len(self.get_children()),
        #     ]
        # ))

        return True

    def set_play_pause(self, play=False):
        if self._current_playing_item is not None:
            self._current_playing_item.set_play_pause(play=play)
        return True

    def set_progress(self, position=0):
        if self._current_playing_item is not None:
            self._current_playing_item.set_progress(position=position)
        return True

    def player_stream_update(self):
        if self._current_playing_item is not None:
            self._current_playing_item.player_stream_update()
        return True

    def insert(self, child, position):
        super().insert(child, position)
        self._row_item_list.append(child)
        return

    def _refresh_dir(self, cur_dir=None):
        self._file_item_list.task_stop()
        for i in self._row_item_list[:]:
            self.remove(i)
        self._file_item_list.task_start(cur_dir)
        return

    def remove(self, widget):
        super().remove(widget.get_row())
        try:
            self._row_item_list.remove(widget)
        except ValueError as e:
            log_debug(widget)
            log_debug(e)
        return
