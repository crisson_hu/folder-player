from gi.repository import Gtk

from tool import log_debug
from base import WidgetBaseMixin
from .addressbar import AddressBar
from dbuslite import store_lite


class MainListAddress(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'MainListAddress'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_property('hexpand', False)
        self.set_property('vexpand', False)
        self._store = store_lite
        self._addressbar = AddressBar()
        vp = Gtk.Viewport()
        vp.add(self._addressbar)
        sw = Gtk.ScrolledWindow()
        sw.add(vp)
        self.add(sw)
        # NOTE: make sure the address bar is visible.
        self.set_size_request(1, 30)
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        self._addressbar.set_mainview(mainview)
        return self

    def setup(self, directory):
        self._addressbar.set_last_longest_dir(self._store.last_longest_dir) \
            .setup(directory)
        return self

    def show_main_menu(self, *args):
        return self._addressbar.show_main_menu(*args)
