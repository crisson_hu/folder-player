from gi.repository import Gtk, Gdk

from dbuslite import store_lite
from tool import (
    _,
    APP,
    APP_WMCLASS,
    APP_DIR,
    APP_NAME,
    log_debug,
)
from app.theme import setup_css
from app.constants import Constants
from base import WidgetBaseMixin
from app.view import ViewDict, ViewFolder, ViewVideo


class MainWindow(Gtk.ApplicationWindow, WidgetBaseMixin):

    __gtype_name__ = 'MainWindow'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_can_focus(False)
        self.set_skip_taskbar_hint(False)
        self.set_skip_pager_hint(False)
        self.set_hide_titlebar_when_maximized(False)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_default_icon_from_file(str(
            APP_DIR / 'ui' / 'folder-player.png'
        ))

        self._mainview = None
        self._store = store_lite
        self._store.set_window(self)

        self._setup_wm()
        self._setup_widgets()
        self._setup_css()
        self.set_title()
        self._title = ''

        return

    def mainwin_ui_switch_view(self, ui_view=Constants.UiView.NORMAL):
        ui_page = None
        msg = None
        if ui_view == Constants.UiView.NORMAL:
            ui_page = self.mainwin_ui_view_folder
            msg = 'NORMAL'
        elif ui_view == Constants.UiView.VIDEO:
            ui_page = self.mainwin_ui_view_video
            msg = 'VIDEO'
        elif ui_view == Constants.UiView.RUN_DICT:
            ui_page = self._ui_run_dict
            msg = 'RUN-DICT'
        else:
            log_debug(ui_view)
            raise NameError('ui_view [{}] not found!'.format(ui_view))

        log_debug('switch ui: {}'.format(msg))
        self.mainwin_ui_view_setter(ui_view)

        # Switch to ui_page.
        page_num = self.notebook.page_num(ui_page)
        if page_num == -1:
            self.notebook.append_page(ui_page)
            page_num = self.notebook.page_num(ui_page)
        #
        # According to the "Gtk+ 3 Reference Manual", it's better to
        # make ui_page visible before switching to a page. As a matter
        # of fact, it's necessary for some version of new Gtk library.
        #
        ui_page.show_all()
        self.notebook.set_current_page(page_num)

        self.reset_window_state()
        return

    @property
    def mainwin_ui_view(self):
        if not hasattr(self, '_mainwin_ui_view'):
            self._mainwin_ui_view = Constants.UiView.NORMAL
        return self._mainwin_ui_view

    def mainwin_ui_view_setter(self, ui_view):
        self._mainwin_ui_view = ui_view
        return

    def reset_window_state(self):
        self.set_decorated(True)
        return

    def setup_signal(self, handler):
        self.connect('delete-event', handler.on_window_delete)
        self.connect('key-press-event', handler.on_key_press)
        self.mainwin_ui_view_video.set_handler(handler)
        return self

    def setup(self, mainview=None):
        self._mainview = mainview
        self.mainwin_ui_view_video.set_mainview(mainview, top_window=self)
        self.mainwin_ui_view_folder.set_mainview(self._mainview)
        self._ui_run_dict.set_mainview(self._mainview)
        self._window_move_resize()
        return self

    def set_title(self, title=None, progress=None):
        if title:
            self._title = title
        else:
            self._title = (
                self._mainview.current_item.get_pathitem().name
                if self._mainview and self._mainview.current_item
                else ''
            )
        text = self._title

        if progress:
            text = '[ ' + progress + ' ] ' + text

        if text:
            text = text + ' | ' + _(APP_NAME)
        else:
            text = _(APP_NAME)

        super().set_title(_(text))
        return text

    def change_directory(self, directory):
        self.mainwin_ui_view_folder.change_directory(directory=directory)
        return

    def show_main_menu(self, *args):
        return self.mainwin_ui_view_folder.show_main_menu(*args)

    def unselect_all(self):
        self.mainwin_ui_view_folder.list_box.unselect_all()
        return True

    def _setup_wm(self):
        self.set_wmclass(APP_WMCLASS, APP_WMCLASS)
        return

    def _setup_css(self):
        setup_css()
        return

    def _window_move_resize(self):
        self.set_default_size(700, 600)
        x = self._store.window_x
        y = self._store.window_y
        w = self._store.window_width
        h = self._store.window_height
        if x and y and w and h:
            self.move(x, y)
            self.resize(w, h)
        if self._store.is_maximized:
            self.maximize()
        return

    def _setup_widgets(self):
        # '''Dict Window'''
        self._ui_run_dict = ViewDict()

        # top container
        self.notebook = Gtk.Notebook()
        self.notebook.set_show_border(False)
        self.notebook.set_show_tabs(False)

        vbox = Gtk.VBox()
        self.add(vbox)
        vbox.add(self.notebook)
        self.notebook.append_page(self.mainwin_ui_view_folder)
        self.notebook.append_page(self.mainwin_ui_view_video)
        self.notebook.append_page(self._ui_run_dict)
        self.mainwin_ui_view_video.realize()
        self.mainwin_ui_view_folder.realize()
        self._ui_run_dict.realize()

        return

    def toggle_fullscreen(self):
        if not hasattr(self, '_is_fullscreen'):
            self._is_fullscreen = False
        self._is_fullscreen = not self._is_fullscreen
        if self._is_fullscreen:
            self.fullscreen()
        else:
            self.unfullscreen()
        return

    @property
    def mainwin_ui_view_folder(self):
        if not hasattr(self, '_mainwin_ui_view_folder'):
            self._mainwin_ui_view_folder = ViewFolder()
        return self._mainwin_ui_view_folder

    @property
    def mainwin_ui_view_video(self):
        if not hasattr(self, '_mainwin_ui_view_video'):
            self._mainwin_ui_view_video = ViewVideo()
        return self._mainwin_ui_view_video

    def do_destroy(self):
        log_debug()
        super().destroy()
        return
