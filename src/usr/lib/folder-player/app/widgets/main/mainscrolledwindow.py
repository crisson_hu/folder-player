from gi.repository import Gtk

from tool import log_debug
from base import WidgetBaseMixin


class MainScrolledWindow(Gtk.ScrolledWindow, WidgetBaseMixin):

    __gtype_name__ = 'MainScrolledWindow'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        return
