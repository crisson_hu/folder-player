from pathlib import Path

from tool import log_debug, _
from app.icons import IconButton
from app.menus.menumain import MenuMainLeft, MenuMainRight


class MainMenu(IconButton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_icon('menu.png')
        self._set_tooltip()
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def setup_signal(self, handler=None):
        self._handler = handler
        self.connect('primary-single-click', self._show_menu)
        # self.connect('primary-single-click', self._show_main_scope)
        # self.connect('secondary-single-click', self._show_menu)
        return self

    def _set_tooltip(self):
        tip = _('Main Menu')
        self.set_tooltip_markup(tip)
        self.set_tooltip_text(tip)
        return self

    def _show_scope(self, scope_name=None):
        if scope_name is None:
            return True
        return True

    def _show_main_scope(self, gmp, n_press, x, y):
        screen, rx, ry = gmp.get_device().get_position()
        self.menu_left.popup(
            rx - x + self.get_allocated_width(),
            ry - y
        )
        return True

    def _show_menu(self, gmp, n_press, x, y):
        screen, rx, ry = gmp.get_device().get_position()
        self.menu_right.popup(
            rx - x,
            ry - y + self.get_allocated_height()
        )
        return True

    def show_main_menu(self, widget, ev):
        rx, ry, rw, rh = self.get_window().get_geometry()
        x, y = self._mainview.mainwin.get_window().get_position()
        self.menu_right.popup(
            x + rx,
            y + ry + rh
        )
        return True

    @property
    def menu_left(self):
        if not hasattr(self, '_menu_left'):
            self._menu_left = MenuMainLeft() \
                .set_mainview(self._mainview) \
                .setup_signal(handler=self._handler)

        return self._menu_left

    @property
    def menu_right(self):
        if not hasattr(self, '_menu_right'):
            self._menu_right = MenuMainRight() \
                .set_mainview(self._mainview) \
                .setup_signal(handler=self._handler)
        return self._menu_right
