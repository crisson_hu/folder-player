from gi.repository import Gtk, Gdk

from tool import log_debug
from base import WidgetBaseMixin


class MainDraw(Gtk.DrawingArea, WidgetBaseMixin):

    __gtype_name__ = 'MainDraw'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        return

    def reset_drawing(self):
        log_debug()
        surface = self.get_window()
        cr = Gdk.cairo_create(surface)

        cr.save()
        cr.rectangle(
            0,
            0,
            self.get_allocated_width(),
            self.get_allocated_height()
        )
        Gdk.cairo_set_source_rgba(
            cr,
            Gdk.RGBA(0, 0, 0, 1.0)
        )
        cr.fill()
        cr.restore()

        return True
