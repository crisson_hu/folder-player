from gi.repository import Gtk

from tool import HotKey, log_debug
from . import PathInput


class PathCompletion(Gtk.VBox):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pi = PathInput()
        self.add(self._pi)

    def set_path_text(self, path_text):
        self._path_text = path_text
        self._pi.set_text(self._path_text).caret_move_end()
        return self
