from gi.repository import Gtk, Gdk, Pango, PangoCairo

from tool import HotKey, log_debug


class InputText(Gtk.Widget):

    __gtype_name__ = 'InputText'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.style = self.Style(self)
        self.set_size_request(150, 24)

        self.struct = self.Structure(self)
        self.dr = self.Draw(self)
        self.set_can_focus(True)

    class Structure:

        CARET_TYPE_BAR = 'caret-type-bar'
        CARET_TYPE_BOX = 'caret-type-box'

        def __init__(self, parent):
            self.widget = parent
            self.caret_type = self.CARET_TYPE_BAR
            # The caret_index is in number of bytes.
            self.caret_index = 0
            self.scroll_x = 0

        def caret_is_bar(self):
            return self.caret_type == self.CARET_TYPE_BAR

        def caret_is_box(self):
            return self.caret_type == self.CARET_TYPE_BOX

        def caret_set(self, index=0):
            if self.caret_index != index:
                self.caret_index = index
                self.widget.queue_draw()

        def caret_get(self):
            return self.caret_index

        def caret_get_extents(self):
            '''Return caret's (x, scroll_x, width).'''
            layout = self.widget.ensure_layout()
            (layout_w, layout_h) = self.text_get_size()
            if layout_w <= 0:
                if self.caret_is_bar():
                    return (0, 0, 1)
                return (0, 0, 0)

            (pos, _pos) = layout.get_cursor_pos(self.caret_get())
            cx = pos.x / Pango.SCALE
            total_char = len(layout.get_text().encode('utf-8'))
            # CARET_TYPE_BOX
            cw = layout_w / total_char
            if self.caret_get() < total_char:
                (pos2, _pos2) = layout.get_cursor_pos(self.caret_get() + 1)
                cw = (pos2.x - pos.x) / Pango.SCALE
            # CARET_TYPE_BAR
            if self.caret_is_bar():
                cw = 1
            (tx, ty, tw, th) = self.text_area_get_text_geometry()
            if (cx + cw) <= self.scroll_x or (cx + cw) - self.scroll_x > tw:
                self.scroll_x = (cx + cw) - (cx + cw) % tw
            if (cx + cw) % tw < cw:
                self.scroll_x = cx
            return (cx, - self.scroll_x, cw)

        def _next_caret_index(self, right=True):
            result = True
            step = 0
            next_char = ''
            tot_bytes = self.widget.ensure_layout().get_text().encode('utf-8')
            for step in (1, 2, 3, 4):
                try:
                    step *= 1 if right else -1
                    il = self.caret_index if right else self.caret_index + step
                    ir = self.caret_index + step if right else self.caret_index
                    next_char = tot_bytes[il:ir].decode('utf-8')
                    raise AssertionError
                except UnicodeDecodeError:
                    pass
                except AssertionError:
                    break
                except Exception as e:
                    log_debug(e)
                    result = False
                    break
                else:
                    result = False

            # log_debug(next_char)
            if step == 0:
                result = False
            return (result, step, next_char)

        def caret_move(self, right=True, begin=False, end=False):
            '''Returns True on success, otherwise returns False.'''
            layout = self.widget.ensure_layout()
            total_len = len(layout.get_text().encode('utf-8'))
            if begin:
                self.caret_index = 0
                self.widget.queue_draw()
                return True
            elif end:
                self.caret_index = total_len
                self.widget.queue_draw()
                return True

            (result, step, next_char) = self._next_caret_index(right=right)
            if not result:
                return False

            if self.caret_index + (step) >= 0 and \
                    self.caret_index + (step) <= total_len:
                self.caret_index += (step)
                self.widget.queue_draw()
                return True
            return False

        def text_get_size(self):
            return self.widget.ensure_layout().get_pixel_size()

        def text_area_get_geometry(self):
            width = self.widget.get_allocated_width()
            height = self.widget.get_allocated_height()
            margin_x = 5
            margin_y = 0
            x = margin_x
            y = margin_y
            w = width - margin_x * 2
            h = height - margin_y * 2
            return (x, y, w, h)

        def text_area_get_text_geometry(self):
            (x, y, w, h) = self.text_area_get_geometry()
            border_x = 0
            border_y = 0
            padding_x = 10
            padding_y = 0

            tx = x + border_x + padding_x
            ty = y + border_y + padding_y
            tw = w - tx - (border_x + padding_x)
            th = h - ty - (border_y + padding_y)
            return (tx, ty, tw, th)

        def kv_text(self, kv):
            index = self.caret_get()
            text = self.widget.get_text()
            text = '{}{:c}{}'.format(
                text[:index], kv, text[index:]
            )
            self.widget.set_text(text)
            self.caret_move(right=True)
            self.widget.queue_draw()
            return True

        def kv_delete_char(self, back=True):
            text = self.widget.get_text().encode('utf-8')
            if back:
                if not self.caret_move(right=False):
                    return False

            (result, step, next_char) = self._next_caret_index(right=True)
            if not result:
                return False
            index = self.caret_get()
            text = '{}{}'.format(
                text[:index].decode('utf-8'),
                text[index + step:].decode('utf-8')
            )
            self.widget.set_text(text)
            self.widget.queue_draw()
            return True

        def kv_delete_to_extent(self, begin=True):
            index = self.caret_get()
            if begin:
                text = self.widget.get_text()[index:]
                self.caret_move(begin=True)
            else:
                text = self.widget.get_text()[:index]

            self.widget.set_text(text)
            self.widget.queue_draw()
            return True

    def do_draw(self, cr):
        context = self.get_style_context()
        context.save()
        if not self.has_focus():
            if hasattr(self, 'state_prelight') and self.state_prelight:
                context.set_state(Gtk.StateFlags.PRELIGHT)
                self.set_state_flags(Gtk.StateFlags.PRELIGHT, False)
            else:
                self.unset_state_flags(Gtk.StateFlags.PRELIGHT)

        self.dr.draw_bg(cr)
        self.dr.draw_frame(cr)
        self.dr.draw_text(cr)
        context.restore()
        return True

    def ensure_layout(self):
        if not hasattr(self, 'pango_layout') or not self.pango_layout:
            self.pango_layout = self.create_pango_layout()
            desc = Pango.font_description_from_string('Monospace 11')
            self.pango_layout.set_font_description(desc)

        return self.pango_layout

    def set_text(self, text=None):
        if not text:
            text = ''
            self.struct.caret_move(right=False, begin=True)
        layout = self.ensure_layout()
        layout.set_text(text, len(text.encode('utf-8')))
        self.queue_draw()

    def modify_font(self, desc=None):
        if desc:
            self.ensure_layout().set_font_description(desc)

    def get_text(self):
        return self.ensure_layout().get_text()

    def do_key_press_event(self, ev):
        kv = ev.keyval
        ks = ev.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)

        if HotKey.is_right(kv) or \
                (k_ctrl and (kv == Gdk.KEY_f or kv == Gdk.KEY_F)):
            self.struct.caret_move(right=True)
            return True
        elif HotKey.is_left(kv) or \
                (k_ctrl and (kv == Gdk.KEY_b or kv == Gdk.KEY_B)):
            self.struct.caret_move(right=False)
            return True
        elif HotKey.is_backspace(kv) or \
                (k_ctrl and (kv == Gdk.KEY_h or kv == Gdk.KEY_H)):
            self.struct.kv_delete_char(back=True)
            return True
        elif HotKey.is_delete(kv) or \
                (k_ctrl and (kv == Gdk.KEY_d or kv == Gdk.KEY_D)):
            self.struct.kv_delete_char(back=False)
            return True
        elif k_ctrl and (kv == Gdk.KEY_u or kv == Gdk.KEY_U):
            self.struct.kv_delete_to_extent()
            return True
        elif k_ctrl and (kv == Gdk.KEY_k or kv == Gdk.KEY_K):
            self.struct.kv_delete_to_extent(begin=False)
            return True
        elif HotKey.is_end(kv) or \
                (k_ctrl and (kv == Gdk.KEY_e or kv == Gdk.KEY_E)):
            self.struct.caret_move(end=True)
            return True
        elif HotKey.is_home(kv) or \
                (k_ctrl and (kv == Gdk.KEY_a or kv == Gdk.KEY_A)):
            self.struct.caret_move(begin=True)
            return True
        elif k_ctrl or k_alt:
            pass
        elif kv >= 0x0 and kv <= 0x7f:
            return self.struct.kv_text(kv)

        return False

    class Draw:

        def __init__(self, widget):
            self.widget = widget
            self.struct = widget.struct
            self.caret_show_always = True

        def draw_bg(self, cr):
            width = self.widget.get_allocated_width()
            height = self.widget.get_allocated_height()
            cr.save()
            cr.rectangle(0, 0, width, height)
            rgba = Gdk.RGBA(14/15, 14/15, 14/15, 1)
            Gdk.cairo_set_source_rgba(cr, rgba)
            cr.fill()
            cr.restore()

        def draw_frame(self, cr):
            context = self.widget.get_style_context()

            cr.save()
            (x, y, w, h) = self.struct.text_area_get_geometry()
            Gtk.render_background(context, cr, x, y, w, h)
            Gtk.render_frame(context, cr, x, y, w, h)
            cr.restore()

        def draw_text(self, cr):
            cr.save()
            (tx, ty, tw, th) = self.struct.text_area_get_text_geometry()
            cr.rectangle(tx, ty, tw, th)
            cr.clip()

            self.draw_text_with_color(cr)
            if self.caret_show_always or self.widget.has_focus():
                self.draw_caret(cr)

            cr.restore()
            return True

        def draw_text_with_color(self, cr):
            layout = self.widget.ensure_layout()
            context = self.widget.get_style_context()
            (tx, ty, tw, th) = self.struct.text_area_get_text_geometry()
            (layout_w, layout_h) = self.struct.text_get_size()
            (cx, scroll_x, cw) = self.struct.caret_get_extents()

            cr.save()

            # centering
            cr.move_to(scroll_x + tx, ty + th / 2 - layout_h / 2)
            Gdk.cairo_set_source_rgba(
                cr,
                context.get_color(self.widget.get_state_flags())
            )
            PangoCairo.show_layout(cr, layout)

            cr.restore()

        def draw_caret(self, cr):
            layout = self.widget.ensure_layout()
            context = self.widget.get_style_context()
            (tx, ty, tw, th) = self.struct.text_area_get_text_geometry()
            (layout_w, layout_h) = self.struct.text_get_size()
            (cx, scroll_x, cw) = self.struct.caret_get_extents()
            if cw <= 0:
                cw = layout_h / 2
            x = tx + cx + 0.5
            y = ty + th / 2 - layout_h / 2 + 0.5
            w = cw - 1
            h = layout_h - 1

            cr.save()

            rgba = context.get_color(self.widget.get_state_flags())
            rgba = Gdk.RGBA(0, 0, 0xff/0xff, 1.0)
            Gdk.cairo_set_source_rgba(cr, rgba)

            cr.set_line_width(1)
            cr.rectangle(scroll_x + x, y, w, h)
            cr.stroke()
            if self.widget.has_focus():
                cr.rectangle(scroll_x + x, y, w, h)
                cr.fill()
            if self.widget.has_focus() and self.struct.caret_is_box():
                cr.rectangle(scroll_x + x, y, w, h)
                cr.clip()
                # Draw text under caret with background color.
                cr.move_to(scroll_x + tx, ty + th / 2 - layout_h / 2)
                rgba = context.get_background_color(
                    self.widget.get_state_flags()
                )
                Gdk.cairo_set_source_rgba(cr, rgba)
                PangoCairo.show_layout(cr, layout)

            cr.restore()

    def do_button_press_event(self, ev):
        (tx, ty, tw, th) = self.struct.text_area_get_text_geometry()
        if tx <= ev.x and ev.x <= tx + tw and ty <= ev.y and ev.y <= ty + th:
            x = (ev.x - tx + self.struct.scroll_x) * Pango.SCALE
            line = self.ensure_layout().get_line(0)
            (inside, index, trailing) = line.x_to_index(x)
            if not inside:
                index += 1
            self.struct.caret_set(index)

        if not self.has_focus():
            self.grab_focus()

    # def do_focus_in_event(self, ev):
    #     pass

    # def do_focus_out_event(self, ev):
    #     pass

    # def do_motion_notify_event(self, ev):
    #     pass

    def do_enter_notify_event(self, ev):
        # pointer shape
        w = self.get_window()
        if w:
            w.set_cursor(Gdk.Cursor(Gdk.CursorType.XTERM))

        # bg/fg color
        self.state_prelight = True
        self.queue_draw()

    def do_leave_notify_event(self, ev):
        w = self.get_window()
        if w:
            w.set_cursor(None)

        # bg/fg color
        self.state_prelight = False
        self.queue_draw()

    def do_realize(self):
        allocation = self.get_allocation()
        attr = Gdk.WindowAttr()
        attr.window_type = Gdk.WindowType.CHILD
        attr.x = allocation.x
        attr.y = allocation.y
        attr.width = allocation.width
        attr.height = allocation.height
        attr.visual = self.get_visual()
        attr.event_mask = (
            self.get_events() |
            Gdk.EventMask.POINTER_MOTION_MASK |
            Gdk.EventMask.EXPOSURE_MASK |
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK |
            Gdk.EventMask.KEY_PRESS_MASK |
            Gdk.EventMask.KEY_RELEASE_MASK |
            Gdk.EventMask.TOUCH_MASK |
            Gdk.EventMask.FOCUS_CHANGE_MASK |
            Gdk.EventMask.ENTER_NOTIFY_MASK |
            Gdk.EventMask.LEAVE_NOTIFY_MASK |
            Gdk.EventMask.VISIBILITY_NOTIFY_MASK
        )
        WAT = Gdk.WindowAttributesType
        mask = WAT.X | WAT.Y | WAT.VISUAL
        window = Gdk.Window(self.get_parent_window(), attr, mask)
        self.set_window(window)
        self.register_window(window)
        self.set_realized(True)
        window.set_background_pattern(None)

        '''
        Gdk.EventMask.ALL_EVENTS_MASK
        Gdk.EventMask.BUTTON1_MOTION_MASK
        Gdk.EventMask.BUTTON2_MOTION_MASK
        Gdk.EventMask.BUTTON3_MOTION_MASK
        Gdk.EventMask.BUTTON_MOTION_MASK
        Gdk.EventMask.BUTTON_PRESS_MASK
        Gdk.EventMask.BUTTON_RELEASE_MASK
        Gdk.EventMask.ENTER_NOTIFY_MASK
        Gdk.EventMask.EXPOSURE_MASK
        Gdk.EventMask.FOCUS_CHANGE_MASK
        Gdk.EventMask.KEY_PRESS_MASK
        Gdk.EventMask.KEY_RELEASE_MASK
        Gdk.EventMask.LEAVE_NOTIFY_MASK
        Gdk.EventMask.POINTER_MOTION_HINT_MASK
        Gdk.EventMask.POINTER_MOTION_MASK
        Gdk.EventMask.PROPERTY_CHANGE_MASK
        Gdk.EventMask.PROXIMITY_IN_MASK
        Gdk.EventMask.PROXIMITY_OUT_MASK
        Gdk.EventMask.SCROLL_MASK
        Gdk.EventMask.SMOOTH_SCROLL_MASK
        Gdk.EventMask.STRUCTURE_MASK
        Gdk.EventMask.SUBSTRUCTURE_MASK
        Gdk.EventMask.TOUCHPAD_GESTURE_MASK
        Gdk.EventMask.TOUCH_MASK
        Gdk.EventMask.VISIBILITY_NOTIFY_MASK
        '''

    class Style:

        def __init__(self, widget=None):
            self.widget = widget
            css = ''

            v_base = '''
                {:s} {{
                  box-shadow: none;
                  background-image: none;
                  margin: 0px;
                  padding: 0px;
                  padding-left: 20px;
                  border-width: 1px;
                  border-style: solid;
                  border-radius: 500px;
                  border-radius: 0px;
                }}
            '''.format(self.widget.__gtype_name__)
            css += v_base

            # normal
            BG_COLOR_NORMAL = '#fcfcfc'
            BORDER_COLOR_NORMAL = '#fff'
            FG_COLOR_NORMAL = '#303030'
            BORDER_COLOR_FOCUS = '#1f1f1f'
            v_normal = '''
                {:s} {{
                  background: {};
                  color: {};
                  border-color: {};
                  border: 1px dotted {};
                  border-left: 3px solid {};
                  border-right: 3px solid {};
                }}
            '''.format(
                self.widget.__gtype_name__,
                BG_COLOR_NORMAL,
                FG_COLOR_NORMAL,
                BORDER_COLOR_NORMAL,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
            )
            css += v_normal

            # focus
            BG_COLOR_FOCUS = '#fff'
            FG_COLOR_FOCUS = '#000'
            BORDER_COLOR_FOCUS = '#4a90d9'
            v_focus = '''
                {:s}:focus {{
                  background: {};
                  color: {};
                  border: 1px dotted {};
                  border-left: 3px solid {};
                  border-right: 3px solid {};
                }}
            '''.format(
                self.widget.__gtype_name__,
                BG_COLOR_FOCUS,
                FG_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
            )
            css += v_focus

            self.setup_widget_css(css=css)

        def setup_widget_css(self, css=None, style_class=None):
            if not css:
                return
            provider = Gtk.CssProvider.new()
            context = self.widget.get_style_context()
            context.add_provider(
                provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
            b_css = bytes(css, encoding='ascii')
            provider.load_from_data(b_css)
            if style_class:
                context.add_class(style_class)
