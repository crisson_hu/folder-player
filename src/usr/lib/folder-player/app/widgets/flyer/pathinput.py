import os
from gi.repository import Gdk

from tool import HotKey, log_debug
from . import InputText


class PathInput(InputText):

    __gtype_name__ = 'PathInput'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_caret_type(bar=False)

    def set_caret_type(self, bar=True):
        self.struct.caret_type = (
            self.Structure.CARET_TYPE_BAR if bar else
            self.Structure.CARET_TYPE_BOX
        )
        return

    def set_text(self, *args, **kwargs):
        super().set_text(*args, **kwargs)
        return self

    def caret_move_end(self):
        self.struct.caret_move(end=True)
        return self

    def caret_move_diritem(self, right=True):
        tot_bytes = self.ensure_layout().get_text().encode('utf-8')
        sep = os.path.sep.encode()
        idx = self.struct.caret_index
        draw = False
        if right:
            i = tot_bytes[idx+1:].find(sep)
            if i > -1:
                self.struct.caret_index += i + 1
                draw = True
            else:
                tot_len = len(tot_bytes)
                if idx < tot_len:
                    self.struct.caret_index = tot_len
                    draw = True
        else:
            i = tot_bytes[:idx].rfind(sep)
            if i > -1:
                self.struct.caret_index = i
                draw = True
            else:
                if self.struct.caret_index > 0:
                    self.struct.caret_index = 0
                    draw = True

        if draw:
            self.queue_draw()
        return draw

    def caret_delete_diritem(self, right=True):
        tot_bytes = self.ensure_layout().get_text().encode('utf-8')
        sep = os.path.sep.encode()
        idx = self.struct.caret_index
        draw = False
        if right:
            i = tot_bytes[idx+1:].find(sep)
            if i > -1:
                end_idx = idx + i + 1
                text = '{}{}'.format(
                    tot_bytes[:idx].decode('utf-8'),
                    tot_bytes[end_idx:].decode('utf-8')
                )
                self.set_text(text)
                draw = True
            else:
                tot_len = len(tot_bytes)
                if idx < tot_len:
                    end_idx = tot_len
                    text = '{}'.format(
                        tot_bytes[:idx].decode('utf-8')
                    )
                    self.set_text(text)
                    draw = True
        else:
            i = tot_bytes[:idx].rfind(sep)
            if i > -1:
                self.struct.caret_index = i
                text = '{}{}'.format(
                    tot_bytes[:self.struct.caret_index].decode('utf-8'),
                    tot_bytes[idx:].decode('utf-8')
                )
                self.set_text(text)
                draw = True
            else:
                if self.struct.caret_index > 0:
                    self.struct.caret_index = 0
                    text = '{}'.format(
                        tot_bytes[idx:].decode('utf-8')
                    )
                    self.set_text(text)
                    draw = True

        if draw:
            self.queue_draw()
        return draw

    def do_key_press_event(self, ev):
        kv = ev.keyval
        ks = ev.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)

        if k_alt and (
                HotKey.is_right(kv) or
                kv == Gdk.KEY_f or
                kv == Gdk.KEY_F
        ):
            self.caret_move_diritem(right=True)
            return True
        elif k_alt and (
                HotKey.is_left(kv) or
                kv == Gdk.KEY_b or
                kv == Gdk.KEY_B
        ):
            self.caret_move_diritem(right=False)
            return True
        elif k_alt and HotKey.is_backspace(kv):
            self.caret_delete_diritem(right=False)
            return True
        elif k_alt and (kv == Gdk.KEY_d or kv == Gdk.KEY_D):
            self.caret_delete_diritem(right=True)
            return True
        elif kv == Gdk.KEY_Escape:
            self.set_text('')
            return True

        super().do_key_press_event(ev)
        return False
