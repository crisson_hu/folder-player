from gi.repository import GLib, Gtk

from tool import log_debug, reg_signal
from .plugins.player import GstPlayer
from .mainview import MainView
from .constants import Constants


class AppGui(Gtk.Application):

    SERVICE_NAME = 'org.folderplayer'

    def __init__(self, args):
        super().__init__(application_id=self.SERVICE_NAME)
        self.ui = None
        self.args = args
        reg_signal(quit_handler=self.destroy)

    def do_activate(self):
        log_debug()
        if not self.ui:
            self._selfstart()
            return
        self.ui.main_raise()

    def _selfstart(self):
        log_debug('GLib.MainLoop()')
        self.mainloop = GLib.MainLoop()

        self.ui = MainView(mainapp=self)

        self.player = (
            GstPlayer()
            .set_mainapp(mainapp=self)
            .set_volume(self.args.volume)
            .set_ui(ui=self.ui)
        )

        # Switch to dictionary UI.
        if self.args.dictgui:
            self.ui.mainwin_ui_switch_view(Constants.UiView.RUN_DICT)
            self.args.filename = None
        else:
            self.ui.mainwin_ui_switch_view(Constants.UiView.NORMAL)

        # .set_repeat_mode(repeat=self.args.repeat)
        self.player.play_file(filename=self.args.filename, seek=self.args.seek)

        self.ui.list_directory(self.args.filename)
        if self.args.filename:
            try:
                from pathlib import Path
                self.ui.current_item = Path(self.args.filename).name
            except Exception as e:
                log_debug(e)

        self.ui.main_raise()

        log_debug('GLib.MainLoop().run(): calling ....')
        self.mainloop.run()
        log_debug('GLib.MainLoop().run(): done.')
        return

    def play_file(self, item):
        return self.player.play_file(filename=item)

    def get_state_is_playing(self):
        return self.player.is_playing()

    def destroy(self):
        from app.plugins.datetime.clock import clock
        clock.stop()
        self.player.player_stop(async_run=False)
        self.ui.ui_destroy()
        log_debug('GLib.MainLoop().quit(): calling ....')
        self.mainloop.quit()
        log_debug('GLib.MainLoop().quit(): done.')
