from pathlib import Path
from gi.repository import Gdk, Gtk

from tool import log_debug


class StyleClassConstants:

    STYLE_CLASS_FAVORITE_ADDRESS_ITEM = 'FavoriteAddressItem'

    @staticmethod
    def set_favorite_address_item(cls, fav=True):
        if fav:
            cls.get_style_context().add_class(
                StyleClassConstants.STYLE_CLASS_FAVORITE_ADDRESS_ITEM
            )
        else:
            cls.get_style_context().remove_class(
                StyleClassConstants.STYLE_CLASS_FAVORITE_ADDRESS_ITEM
            )
        return


def setup_css():
    provider = Gtk.CssProvider()
    style_context = Gtk.StyleContext()
    style_context.add_provider_for_screen(
        Gdk.Screen.get_default(),
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_USER
    )

    with open(
            str(Path(__file__).parent / 'gtk3.css'), mode='rb'
    ) as fd:
        css = fd.read()
        if Gtk.MINOR_VERSION < 24:
            with open(
                    str(Path(__file__).parent / 'gtk318.css'),
                    mode='rb'
            ) as fd318:
                css += fd318.read()
        provider.load_from_data(css)
    return


def set_selected(cls, is_selected):
    if not hasattr(cls, 'is_selected'):
        cls.is_selected = False
    if cls.is_selected == is_selected:
        return True
    cls.is_selected = is_selected
    context = cls.get_style_context()
    state = context.get_state()
    selected = Gtk.StateFlags.SELECTED
    if is_selected:
        new_state = state | selected
    else:
        new_state = state & (~selected)
    context.set_state(Gtk.StateFlags(new_state))
    return True


def set_prelight(cls, is_prelight):
    if not hasattr(cls, 'is_prelight'):
        cls.is_prelight = False
    if cls.is_prelight == is_prelight:
        return True
    cls.is_prelight = is_prelight
    context = cls.get_style_context()
    state = context.get_state()
    prelight = Gtk.StateFlags.PRELIGHT
    if is_prelight:
        new_state = state | prelight
    else:
        new_state = state & (~prelight)
    context.set_state(Gtk.StateFlags(new_state))
    return True
