from pathlib import Path
from gi.repository import Gtk

from app.constants import Constants
from tool import FileType
from base import WidgetBaseMixin, IconBase


class IconType(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'IconType'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        return

    def set_display_text(self, pathitem):
        suffix = (
            str(pathitem).split('.')[-1].lower()
            if str(pathitem).find('.') != -1
            else None
        )
        icon_name = 'file.png'
        if FileType.is_dir(pathitem):
            icon_name = 'folder.png'
        elif suffix in Constants.FileSuffix.MUSICS:
            icon_name = 'music.png'
        elif suffix in Constants.FileSuffix.VIDEOS:
            icon_name = 'video.png'
        elif suffix in ['pdf']:
            icon_name = 'pdf.png'
        elif suffix in Constants.FileSuffix.IMAGES:
            icon_name = 'image.png'

        # Clear all contents.
        for c in self.get_children():
            self.remove(c)

        # Add new icon.
        self.icon_type_icon = IconBase().set_icon('type/' + icon_name)

        self.add(self.icon_type_icon)
        self.show_all()

        return self
