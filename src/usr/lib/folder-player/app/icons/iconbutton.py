from gi.repository import Gdk

from tool import log_debug
from base import IconBase, ButtonBaseMixin


class IconButton(IconBase, ButtonBaseMixin):

    __gtype_name__ = 'IconButton'
    __extra_style_classes__ = [
        __gtype_name__,
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_cursor_hand1()
        self.set_can_focus(False)
        return


class IconButtonRepeat(IconButton):

    __gtype_name__ = 'IconButtonRepeat'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return


class IconButtonRepeatDisable(IconButton):

    __gtype_name__ = 'IconButtonRepeatDisable'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return
