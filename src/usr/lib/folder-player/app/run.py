import argparse

from tool import log_debug, todo
from dbuslite import store_lite, Player as PlayerConstant
from .gui import AppGui
from app.plugins.dictclient import DictClient


class AppRunner:

    def __init__(self, *args, **kwargs):
        self._do_argparse()
        return

    def _do_argparse(self):
        parser = argparse.ArgumentParser(
            description='GStreamer based Folder Player.',
            allow_abbrev=False
        )
        parser.add_argument(
            '-c', '--cli',
            action='store_true',
            help='list available proxy entries'
        )
        parser.add_argument(
            '--dictcli',
            help='Run dictionary [CLI].'
        )
        parser.add_argument(
            '--dictgui',
            action='store_true',
            help='Run dictionary [GUI].'
        )
        parser.add_argument(
            '--volume',
            type=float,
            action='store',
            default=0.20,
            help='set initial player volume. Value range: [0.0, 1.0]'
        )
        parser.add_argument(
            '--seek',
            type=int,
            action='store',
            default=0,
            help='seek SEEK seconds'
        )
        parser.add_argument(
            '-d', '--debug',
            action='store_true',
            default=False,
            help='enable debug mode'
        )
        parser.add_argument(
            'filename',
            nargs='?',
            help='music/video file'
        )
        # parser.add_argument(
        #     '-r', '--repeat',
        #     action='store_true',
        #     default=False,
        #     help='Repeat play list.'
        # )

        self.args = parser.parse_args()
        self.reset_default_params()

        if self.args.volume > 1.0 or self.args.volume < 0:
            parser.error('Volume should be within [0, 1].')

        if not self.args.debug:
            import logging
            from tool import logger
            logger.set_level(logging.INFO)

        log_debug(self.args)
        log_debug('starting app ....')

        self._show_config()

        if self.args.cli or self.args.dictcli:
            self._cli()
        else:
            self._gui()

        self._terminate()
        return

    def _cli(self):
        # Switch the UI.
        if self.args.dictcli:
            dc = DictClient()
            log_debug(self.args.dictcli)
            msg = dc.get_response(self.args.dictcli)
            log_debug(msg)

        return

    def _gui(self):
        appgui = AppGui(args=self.args)
        appgui.run()
        return

    def _terminate(self):
        log_debug('stopped app.')
        return

    def _show_config(self):
        '''Show Configuration'''
        log_debug(store_lite)
        return

    def reset_default_params(self):
        self.args.volume = 0.20
        store_lite.player_repeat_mode = PlayerConstant.REPEAT_NONE
        return
