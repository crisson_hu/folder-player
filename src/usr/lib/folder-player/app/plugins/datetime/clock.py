from datetime import datetime
import calendar
from gi.repository import GLib, Gtk

# from tool import log_debug
from base import WidgetBaseMixin


class Clock(Gtk.EventBox, WidgetBaseMixin):

    __gtype_name__ = 'Clock'
    STYLE_CLASS_HOVER = 'ClockHover'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.label = ClockLabel()
        self.add(self.label)
        return

    def do_enter_notify_event(self, ev):
        ctx = self.get_style_context()
        if self.STYLE_CLASS_HOVER not in ctx.list_classes():
            ctx.add_class(self.STYLE_CLASS_HOVER)
        return

    def do_leave_notify_event(self, ev):
        ctx = self.get_style_context()
        if self.STYLE_CLASS_HOVER in ctx.list_classes():
            ctx.remove_class(self.STYLE_CLASS_HOVER)
        return

    def do_button_press_event(self, ev):
        self.label.toggle_second()
        return

    def stop(self):
        self.label.stop()
        return True


class ClockLabel(Gtk.Label):

    __gtype_name__ = 'ClockLabel'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_property('margin-left', 5)
        self.set_property('margin-right', 5)
        self.should_stop = False
        self.opt_second = True
        self.tid = 0
        self.update_label()
        self.connect('query-tooltip', self.refresh_tooltip)
        self.refresh_tooltip()
        return

    def refresh_tooltip(self, *args):
        txt = calendar.TextCalendar(firstweekday=6).formatmonth(
            datetime.now().year,
            datetime.now().month,
        )
        self.set_tooltip_text(txt)
        # FIXME conflict with the documentation:
        #
        #   Using the given coordinates, the signal handler should
        #   determine whether a tooltip should be shown for widget
        #   . If this is the case TRUE should be returned, FALSE
        #   otherwise. Note that if keyboard_mode is TRUE, the values
        #   of x and y are undefined and should not be used.
        #
        return False

    def toggle_second(self):
        self.opt_second = not self.opt_second
        self.restart()
        return True

    def clock_run(self):
        self.update_label()
        return self

    def clock_sched_run(self):
        # seconds
        sec = int(60 - GLib.DateTime.new_now_local().get_seconds())
        # milli-secondsx
        now = datetime.now()
        max_msec = 0.9999
        min_msec = 0.02
        residue = 1.0 - (now.microsecond / 1000000)
        if residue >= max_msec:
            residue = max_msec
        elif residue <= min_msec:
            residue = min_msec
        interval = residue
        if sec < 1:
            sec = 1
        # log_debug(sec)
        if self.opt_second:
            GLib.timeout_add(interval * 1000, self.update_label)
        else:
            GLib.timeout_add_seconds(sec, self.update_label)
        return self

    def update_label(self):
        t = GLib.DateTime.new_now_local().format(
            '%Y-%m-%d %A %H:%M{}'.format(':%S' if self.opt_second else '')
        )
        f = '{}'
        self.set_markup(f.format(t))
        # log_debug(t)
        if not self.should_stop:
            self.clock_sched_run()
        # NOTE: return False to be called only once.
        return False

    def restart(self):
        self.stop()
        self.should_stop = False
        self.clock_run()
        return

    def stop(self):
        self.should_stop = True
        if self.tid != 0:
            GLib.source_remove(self.tid)
            self.tid = 0
        return True


clock = Clock()
