import subprocess
import ctypes
from pathlib import Path

from tool import log_debug


class DictClient:

    def __init__(self):
        pass

    def stop_task(self):
        pass

    def _get_response_dll(self, cmd):
        return
        libfile = str(Path(__file__).parent / 'libdictool.so')
        libdict = ctypes.cdll.LoadLibrary(libfile)
        libdict.get_response.restype = ctypes.c_char_p
        s = libdict.get_response().decode()
        log_debug(s)

    def get_response(self, cmd):
        try:
            self._get_response_dll(cmd)
        except Exception:
            pass

        if not isinstance(cmd, (str, )):
            raise ValueError('Not a string input.')

        msg = ''
        try:
            app = subprocess.Popen(
                ['dict', cmd], stdout=subprocess.PIPE, stderr=subprocess.STDOUT
            )
        except Exception as e:
            log_debug(cmd)
            if e.errno == 2:
                return 'Make sure to install `dict` client.'
            log_debug((e.errno, e.strerror))
            log_debug(e)
            raise e
        else:
            lines = app.stdout.readlines()
            for m in lines:
                try:
                    msg += m.decode('utf-8')
                except Exception:
                    msg += m.decode('cp437')
                except Exception:
                    log_debug(m)
                    msg += m.hex() + '\n'

        return msg

    def _get_response_ui(self, textview_widget, cmd):
        try:
            msg = self.get_response(cmd)
        except Exception as e:
            log_debug('Exception:' + str(e))
            msg = ''
        else:
            textview_widget.get_buffer().set_text(msg)
