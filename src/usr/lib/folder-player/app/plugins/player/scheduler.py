from app.constants import Constants
from tool import log_debug
from dbuslite import store_lite


class PlayerScheduler:

    def __init__(self, player=None):
        self._repeat = Constants.PlaySched.REPEAT_ONCE
        self._player = player
        return

    @property
    def play_list(self):
        if not hasattr(self, '_play_list'):
            self._play_list = []
        return self._play_list

    def next_file(self):
        if not self.play_list:
            return None

        f = None
        if self._repeat == Constants.PlaySched.REPEAT_LOOP:
            f = self.play_list[0]
        else:
            f = self.play_list.pop()

        log_debug(f)
        return f

    def notify_state(self, state=None, from_user=False):
        msg = 'state: {}, repeat: {}'.format(state, self._repeat)
        if state == Constants.PlayerState.STOPPED:
            info = 'STOPPED'
            if from_user and self.play_list:
                self.play_list.pop(0)
                return
            if self._repeat == Constants.PlaySched.REPEAT_LOOP:
                info += '/REPEAT_LOOP'
                self._player.play_file()
            log_debug('{}. {}'.format(msg, info))
        return

    # def set_repeat_mode(self, repeat=False):
    #     self._repeat = (
    #         Constants.PlaySched.REPEAT_LOOP if repeat else
    #         Constants.PlaySched.REPEAT_ONCE
    #     )
    #     return
