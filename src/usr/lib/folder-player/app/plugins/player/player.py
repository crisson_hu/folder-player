from urllib import parse
from pathlib import Path
from gi.repository import (
    GObject, GLib, Gtk, GdkX11,
    Gst, GstVideo,
)

from dbuslite import store_lite, Player as PlayerConstant
from tool import log_debug, log_info
from app.constants import Constants
from .scheduler import PlayerScheduler


class GstPlayer:

    def __init__(self):
        Gst.init(None)
        self.filename = None
        self._timer = self.Timer(self)

        self.has_started = False
        self.player_start_mutex = GLib.Mutex()
        self.player_start_mutex.init()
        self.player = Gst.ElementFactory.make('playbin', None)
        self._player_seek = 0
        self._scheduler = PlayerScheduler(player=self)
        self._store = store_lite

        return

    def set_mainapp(self, mainapp=None):
        self.mainapp = mainapp
        return self

    def set_ui(self, ui=None):
        self.ui = ui
        self._ui = self.UI(ui=ui, timer=self._timer)
        self._stat = self.Stat(self, self.player)
        return self

    def set_volume(self, volume=0.2):
        self.volume = volume
        return self

    # def set_repeat_mode(self, repeat=False):
    #     self._scheduler.set_repeat_mode(repeat)
    #     return self

    def atomic_state(self, func=None, *args, **kwargs):
        ret = None
        try:
            self.player_start_mutex.lock()
            ret = func(*args, **kwargs)
        finally:
            self.player_start_mutex.unlock()
        return ret

    def state_machine(self, state):
        if state == Constants.PlayerState.PLAYING:
            return self.atomic_state(self._play_file_nl)
        if state == Constants.PlayerState.PAUSED:
            return self.atomic_state()
        if state == Constants.PlayerState.STOPPED:
            return self.atomic_state()
        return True

    def play_file(self, filename=None, seek=0):
        self._player_seek = seek
        if filename:
            self._scheduler.play_list.append(filename)
        return self.atomic_state(self._play_file_nl)

    def _play_file_nl(self):
        if self.has_started:
            log_debug('Player has started, NO-OP.')
            return False

        filename = self._scheduler.next_file()
        if filename is None:
            # log_debug('Filename is missing.')
            return False

        self.has_started = True

        try:
            self.filename = Path(filename)
        except FileNotFoundError:
            log_info('Error: file `{}` not found.'.format(filename))
            return False

        self.player.get_bus().add_watch(0, self.playbin_bus_cb, None)
        self.player.get_bus().set_sync_handler(self._create_win_cb, None)
        self._stat.reset_state()
        self._stat.query_start()
        self.player_start()

        return True

    def player_start(self):
        self._timer._async_run('player_start_tid', 0, self._player_start_cb)
        return

    def _player_start_cb(self):
        self.atomic_state(self._player_start_nl)
        # One time call.
        return False

    def _player_start_nl(self):
        # FIXME why not work?
        if self._player_seek > 0:
            self.player_seek(self._player_seek)
            self._player_seek = 0
        self._player_initial_ready_nl()
        self.player.set_state(Gst.State.PLAYING)
        self._ui.play_pause(play=True)
        return

    class UI:

        def __init__(self, ui=None, timer=None):
            self._ui = ui
            self._timer = timer
            return

        def set_volume(self, volume):
            self._timer.async_abort('ui_set_volume_tid')
            self._timer._async_run(
                'ui_set_volume_tid', 0, self._set_volume_cb, volume,
            )
            return

        def _set_volume_cb(self, volume):
            self._ui.ui_set_volume(volume)
            # One time call
            return False

        def set_volume_mute(self, mute):
            self._timer.async_abort('ui_set_volume_mute_tid')
            self._timer._async_run(
                'ui_set_volume_mute_tid', 0, self._set_volume_mute_cb, mute,
            )
            return

        def _set_volume_mute_cb(self, mute):
            self._ui.ui_set_volume_mute(mute)
            # One time call
            return False

        def play_pause(self, play=True, stop=False, async_run=True):
            self._timer.async_abort('ui_play_pause_tid')
            if async_run:
                self._timer._async_run(
                    'ui_play_pause_tid', 0, self._play_pause_cb, play, stop,
                )
            else:
                self._timer._run_cb(
                    'ui_play_pause_tid', self._play_pause_cb, play, stop,
                )
            return

        def _play_pause_cb(self, play, stop=False):
            self._ui.set_play_pause(play=play, stop=stop)
            # One time call
            return False

        def video_control_set_visible(self, visible=True):
            self._timer.async_abort(
                'mainwin_ui_view_video_control_set_visible_tid'
            )
            self._timer._async_run(
                'mainwin_ui_view_video_control_set_visible_tid', 0,
                self._video_control_set_visible_cb, visible
            )
            return

        def _video_control_set_visible_cb(self, visible):
            self._ui.mainwin.video_control_set_visible(visible)
            # One time call
            return False

    def player_initial_ready(self):
        return self.atomic_state(self._player_initial_ready_nl)

    def _player_initial_ready_nl(self):
        if not self.has_started:
            log_debug('Player has not started, NO-OP.')
            return False
        self._ui.play_pause(play=False)
        self.player.set_state(Gst.State.READY)
        self.uri = str(self.filename)
        self.reset_state()
        return True

    def player_play(self):
        return self.atomic_state(self._player_play_nl)

    def _player_play_nl(self):
        if not self.has_started:
            log_debug('Player has not started, NO-OP.')
            return False
        log_debug('volume: {:.1f}, {:.1f}, mute: {}.'.format(
            self.volume, self.player.get_property('volume'), self.mute
        ))
        self.player.set_state(Gst.State.PLAYING)
        self._stat.query_start()
        self._ui.play_pause(play=True)
        return True

    def player_pause(self):
        return self.atomic_state(self._player_pause_nl)

    def _player_pause_nl(self):
        if not self.has_started:
            log_debug('Player has not started, NO-OP.')
            return False
        self.player.set_state(Gst.State.PAUSED)
        self._stat.query_stop()
        self._ui.play_pause(play=False)
        return True

    def player_stop(self, from_user=False, async_run=True):
        self.atomic_state(self._player_stop_nl)

        if async_run:
            self._timer._async_run(
                'player_play_file_tid', 1,
                self._player_stop_notify_cb,
                from_user
            )
        else:
            self._timer._run_cb(
                'player_play_file_tid',
                self._player_stop_notify_cb,
                from_user
            )

        return True

    def _player_stop_notify_cb(self, from_user=False):
        self._scheduler.notify_state(
            state=Constants.PlayerState.STOPPED,
            from_user=from_user
        )
        # One time call.
        return False

    def _player_stop_nl(self):
        if not self.has_started:
            # log_debug('Already stopped, NO-OP.')
            return

        self.has_started = False

        self._timer.async_abort_all()
        self._stat.query_stop()

        self.player.set_state(Gst.State.NULL)
        self.uri = ''

        self.player.get_bus().remove_watch()
        self.player.get_bus().set_sync_handler(None)
        self._ui.play_pause(play=False, stop=True, async_run=False)
        self.player.set_window_handle(0)
        log_debug('stopped')

        return

    def player_switch_view_normal(self):
        self._mainwin_ui_switch_view(Constants.UiView.NORMAL)
        return

    def _create_win_cb(self, bus, msg, data):
        if not GstVideo.is_video_overlay_prepare_window_handle_message(msg):
            return Gst.BusSyncReply.PASS

        self._mainwin_ui_switch_view(Constants.UiView.VIDEO)
        self.player.set_window_handle(self.ui.get_video_window_xid())
        self._ui.play_pause(play=True)

        return Gst.BusSyncReply.DROP

    def get_video_size(self):
        video_sink = self.player.get_property('video-sink')
        return (video_sink.width, video_sink.height)

    def _mainwin_ui_switch_view(self, view):
        self._timer._async_run(
            'ui_switch_video_tid', 0, self._mainwin_ui_switch_view_cb, view
        )
        return

    def _mainwin_ui_switch_view_cb(self, view):
        self.ui.mainwin_ui_switch_view(view)
        # One time call.
        return False

    def _player_destroy(self):
        raise NotImplementedError('SHOULD NEVER BE CALLED!')
        self.player.get_bus().remove_watch()
        self.player.get_bus().set_sync_handler(None)
        return

    def is_playing(self):
        return self.player.current_state == Gst.State.PLAYING

    def is_stopped(self):
        return self.player.current_state == Gst.State.NULL

    def player_seek(self, seconds):
        self.player.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            seconds * Gst.SECOND
        )
        store_lite.player_stream_current_position = seconds * Gst.SECOND
        log_debug('%f Seconds.' % seconds)
        return

    def player_seek_step_backward(self, step=20):
        self.player_seek(
            store_lite.player_stream_current_position_second - step
            if store_lite.player_stream_current_position_second > step
            else 0
        )
        return

    def player_seek_step_forward(self, step=20):
        self.player_seek(
            store_lite.player_stream_current_position_second + step
        )
        return

    def playbin_bus_cb(self, bus, message, data):
        # log_debug(message.type)

        if message.type == Gst.MessageType.STATE_CHANGED:
            state = self.player.current_state
            # log_debug(state)
        elif message.type == Gst.MessageType.STREAM_STATUS:
            pass
        elif message.type == Gst.MessageType.EOS:
            log_debug('Gst.MessageType.EOS')

            # self.player_stop()
            # return False

            # FIXME
            if self._store.player_repeat_mode == PlayerConstant.REPEAT_CURRENT:
                self.player_start()
                return True

            if self.ui.mainwin_ui_view == Constants.UiView.VIDEO:
                self.player_seek(0)
                self.player_initial_ready()
                # fixme not work? consume 100% CPU.
                # self._stat.query_once()
                return True
            else:
                self.player_stop()
                return False

        elif message.type == Gst.MessageType.ERROR:
            log_debug('Gst.MessageType.ERROR playing: %s' % self.filename.name)
            self._stat.query_stop()
            if self.ui.mainwin_ui_view == Constants.UiView.VIDEO:
                self.player_pause()
                return True
            else:
                self.player_stop()
                return False
        return True

    class Stat:

        def __init__(self, parent, player):
            self.parent = parent
            self.ui = self.parent.ui
            self.player = player
            self.duration_tid = 0
            self.position_tid = 0
            return

        def reset_state(self):
            store_lite.player_stream_current_position = -1
            store_lite.player_stream_total_length = -1
            return

        def query_start(self):
            self.query_stop()
            self.duration_tid = GLib.timeout_add(200, self.progress_duration)
            self.position_tid = GLib.timeout_add(200, self.progress_position)
            return

        def query_once(self):
            self.query_stop()
            self.duration_tid = GLib.idle_add(self.progress_duration)
            self.position_tid = GLib.idle_add(self.progress_position, True)
            return

        def query_stop(self):
            if self.duration_tid > 0:
                GLib.source_remove(self.duration_tid)
                self.duration_tid = 0
            if self.position_tid > 0:
                GLib.source_remove(self.position_tid)
                self.position_tid = 0
            return

        def progress_position(self, once=False):
            if self.player.query(self.position_query):
                (fmt, self.position_ns) = self.position_query.parse_position()
                store_lite.player_stream_current_position = self.position_ns
                self.ui.player_stream_update()
            if once:
                self.position_tid = 0
                return False

            return True

        def progress_duration(self):
            if self.player.query(self.duration_query):
                (fmt, self.duration_ns) = self.duration_query.parse_duration()
                store_lite.player_stream_total_length = self.duration_ns
                self.ui.player_stream_update()
                self.duration_tid = 0
                return False

            return True

        @property
        def position_query(self):
            if not hasattr(self, '_position_query'):
                self._position_query = Gst.Query.new_position(Gst.Format.TIME)
            return self._position_query

        @property
        def duration_query(self):
            if not hasattr(self, '_duration_query'):
                self._duration_query = Gst.Query.new_duration(Gst.Format.TIME)
            return self._duration_query

    class Timer:

        def __init__(self, parent):
            '''The timer should be ended with `_tid`. '''
            self.player_play_file_tid = 0
            self.player_start_tid = 0
            self.ui_switch_video_tid = 0
            self.ui_switch_normal_tid = 0
            self.ui_play_pause_tid = 0
            self.mainwin_ui_view_video_control_set_visible_tid = 0
            self.ui_set_volume_tid = 0
            self.ui_set_volume_mute_tid = 0
            return

        def async_abort_all(self):
            for a in filter(lambda x: x.endswith('_tid'), dir(self)):
                self.async_abort(a)
            return

        def async_abort(self, str_tid):
            if not hasattr(self, str_tid):
                return 0
            tid = getattr(self, str_tid)
            if tid > 0:
                log_debug('remove %s' % str_tid)
                GLib.source_remove(tid)
                tid = 0
            return 0

        def _async_run(self, str_tid, interval, func, *args, **kwargs):
            # Without setting `default` kwarg value, make this error visible.
            tid = getattr(self, str_tid)

            if tid > 0:
                return tid
            if interval > 0:
                tid = GLib.timeout_add(
                    interval, self._run_cb, str_tid, func, *args, **kwargs
                )
            else:
                tid = GLib.idle_add(
                    self._run_cb, str_tid, func, *args, **kwargs
                )
            return tid

        def _run_cb(self, str_tid, func, *args, **kwargs):
            r = func(*args, **kwargs)
            if r is False:
                tid = getattr(self, str_tid)
                tid = 0
            return r

    @property
    def now_playing_uri(self):
        return self.player.get_property('current-uri')

    @property
    def uri(self):
        return self.player.get_property('uri')

    @uri.setter
    def uri(self, new_uri):
        self.player.set_property(
            'uri',
            '' if not new_uri else
            'file://{}'.format(parse.quote(new_uri, safe=':/'))
        )
        self.ui.update_ui_info()
        return

    @property
    def volume(self):
        if not hasattr(self, '_volume'):
            self._volume = self.player.get_property('volume')
        return self._volume

    @volume.setter
    def volume(self, volume):
        '''
        1.0 means 100%, 0.0 means mute. If specifying 10, the sound
        is VERY large, so avoid the value greater than 1.0.
        '''
        DOWN_LIMIT = 0.0
        UP_LIMIT = 10.0
        self._volume = (
            DOWN_LIMIT if volume < DOWN_LIMIT else
            volume if volume <= UP_LIMIT else
            UP_LIMIT
        )
        self._volume_exec()
        return

    def _volume_exec(self):
        self.player.set_property('volume', self._volume)
        if hasattr(self, '_ui'):
            self._ui.set_volume(self._volume)
        return True

    @property
    def mute(self):
        return self.player.get_property('mute')

    @mute.setter
    def mute(self, mute=True):
        self.player.set_property('mute', mute)
        if hasattr(self, '_ui'):
            self._ui.set_volume_mute(mute)
        return

    def _mute_exec(self):
        self.player.set_property('mute', self.mute)
        return True

    def reset_state(self):
        self._mute_exec()
        self._volume_exec()
        log_debug('volume: {:.1f}, mute: {}'.format(self._volume, self.mute))
        return True
