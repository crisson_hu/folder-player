from gi.repository import Gtk

from tool import _
from base import WidgetBaseMixin
from .button import (
    ButtonActionSave,
    ButtonActionInfo,
    ButtonActionLanguage,
    ButtonActionRange,
    ButtonActionFit,
    ButtonActionLayout,
)


class BarEditor(Gtk.HBox, WidgetBaseMixin):

    __gtype_name__ = 'BarEditor'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.set_can_focus(False)
        self.set_can_default(False)
        self._setup()
        return

    def _setup(self):
        self.set_property('spacing', 1)
        # Set reasonable height.
        self.set_size_request(1, 30)

        self.pager_txt = Gtk.HBox()
        self.add(self.pager_txt)
        self.child_set_property(self.pager_txt, 'expand', False)

        self._bsave = ButtonActionSave().set_text(_('Save'))
        self.pager_txt.add(self._bsave)
        self._blanguage = ButtonActionLanguage().set_text('')
        self._blanguage.set_no_show_all(True)
        self.pager_txt.add(self._blanguage)

        self.pager_pdf = Gtk.HBox()
        self.pager_pdf.set_property('halign', Gtk.Align.CENTER)
        self.add(self.pager_pdf)
        self.child_set_property(self.pager_pdf, 'expand', True)

        self._binfo = ButtonActionInfo().set_text('Info')
        self._binfo.connect('query-tooltip', self.refresh_tooltip)
        self.pager_pdf.add(self._binfo)
        self._brange = ButtonActionRange().set_text('')
        self._brange.set_no_show_all(True)
        self.pager_pdf.add(self._brange)
        self._bfit = ButtonActionFit()
        self.pager_pdf.add(self._bfit)
        self._blayout = ButtonActionLayout()
        self.pager_pdf.add(self._blayout)

        self._bmore = ButtonActionSave().set_text('{} ...'.format(_('More')))
        self.add(self._bmore)
        self.child_set_property(self._bmore, 'expand', False)
        self._bmore.set_property('halign', Gtk.Align.END)

        return self

    def set_save_action(self, save_action=None):
        self._bsave.set_action(save_action)
        return self

    def set_lang_action(self, lang_action=None):
        self._blanguage.set_action(lang_action)
        return self

    def set_info_action(self, info_action=None):
        self._binfo.set_action(info_action)
        return self

    def set_range_action(self, range_action=None):
        self._brange.set_action(range_action)
        return self

    def set_fit_action(self, fit_action=None):
        self._bfit.set_action(fit_action)
        return self

    def set_layout_action(self, layout_action=None):
        self._blayout.set_action(layout_action)
        return self

    def refresh_tooltip(self, *args):
        self._binfo.refresh_tooltip()
        # FIXME conflict with the documentation:
        #
        #   Using the given coordinates, the signal handler should
        #   determine whether a tooltip should be shown for widget
        #   . If this is the case TRUE should be returned, FALSE
        #   otherwise. Note that if keyboard_mode is TRUE, the values
        #   of x and y are undefined and should not be used.
        #
        return False

    def set_more_action(self, more_action=None):
        self._bmore.set_action(more_action)
        return self

    def shift_visible(self, visible=False):
        w = self._bsave
        # if visible:
        #     # enable show
        #     w.set_no_show_all(False)
        #     w.show_all()
        # else:
        #     # disable show
        #     w.set_no_show_all(True)
        #     w.set_visible(False)
        w.enable = visible

        return self
