from pathlib import Path
from gi.repository import Gdk, Gtk, Pango

from tool import HotKey, log_debug, _
from app.constants import Constants
from ..bar import BarEditor
from ..textwidget import TextWidget
from ..dialogfilename import DialogFileName
from .dvbase import DialogViewBase


class DialogViewText(DialogViewBase):

    __gtype_name__ = 'DialogViewText'

    __supported_encoding__ = (
        'utf-8',
        'cp936',
        'cp437',
    )

    LC_MODIFY = _('Modify')
    LC_READONLY = _('Readonly')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.window_init_maximized = not self.window_init_maximized
        self.connect('key-press-event', self._on_key_press)
        self.is_can_modify = True
        return

    @property
    def font_size(self):
        if not hasattr(self, '_font_size'):
            self._font_size = Constants.Font.FONT_SIZE_QUICK_VIEW
        return self._font_size

    @font_size.setter
    def font_size(self, fsz):
        self._font_size = fsz
        return

    @property
    def is_changed(self):
        if not hasattr(self, '_is_changed'):
            self._is_changed = False
        return self._is_changed

    @is_changed.setter
    def is_changed(self, changed):
        self._is_changed = changed
        return

    @property
    def is_can_modify(self):
        if not hasattr(self, '_is_can_modify'):
            self._is_can_modify = False
        return self._is_can_modify

    @is_can_modify.setter
    def is_can_modify(self, modify):
        self._is_can_modify = modify
        self._tv.set_editable(modify)
        self._ebar.shift_visible(visible=modify)
        return

    def setup_widgets(self):
        vb = Gtk.VBox()
        self.add(vb)

        self._ebar = BarEditor().shift_visible(visible=False) \
                                .set_save_action(self.action_save) \
                                .set_more_action(self.action_more) \
                                .set_info_action(self.action_info)
        vb.add(self._ebar)
        vb.child_set_property(self._ebar, 'expand', False)

        self.main_sw = Gtk.ScrolledWindow()
        vb.add(self.main_sw)

        self._tv = TextWidget().set_ebar(self._ebar)
        self._tv.connect('populate-popup', self._op_tv_menu)
        self._tv.connect('preedit-changed', self._op_tv_changed)
        self.main_sw.add(self._tv)

        self._setup_style()
        return

    def _setup_style(self):
        font = '"Lucida Grande", Arial, sans-serif, Monospace'
        font = 'Monospace'
        des13_bold = Pango.font_description_from_string('%s bold 13' % font)
        des13 = Pango.font_description_from_string('%s 13' % font)
        des_bold = Pango.font_description_from_string('%s bold 11' % font)
        des = Pango.font_description_from_string(
            '%s %d' % (
                font,
                self.font_size
            )
        )
        self._tv.modify_font(des)
        return

    @property
    def use_encoding(self):
        if not hasattr(self, '_use_encoding'):
            self._use_encoding = None
        return self._use_encoding

    @use_encoding.setter
    def use_encoding(self, encoding):
        self._use_encoding = encoding
        log_debug('using {}'.format(encoding))
        return

    def _load_file(self):
        if self.use_encoding:
            self._load_file_encoding(self.use_encoding)
            return
        log_debug('Detect encoding: "{}"'.format(str(self._pi)))
        for enc in self.__supported_encoding__:
            self._load_file_encoding(enc)
            if self.use_encoding:
                # got correct encoding
                break
        return

    def _load_file_encoding(self, encoding):
        with open(str(self._pi), encoding=encoding) as f:
            try:
                s = f.read()
            except Exception as e:
                log_debug(e)
            else:
                self.use_encoding = encoding
                self._set_text(s)
        return

    def _store_file(self):
        try:
            f = open(str(self._pi), mode='w+', encoding='utf-8')
            log_debug('using utf-8')
        except UnicodeDecodeError:
            f = open(str(self._pi), mode='w+', encoding='cp936')
            log_debug('using cp936')
        except Exception:
            f = open(str(self._pi), mode='w+', encoding='cp437')
            log_debug('using cp437')
        except Exception:
            log_debug('Decoding Error!')
            raise
        else:
            f.write(self._tv.get_full_text())
            f.flush()
            f.close()
            self._mainview.list_directory()
        return

    def _set_text(self, text=''):
        self._tv.set_text(text)
        self.is_can_modify = False
        return

    def _op_tv_changed(self, *args):
        log_debug(args)
        self.is_changed = True
        return True

    def _op_tv_menu(self, _tv, menu):
        if self.is_changed:
            return True
        mi = Gtk.SeparatorMenuItem()
        mi.show()
        menu.insert(mi, 0)

        def _op_mod(w):
            self.is_can_modify = True
            return True

        def _op_no_mod(w):
            self.is_can_modify = False
            return True

        mod_label = self.LC_MODIFY
        mod_op = _op_mod
        if self.is_can_modify:
            mod_label = self.LC_READONLY
            mod_op = _op_no_mod
        mi = Gtk.MenuItem.new_with_label('{} ..'.format(mod_label))
        mi.show()
        mi.connect('activate', mod_op)
        menu.insert(mi, 0)

        return True

    def action_save(self, *args):
        if not self._pi:
            DialogFileName().set_mainwindow(self) \
                            .set_cb(self._file_name_cb) \
                            .show()
            return self
        try:
            self._store_file()
        except Exception as e:
            log_debug('Exception:' + e)
            return self

        self.is_can_modify = False
        return self

    def set_pathitem(self, pathitem=None, address=None, pre_save=False):
        super().set_pathitem(
            pathitem=pathitem, address=address, pre_save=pre_save
        )
        self._tv.set_filename(self._pi)
        if pathitem and not pre_save:
            self._load_file()
        self._ebar.refresh_tooltip()
        return self

    def _file_name_cb(self, pathitem):
        if not pathitem:
            return True
        self.set_pathitem(
            pathitem=(
                Path(pathitem) if isinstance(pathitem, str) else
                pathitem
            ),
            pre_save=True
        )
        self.action_save()
        return True

    def handle_down(self, page=False):
        if page:
            move = Gtk.ScrollType.PAGE_DOWN
        else:
            move = Gtk.ScrollType.STEP_DOWN
        self.main_sw.do_scroll_child(self.main_sw, move, False)
        return

    def handle_up(self, page=False):
        if page:
            move = Gtk.ScrollType.PAGE_UP
        else:
            move = Gtk.ScrollType.STEP_UP
        self.main_sw.do_scroll_child(self.main_sw, move, False)
        return

    def _on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        if (
                kv == Gdk.KEY_Escape
                or (kv in (Gdk.KEY_w, Gdk.KEY_W) and k_ctrl)
                or (not self.is_can_modify and kv in (Gdk.KEY_q, Gdk.KEY_Q))
        ):
            self.close()
            return True
        elif (
                k_ctrl or
                not self.is_can_modify
        ) and (
                kv in (ord('+'), ord('-'), ord('='), ord('0'),)
        ):
            if kv in (ord('+'), ord('='),):
                self.font_size += 2
            elif kv == ord('-'):
                self.font_size -= 2
            elif kv == ord('0'):
                self.font_size = Constants.Font.FONT_SIZE_QUICK_VIEW
            else:
                pass
            self._setup_style()
        elif k_ctrl and (
                kv in (Gdk.KEY_s, Gdk.KEY_S,)
        ):
            self.action_save()
            return True
        elif not self.is_can_modify:
            if kv in (
                    Gdk.KEY_Up,
                    Gdk.KEY_k,
            ):
                self.handle_up()
                return True
            elif kv in (
                    Gdk.KEY_Down,
                    Gdk.KEY_j,
            ):
                self.handle_down()
                return True
            elif HotKey.is_page_down(kv) or (
                    HotKey.is_space(kv) and not k_shift
            ):
                self.handle_down(page=True)
                return True
            elif (
                    HotKey.is_page_up(kv) or
                    HotKey.is_backspace(kv) or
                    (HotKey.is_space(kv) and k_shift)
            ):
                self.handle_up(page=True)
                return True
            elif k_ctrl and kv in (
                    Gdk.KEY_f, Gdk.KEY_F,
            ):
                self.handle_down(page=True)
                return True
            elif k_ctrl and kv in (
                    Gdk.KEY_b, Gdk.KEY_B,
            ):
                self.handle_up(page=True)
                return True
            elif k_ctrl and kv in (
                    Gdk.KEY_v, Gdk.KEY_V,
            ):
                self.handle_down(page=True)
                return True
            elif k_alt and kv in (
                    Gdk.KEY_v, Gdk.KEY_V,
            ):
                self.handle_up(page=True)
                return True

        return False

    def show(self):
        super().show()
        self.window_move()
        return
