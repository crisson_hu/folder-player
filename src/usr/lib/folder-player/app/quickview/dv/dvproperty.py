from gi.repository import Gdk, Gtk, Pango

from tool import (
    HotKey,
    log_debug,
    FileProp,
    FilePropPdf,
    FilePropZip,
    FilePropMedia,
)
from app.theme import set_prelight
from .dvbase import DialogViewBase


class DialogViewProperty(DialogViewBase):

    __gtype_name__ = 'DialogViewProperty'

    OPEN_AS_TEXT_BAR = 'DialogViewPropertyBar'
    OPEN_AS_TEXT_BAR_BUTTON = 'DialogViewPropertyBarButton'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.window_init_maximized = not self.window_init_maximized
        self.connect('realize', self._on_realized)
        self.connect('key-press-event', self._on_key_press)
        self.txt_prop = ''
        self.prop_media = None
        return

    def setup_widgets(self):
        self.mvb = Gtk.VBox()
        self.add(self.mvb)

        self.rv = Gtk.Revealer()
        self.mvb.add(self.rv)
        self.open_as_txt_btn = Gtk.Button()
        self.open_as_txt_btn.set_label('Re-Open as Text File ....')
        self.btn_setup()
        hb = Gtk.HBox()
        hb.set_spacing(50)
        hb.add(self.open_as_txt_btn)
        hb.child_set_property(self.open_as_txt_btn, 'expand', False)
        self.rv.add(hb)
        hb.get_style_context().add_class(self.OPEN_AS_TEXT_BAR)

        self.rv.set_reveal_child(True)
        self.mvb.child_set_property(self.rv, 'expand', False)

        self.sw = Gtk.ScrolledWindow()
        self.mvb.add(self.sw)
        self.txt = Gtk.Label()
        self.txt.set_line_wrap(True)
        self.prop = self.txt
        self.sw.add(self.prop)

        w = self.txt
        w.set_margin_top(20)
        w.set_margin_end(20)
        w.set_margin_bottom(20)
        w.set_margin_start(20)
        w.set_halign(Gtk.Align.START)
        w.set_valign(Gtk.Align.START)

        return

    def btn_setup(self):
        self.open_as_txt_btn.get_style_context().add_class(
            self.OPEN_AS_TEXT_BAR_BUTTON
        )
        self.open_as_txt_btn.connect('activate', self.reopen_txt)
        self.open_as_txt_btn.connect('clicked', self.reopen_txt)
        self.open_as_txt_btn.connect('enter-notify-event', self._enter_notify)
        self.open_as_txt_btn.connect('leave-notify-event', self._leave_notify)
        return

    def _enter_notify(self, w, ev):
        set_prelight(w, True)
        win = w.get_window()
        if win:
            win.set_cursor(Gdk.Cursor(Gdk.CursorType.HAND1))
        return True

    def _leave_notify(self, w, ev):
        set_prelight(w, False)
        win = w.get_window()
        if win:
            win.set_cursor(Gdk.Cursor(Gdk.CursorType.ARROW))
        return True

    def reopen_txt(self, *args):
        from app.quickview.quickview import show_quick_view
        show_quick_view(
            text_mode=True,
            pathitem=self._pi,
            mainview=self._mainview
        )
        self.close()
        return True

    def _get_prop_str(self):
        self.txt_prop = '{}'.format(FileProp.get_stat(self._pi))
        furi = 'file://{}'.format(self._pi)
        longinfo = False
        if self._pi.name.lower().endswith('pdf'):
            longinfo = True
            pdffile = FilePropPdf(Poppler.Document.new_from_file(furi, None))
        elif self._pi.name.lower().endswith('zip'):
            longinfo = True
            f = '{}'.format(self._pi)
            self.txt_prop = '{}\n\n\n{}'.format(
                self.txt_prop,
                FilePropZip.get_zip_info(f),
            )

        self.prop_media = FilePropMedia().set_uri(furi).do_discovery(
            self._gst_cb
        )

        return self.txt_prop

    def _gst_cb(self):
        self.txt_prop_media = self.prop_media.get_disc_info_tags_str()
        if self.txt_prop_media:
            txt = '{}\n\n\n{}'.format(self.txt_prop, self.txt_prop_media)
            self.set_entry_text(txt, longinfo=True)
        return

    def set_entry_text(self, txt, longinfo=False):
        self.txt.set_text(txt)
        return

    def do_destroy(self, *args):
        self.prop_media.do_discovery_stop()
        super().do_destroy(*args)
        return

    def _on_realized(self, *args):
        self.refresh_prop()
        return True

    def refresh_prop(self):
        fn = str(self._pi)
        self.set_entry_text(self._get_prop_str())
        self.txt.modify_font(
            Pango.font_description_from_string('Monospace 13')
        )
        return True

    def _on_key_press(self, widget, ev):
        kv = ev.keyval
        ks = ev.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        # log_debug(msg)

        if kv in (
                Gdk.KEY_Escape,
                Gdk.KEY_W, Gdk.KEY_w,
                Gdk.KEY_Q, Gdk.KEY_q,
        ):
            self.close()
            return True

        if kv in (
                Gdk.KEY_R, Gdk.KEY_r,
        ) or HotKey.is_space(kv):
            self.refresh_prop()
            return True

        self.refresh_prop()
        return False
