import cairo
from gi.repository import Gdk, GdkPixbuf

from app.constants import Constants
from tool import log_debug, HotKey
from .dvbase import DialogViewBase


class DialogViewImg(DialogViewBase):

    __gtype_name__ = 'DialogViewImg'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connect('key-press-event', self._keypress)
        return

    def setup_widgets(self):
        return

    @property
    def surface(self):
        if not hasattr(self, '_surface'):
            if not hasattr(self, '_pi') or not self._pi:
                raise ValueError('invalid self._pi')
            fn = str(self._pi)
            fn_lower = fn.lower()
            log_debug(fn)
            if any(
                    fn_lower.endswith(i)
                    for i in Constants.FileSuffix.IMAGES_CAIRO
            ):
                self._surface = cairo.ImageSurface.create_from_png(fn)
            elif any(
                    fn_lower.endswith(i)
                    for i in Constants.FileSuffix.IMAGES_PIXBUF
            ):
                pb = GdkPixbuf.Pixbuf.new_from_file(fn)
                self._surface = Gdk.cairo_surface_create_from_pixbuf(pb, 1)
        return self._surface

    @property
    def scale_factor(self):
        if not hasattr(self, '_scale_factor'):
            w = self.get_allocated_width()
            h = self.get_allocated_height()
            iw = self.surface.get_width()
            ih = self.surface.get_height()
            sw = w / iw
            sh = h / ih
            swh = sw if sw < sh else sh
            self._scale_factor = 1.0 if swh > 1.0 else swh
        return self._scale_factor

    @scale_factor.setter
    def scale_factor(self, val):
        self._scale_factor = val
        return

    @property
    def bg_rgba(self):
        if not hasattr(self, '_bg_rgba'):
            # 8470ff
            bg = (0x84, 0x70, 0xff)
            bg_rgba = Gdk.RGBA(bg[0]/255.0, bg[1]/255.0, bg[2]/255.0, 1.0)
            self._bg_rgba = bg_rgba
        return self._bg_rgba

    def do_draw(self, cr):
        w = self.get_allocated_width()
        h = self.get_allocated_height()
        iw = self.surface.get_width()
        ih = self.surface.get_height()
        sw = w / iw
        sh = h / ih
        sw = sh = swh = sw if sw < sh else sh

        if self.scale_factor > 0:
            sw = self.scale_factor
            sh = self.scale_factor
        if sw < 0.1 or sh < 0.1:
            sw = sh = 0.1
        x = (w - iw * sw) / 2
        y = (h - ih * sh) / 2

        # log_debug((w,h, iw, ih, sw, sh, x, y, self.scale_factor,))

        # backdrop
        cr.save()
        cr.rectangle(0, 0, w, h)
        Gdk.cairo_set_source_rgba(cr, self.bg_rgba)
        cr.fill()
        cr.restore()

        # image
        cr.save()
        cr.rectangle(x, y, iw * sw, ih * sh)
        cr.clip()

        cr.save()
        cr.scale(sw, sh)
        cr.set_source_surface(self.surface, x / sw, y / sh)
        cr.paint()
        cr.restore()

        cr.restore()
        return True

    def _keypress(self, widget, ev):
        kv = ev.keyval
        ks = ev.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        msg = '{}{}{}{:c} [0x{:x}]'.format(
            'Ctrl-' if k_ctrl else
            '',
            'Alt-' if k_alt else
            '',
            'Shift-' if k_shift else
            '',
            kv, kv
        )
        # log_debug(msg)

        if kv in (
                Gdk.KEY_Escape,
                Gdk.KEY_W, Gdk.KEY_w,
                Gdk.KEY_Q, Gdk.KEY_q,
        ):
            self.close()
            return True
        elif kv in (Gdk.KEY_F, Gdk.KEY_f,):
            if self.is_maximized():
                self.unmaximize()
            else:
                self.maximize()
            return True

        w = self.get_allocated_width()
        h = self.get_allocated_height()
        iw = self.surface.get_width()
        ih = self.surface.get_height()
        sw = w / iw
        sh = h / ih
        swh = sw if sw < sh else sh

        refresh = False
        if kv in (ord('+'),):
            self.scale_factor = (
                (self.scale_factor + 0.1) if self.scale_factor > 0 else
                swh
            )
            refresh = True
        elif kv in (ord('-'),):
            self.scale_factor = (
                (self.scale_factor - 0.1) if self.scale_factor > 0 else
                swh
            )
            refresh = True
        elif kv in (ord('='),):
            self.scale_factor = 1
            refresh = True
        elif kv in (ord('1'), ord('2'), ord('3'), ord('4'), ord('5')):
            self.scale_factor = kv - ord('0')
            refresh = True
        elif kv in (ord('0'),):
            self.scale_factor = 0.5
            refresh = True
        elif kv in (Gdk.KEY_E, Gdk.KEY_e):
            self.scale_factor = swh
            refresh = True

        if refresh:
            # log_debug('{:.2f}'.format(self.scale_factor))
            self.queue_draw()
            return True

        return False
