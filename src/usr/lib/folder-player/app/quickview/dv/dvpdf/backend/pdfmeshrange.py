class LayoutList(list):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_parent(self, mesh):
        self._mesh = mesh
        return self

    def update_info(self):
        self._mesh.update_info()
        return

    def set_value(self, pn):
        super().clear()
        self.append(pn)
        return

    def append(self, i):
        a = []
        if isinstance(i, list):
            a += i
        elif isinstance(i, int):
            a.append(i)

        for pindex in a:
            ppage = self._mesh.get_page(pindex)
            if ppage is None:
                raise RuntimeError(
                    ('Page should not be None {}').format(pindex)
                )
            super().append(pindex)
        self.update_info()
        return

    def insert(self, index, i):
        a = []
        if isinstance(i, list):
            a += i
        elif isinstance(i, int):
            a.append(i)

        a.reverse()
        for pindex in a:
            ppage = self._mesh.get_page(pindex)
            if ppage is None:
                raise RuntimeError(
                    ('Page should not be None {}').format(pindex)
                )
            super().insert(index, pindex)
        self.update_info()
        return

    def remove(self, i):
        a = []
        if isinstance(i, list):
            a += i
        elif isinstance(i, int):
            a.append(i)

        for pindex in a:
            super().remove(pindex)
        self.update_info()
        return
