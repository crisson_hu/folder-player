from enum import Enum
from gi.repository import Gtk, Gdk, Pango, PangoCairo

from tool import HotKey, log_debug
from app.quickview.dv.dvpdf.common.style import Style


class Track(Gtk.Widget):

    __gtype_name__ = 'Track'

    class Constants(Enum):
        DIR_HORIZ_L2R = 'DIR_HORIZ_L2R'
        DIR_HORIZ_R2L = 'DIR_HORIZ_R2L'
        DIR_VERTICAL = 'DIR_VERTICAL'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.style = self.Style(self)
        self.set_direction()

        self.strc = self.Structure(self)
        self.dr = self.Draw(self)
        self.set_can_focus(False)
        return

    def set_range(self, vmin=1, vmax=1):
        self.strc.vmin = vmin
        self.strc.vmax = vmax
        self.queue_draw()
        return

    def set_value(self, val=1):
        self.strc.current_value = val
        self.queue_draw()
        return

    def set_hover_value(self, val=1):
        self.strc.hover_value = val
        self.queue_draw()
        return

    def set_direction(self, direction=Constants.DIR_VERTICAL):
        self.direction = direction
        r = 10
        if direction == self.Constants.DIR_VERTICAL:
            self.set_size_request(r, 1)
        elif direction in (
                self.Constants.DIR_HORIZ_L2R,
                self.Constants.DIR_HORIZ_R2L,
        ):
            self.set_size_request(1, r)
        self.queue_draw()
        return self

    class Structure:

        def __init__(self, parent):
            self.widget = parent
            self.vmin = 1
            self.vmax = 1
            self.current_value = 1
            self.hover_value = 1
            return

        def bar_area_get_geometry(self, page_index=0):
            idx = self.current_value if page_index == 0 else page_index

            width = self.widget.get_allocated_width()
            height = self.widget.get_allocated_height()

            if self.widget.direction in (
                    self.widget.Constants.DIR_HORIZ_L2R,
                    self.widget.Constants.DIR_HORIZ_R2L,
            ):
                y = int(height / 2) - 1
                h = height - 2 * y
                w = int(width * idx / self.vmax)
                x = 0
                if (
                        self.widget.direction ==
                        self.widget.Constants.DIR_HORIZ_R2L
                ):
                    x = width - w
            else:
                # self.Constants.DIR_VERTICAL
                x = int(width / 2) - 1
                w = width - 2 * x
                y = 0
                h = int(height * idx / self.vmax)

            return (x, y, w, h)

        @property
        def bar_indicator_area_list(self):
            # Update indicator balls list
            segs = 10
            if self.widget.direction == self.widget.Constants.DIR_VERTICAL:
                step = self.widget.get_allocated_height() / self.vmax
                MIN_STEP = self.widget.get_screen().get_height() / segs
            else:
                step = self.widget.get_allocated_width() / self.vmax
                MIN_STEP = self.widget.get_screen().get_width() / segs
            pg_step = int((MIN_STEP - 1) / step + 1)
            if pg_step > 5:
                pg_step = int((pg_step - 1) / 5 + 1) * 5
            return list(range(0, self.vmax + 1, pg_step))

        def bar_indicator_area_get_geometry(self, page_index=0):
            if self.widget.direction == self.widget.Constants.DIR_VERTICAL:
                block_w = self.widget.get_allocated_width() / 2
            else:
                block_w = self.widget.get_allocated_height() / 2

            (x, y, w, h) = self.bar_area_get_geometry(page_index=page_index)
            if self.widget.direction == self.widget.Constants.DIR_VERTICAL:
                x = 0
                y += h
                y = int(y - block_w if y >= block_w else 0)
            else:
                x = int(x - block_w if x >= block_w else 0)
                y = 0
                if (
                        self.widget.direction ==
                        self.widget.Constants.DIR_HORIZ_L2R
                ):
                    x += w

            h = int(2 * block_w)
            w = int(2 * block_w)
            return (x, y, w, h)

    def do_draw(self, cr):
        context = self.get_style_context()
        context.save()
        if not self.has_focus():
            if hasattr(self, 'state_prelight') and self.state_prelight:
                context.set_state(Gtk.StateFlags.PRELIGHT)
                self.set_state_flags(Gtk.StateFlags.PRELIGHT, False)
            else:
                self.unset_state_flags(Gtk.StateFlags.PRELIGHT)
        self.dr.draw_bg(cr)
        if self.strc.vmax > 1:
            self.dr.draw_progress(cr)
        context.restore()
        return True

    class Draw:

        def __init__(self, widget):
            self.widget = widget
            self.strc = widget.strc
            self.caret_show_always = True
            return

        def draw_bg(self, cr):
            ctx = self.widget.get_style_context()
            width = self.widget.get_allocated_width()
            height = self.widget.get_allocated_height()
            color = Style.Constants.BGCOLOR.value

            self.render_background(ctx, cr, 0, 0, width, height, color)
            return

        def draw_progress(self, cr):
            ctx = self.widget.get_style_context()

            pg = self.strc.vmax
            color = Style.Constants.PROGRESSCOLOR_BG.value
            self.draw_progress_helper(cr, pg, color)

            pg = self.strc.current_value
            color = Style.Constants.PROGRESSCOLOR.value
            self.draw_progress_helper(cr, pg, color)
            (x, y, w, h) = self.strc.bar_indicator_area_get_geometry(pg)
            self.render_ball(ctx, cr, x, y, w, h, color)

            set1 = set(ctx.get_state().value_names)
            set2 = set(Gtk.StateFlags.PRELIGHT.value_names)
            if set(Gtk.StateFlags.PRELIGHT.value_names).issubset(
                    set(ctx.get_state().value_names)
            ):
                # log_debug(set(ctx.get_state().value_names))
                pg = self.strc.hover_value
                color = Style.Constants.PROGRESSCOLOR_BG_POINTER.value
                self.draw_progress_helper(cr, pg, color)
                (x, y, w, h) = self.strc.bar_indicator_area_get_geometry(pg)
                self.render_ball(ctx, cr, x, y, w, h, color)
            return

        def draw_progress_helper(self, cr, idx, color):
            ctx = self.widget.get_style_context()
            (x, y, w, h) = self.strc.bar_area_get_geometry(page_index=idx)
            self.render_background(ctx, cr, x, y, w, h, color)
            for i in self.strc.bar_indicator_area_list:
                # The current page ball is drawn when i == 0.
                if i == 0:
                    continue
                if i > idx:
                    break
                (x, y, w, h) = self.strc.bar_indicator_area_get_geometry(
                    page_index=i
                )
                self.render_ball(ctx, cr, x, y, w, h, color)
            return

        @staticmethod
        def render_background(ctx, cr, x, y, w, h, rgba):
            cr.save()

            alpha = 1 if len(rgba) <= 7 else (int(rgba[7:9], 16) / 0xff)
            rlist = tuple(int(rgba[i:i+2], 16) for i in (1, 3, 5))
            nrgba = Gdk.RGBA(rlist[0]/255, rlist[1]/255, rlist[2]/255, alpha)
            cr.set_line_width(1)

            cr.rectangle(x, y, w, h)
            Gdk.cairo_set_source_rgba(cr, nrgba)
            cr.fill()

            # cr.rectangle(x, y, w, h)
            # Gdk.cairo_set_source_rgba(cr, nrgba)
            # cr.stroke()

            cr.restore()
            return

        @staticmethod
        def render_ball(ctx, cr, x, y, w, h, rgba):
            cr.save()

            alpha = 1 if len(rgba) <= 7 else (int(rgba[7:9], 16) / 0xff)
            rlist = tuple(int(rgba[i:i+2], 16) for i in (1, 3, 5))
            nrgba = Gdk.RGBA(rlist[0]/255, rlist[1]/255, rlist[2]/255, alpha)
            r = w / 2
            cr.set_line_width(1)

            cr.arc(x + r, y + r, r - 0.5, 0, 7)
            Gdk.cairo_set_source_rgba(cr, nrgba)
            cr.fill()
            cr.arc(x + r, y + r, r - 0.5, 0, 7)
            Gdk.cairo_set_source_rgba(cr, nrgba)
            cr.stroke()

            cr.restore()
            return

    # def do_button_press_event(self, ev):
    #     (tx, ty, tw, th) = self.strc.text_area_get_text_geometry()
    #     if tx <= ev.x and ev.x <= tx + tw and ty <= ev.y and ev.y <= ty + th:
    #         x = (ev.x - tx + self.strc.scroll_x) * Pango.SCALE
    #         line = self.ensure_layout().get_line(0)
    #         (inside, index, trailing) = line.x_to_index(x)
    #         if not inside:
    #             index += 1
    #         self.strc.caret_set(index)

    #     if not self.has_focus():
    #         self.grab_focus()

    # def do_focus_in_event(self, ev):
    #     pass

    # def do_focus_out_event(self, ev):
    #     pass

    # def do_motion_notify_event(self, ev):
    #     pass

    def do_enter_notify_event(self, ev):
        # pointer shape
        w = self.get_window()
        if w:
            w.set_cursor(Gdk.Cursor(
                Gdk.CursorType.RIGHT_PTR
                if self.direction == self.Constants.DIR_VERTICAL else
                Gdk.CursorType.LEFT_PTR
            ))

        self.set_hover_value(self.get_ev_page_number(ev))

        # bg/fg color
        self.state_prelight = True
        self.queue_draw()
        return

    def do_leave_notify_event(self, ev):
        w = self.get_window()
        if w:
            w.set_cursor(None)

        # bg/fg color
        self.state_prelight = False
        self.queue_draw()
        return

    def get_ev_page_number(self, ev):
        if self.direction == self.Constants.DIR_VERTICAL:
            pg = int(ev.y / self.get_allocated_height() * self.strc.vmax + 0.5)
        else:
            pg = int(ev.x / self.get_allocated_width() * self.strc.vmax + 0.5)
            if self.direction == self.Constants.DIR_HORIZ_R2L:
                pg = self.strc.vmax - pg
        return pg

    def do_button_press_event(self, ev):
        self.get_toplevel().goto_page(self.get_ev_page_number(ev))
        return

    def do_motion_notify_event(self, ev):
        self.set_hover_value(self.get_ev_page_number(ev))
        return

    def do_touch_event(self, *args):
        log_debug(args)
        return

    def do_realize(self):
        allocation = self.get_allocation()
        attr = Gdk.WindowAttr()
        attr.window_type = Gdk.WindowType.CHILD
        attr.x = allocation.x
        attr.y = allocation.y
        attr.width = allocation.width
        attr.height = allocation.height
        attr.visual = self.get_visual()
        attr.event_mask = (
            self.get_events() |
            Gdk.EventMask.POINTER_MOTION_MASK |
            Gdk.EventMask.EXPOSURE_MASK |
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK |
            Gdk.EventMask.KEY_PRESS_MASK |
            Gdk.EventMask.KEY_RELEASE_MASK |
            Gdk.EventMask.TOUCH_MASK |
            Gdk.EventMask.FOCUS_CHANGE_MASK |
            Gdk.EventMask.ENTER_NOTIFY_MASK |
            Gdk.EventMask.LEAVE_NOTIFY_MASK |
            Gdk.EventMask.VISIBILITY_NOTIFY_MASK
        )
        WAT = Gdk.WindowAttributesType
        mask = WAT.X | WAT.Y | WAT.VISUAL
        window = Gdk.Window(self.get_parent_window(), attr, mask)
        self.set_window(window)
        self.register_window(window)
        self.set_realized(True)
        window.set_background_pattern(None)

        (
            Gdk.EventMask.ALL_EVENTS_MASK |
            Gdk.EventMask.BUTTON1_MOTION_MASK |
            Gdk.EventMask.BUTTON2_MOTION_MASK |
            Gdk.EventMask.BUTTON3_MOTION_MASK |
            Gdk.EventMask.BUTTON_MOTION_MASK |
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK |
            Gdk.EventMask.ENTER_NOTIFY_MASK |
            Gdk.EventMask.EXPOSURE_MASK |
            Gdk.EventMask.FOCUS_CHANGE_MASK |
            Gdk.EventMask.KEY_PRESS_MASK |
            Gdk.EventMask.KEY_RELEASE_MASK |
            Gdk.EventMask.LEAVE_NOTIFY_MASK |
            Gdk.EventMask.POINTER_MOTION_HINT_MASK |
            Gdk.EventMask.POINTER_MOTION_MASK |
            Gdk.EventMask.PROPERTY_CHANGE_MASK |
            Gdk.EventMask.PROXIMITY_IN_MASK |
            Gdk.EventMask.PROXIMITY_OUT_MASK |
            Gdk.EventMask.SCROLL_MASK |
            Gdk.EventMask.SMOOTH_SCROLL_MASK |
            Gdk.EventMask.STRUCTURE_MASK |
            Gdk.EventMask.SUBSTRUCTURE_MASK |
            Gdk.EventMask.TOUCHPAD_GESTURE_MASK |
            Gdk.EventMask.TOUCH_MASK |
            Gdk.EventMask.VISIBILITY_NOTIFY_MASK
        )

    class Style:

        def __init__(self, widget=None):
            self.widget = widget
            css = ''

            v_base = '''
                {:s} {{
                  box-shadow: none;
                  background-image: none;
                  margin: 0px;
                  padding: 0px;
                  padding-left: 20px;
                  border-width: 1px;
                  border-style: solid;
                  border-radius: 500px;
                  border-radius: 0px;
                }}
            '''.format(self.widget.__gtype_name__)
            css += v_base

            # normal
            BG_COLOR_NORMAL = '#fcfcfc'
            BG_COLOR_NORMAL = '#4a90d9'
            BORDER_COLOR_NORMAL = '#fff'
            FG_COLOR_NORMAL = '#303030'
            BORDER_COLOR_FOCUS = '#1f1f1f'
            v_normal = '''
                {:s} {{
                  background-image: linear-gradient({});
                  color: {};
                  border-color: {};
                  border: 1px dotted {};
                  border: none;
                  border-left: 3px solid {};
                  border-right: 3px solid {};
                }}
            '''.format(
                self.widget.__gtype_name__,
                BG_COLOR_NORMAL,
                FG_COLOR_NORMAL,
                BORDER_COLOR_NORMAL,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
            )
            css += v_normal

            # focus
            BG_COLOR_FOCUS = '#fff'
            FG_COLOR_FOCUS = '#000'
            BORDER_COLOR_FOCUS = '#4a90d9'
            v_focus = '''
                {:s}:focus {{
                  background-image: linear-gradient({});
                  color: {};
                  border: 1px dotted {};
                  border-left: 3px solid {};
                  border-right: 3px solid {};
                }}
            '''.format(
                self.widget.__gtype_name__,
                BG_COLOR_FOCUS,
                FG_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
                BORDER_COLOR_FOCUS,
            )
            css += v_focus

            self.setup_widget_css(css=css)

        def setup_widget_css(self, css=None, style_class=None):
            if not css:
                return
            provider = Gtk.CssProvider.new()
            context = self.widget.get_style_context()
            context.add_provider(
                provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
            b_css = bytes(css, encoding='ascii')
            provider.load_from_data(b_css)
            if style_class:
                context.add_class(style_class)
