from pathlib import Path
from enum import Enum
from gi.repository import Gtk, Gdk


class Style:

    class Constants(Enum):
        CLASS_MAIN_CANVAS = 'main-canvas'
        CLASS_DOCUMENT_PAGE = 'document-page'
        list_colors = [
            '#008b8b',
            '#27408b',
            '#20b2aa',
            '#f2f2f2',
            '#aaaaaa',
            '#bbbbbb',
            '#4a90d9',
            '#8470ff',
            '#ff8c00',
            '#777777',
        ]
        BGCOLOR = list_colors[-1]
        PROGRESSCOLOR = list_colors[-2]
        PROGRESSCOLOR_BG = '#a0a0a0'
        PROGRESSCOLOR_BG_POINTER = '#ffffff7f'

    class Border(Gdk.Rectangle):
        top = 3
        right = 3
        bottom = 6
        left = 4
        spacing = 5

    @staticmethod
    def css_setup():
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            Gdk.Screen.get_default(),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        css_debug = (
            '.{} {{'
            'border-width: {}px {}px {}px {}px;'
            'border-color: #00ff00;'
            'border-style: solid;'
            '}}'
        ).format(
            Style.Constants.CLASS_DOCUMENT_PAGE.value,
            Style.Border.top,
            Style.Border.right,
            Style.Border.bottom,
            Style.Border.left,
        )

        css = (
            '.{} {{'
            '  background-color: {};'
            '}}'
            '.{} {{'
            '  border-style: solid;'
            '  border-width: {}px {}px {}px {}px;'
            '  border-image: '
            '  url("{}/thumbnail-frame.png") {} {} {} {};'
            '  background-color: @theme_bg_color;'
            '  background-color: {};'
            '}}'
        ).format(
            Style.Constants.CLASS_MAIN_CANVAS.value,
            Style.Constants.BGCOLOR.value,
            Style.Constants.CLASS_DOCUMENT_PAGE.value,

            Style.Border.top,
            Style.Border.right,
            Style.Border.bottom,
            Style.Border.left,

            Path(__file__).parent,

            Style.Border.top,
            Style.Border.right,
            Style.Border.bottom,
            Style.Border.left,

            Style.Constants.BGCOLOR.value,
        )

        provider.load_from_data(bytes(css, encoding='ascii'))
        return
