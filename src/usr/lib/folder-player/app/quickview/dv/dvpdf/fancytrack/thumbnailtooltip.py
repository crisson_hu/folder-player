from gi.repository import Gdk, Gtk

from tool import _
from ..common.constants import Constants
from ..common.style import Style
from ..backend.pdfmeshstate import StateLayout


class ThumbnailTooltip(Gtk.Window):

    __gtype_name__ = 'ThumbnailTooltip'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, type=Gtk.WindowType.POPUP)
        self.pindex = 0
        self.setup_widgets()
        return

    def setup_widgets(self):
        self.vb = Gtk.VBox()
        self.add(self.vb)

        self.lbl = Gtk.Label()
        self.lbl.set_text('Page Number')
        self.vb.add(self.lbl)
        self.thumbnail = Gtk.DrawingArea()
        self.thumbnail.add_events(Gdk.EventMask.ALL_EVENTS_MASK)
        self.thumbnail.set_size_request(
            Constants.PreviewWindow.WIDTH,
            Constants.PreviewWindow.HEIGHT
        )
        self.thumbnail.get_style_context().add_class(
            Style.Constants.CLASS_MAIN_CANVAS.value
        )
        self.thumbnail.get_style_context().add_class(
            Style.Constants.CLASS_DOCUMENT_PAGE.value
        )

        self.thumbnail.connect('draw', self.dw_draw)
        self.vb.add(self.thumbnail)
        self.vb.show_all()
        return

    def set_text(self, txt):
        self.lbl.set_text(_('Page {}').format(txt))
        self.pindex = int(txt) - 1
        self.thumbnail.queue_draw()
        return

    def set_mesh(self, mesh):
        self.mesh = mesh
        return self

    def dw_draw(self, widget, cr):
        border = Style.Border
        ppage = self.mesh.get_page(self.pindex)
        if ppage is None:
            return
        (rpx, rpy) = ppage.render_page_size_thumbnail

        # START
        cr.save()
        tx = int((Constants.PreviewWindow.WIDTH - rpx) / 2)
        ty = int((Constants.PreviewWindow.HEIGHT - rpy) / 2)
        cr.translate(tx, ty)
        StateLayout.render_single_page_thumbnail(widget, cr, ppage)
        # END
        cr.restore()
        return True
