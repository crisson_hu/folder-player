from .track import Track
from .thumbnailtooltip import ThumbnailTooltip


class TrackBar(Track):

    __gtype_name__ = 'TrackBar'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setup_tooltip_widget()
        return

    def setup_tooltip_widget(self):
        self.set_tooltip_text('Page Preview')
        self.tw = ThumbnailTooltip()
        self.set_tooltip_window(self.tw)
        self.set_has_tooltip(True)
        return

    def set_mesh(self, mesh):
        self.mesh = mesh
        self.tw.set_mesh(mesh)
        return self

    def set_hover_value(self, val=1):
        if val < 1:
            val = 1
        self.tw.set_text('{}'.format(val))
        super().set_hover_value(val)
        return


class TrackHBarL2R(TrackBar):

    __gtype_name__ = 'TrackHBarL2R'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_direction(self.Constants.DIR_HORIZ_L2R)
        return


class TrackHBarR2L(TrackBar):

    __gtype_name__ = 'TrackHBarR2L'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_direction(self.Constants.DIR_HORIZ_R2L)
        return


class TrackVBar(TrackBar):

    __gtype_name__ = 'TrackVBar'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_direction(self.Constants.DIR_VERTICAL)
        return
