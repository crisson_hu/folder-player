import cairo

from tool import log_debug
from app.quickview.dv.dvpdf.common.style import Style
from app.quickview.dv.dvpdf.common.constants import Constants


class PdfPage:

    def __init__(
            self, rawdoc, rawdoc_prop, rawindex,
            scale_x=1.0,
            scale_y=1.0
    ):
        self._rawdoc = rawdoc
        self._rawdoc_prop = rawdoc_prop
        self.rawindex = rawindex
        if rawindex >= self._rawdoc_prop.n_pages:
            raise ValueError('exceed n_pages: [{}]'.format(rawindex))
        self.rawpage = rawdoc.get_page(rawindex)
        img_list = []
        for i in self.rawpage.get_image_mapping():
            isurf = self.rawpage.get_image(i.image_id)
            img_list.append((
                'isurf: pg {}, id {}, w/h/s {}/{}/{}'.format(
                    rawindex,
                    i.image_id,
                    isurf.get_width(),
                    isurf.get_height(),
                    isurf.get_stride(),
                ),
                # isurf.get_data(),
            ))
        if img_list:
            # log_debug(img_list)
            pass
        self._scale_x = scale_x
        self._scale_y = scale_y
        return

    @property
    def rawpage_size(self):
        if not hasattr(self, '_rawpage_size'):
            (px, py) = self.rawpage.get_size()
            self._rawpage_size = (
                (px * self._scale_x),
                (py * self._scale_y)
            )
        return self._rawpage_size

    @property
    def render_page_size(self):
        if not hasattr(self, '_render_page_size'):
            (px, py) = self.rawpage_size
            self._render_page_size = (
                px + Style.Border.left + Style.Border.right,
                py + Style.Border.top + Style.Border.bottom
            )
        return self._render_page_size

    def get_image_surface(self):
        if not hasattr(self, '_image_surface'):
            (px, py) = self.rawpage_size
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(px), int(py))
            cr = cairo.Context(surface)
            cr.scale(self._scale_x, self._scale_y)
            self.rawpage.render(cr)
            cr.set_operator(cairo.OPERATOR_DEST_OVER)
            cr.set_source_rgb(1.0, 1.0, 1.0)
            cr.paint()
            self._image_surface = surface
        return self._image_surface

    @property
    def rawpage_size_thumbnail(self):
        if not hasattr(self, '_rawpage_size_thumbnail'):
            (px, py) = self.rawpage.get_size()
            sx = (
                Constants.PreviewWindow.WIDTH
                - Style.Border.left - Style.Border.right
            ) / px
            sy = (
                Constants.PreviewWindow.HEIGHT
                - Style.Border.top - Style.Border.bottom
            ) / py
            sxy = sx if sx < sy else sy
            self._rawpage_size_thumbnail = (int(px * sxy), int(py * sxy))
        return self._rawpage_size_thumbnail

    @property
    def render_page_size_thumbnail(self):
        if not hasattr(self, '_render_page_size_thumbnail'):
            (px, py) = self.rawpage_size_thumbnail
            self._render_page_size_thumbnail = (
                px + Style.Border.left + Style.Border.right,
                py + Style.Border.top + Style.Border.bottom
            )
        return self._render_page_size_thumbnail

    def get_image_surface_thumbnail(self):
        if not hasattr(self, '_image_surface_thumbnail'):
            (px, py) = self.rawpage_size
            (px_thm, py_thm) = self.rawpage_size_thumbnail
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, px_thm, py_thm)
            cr = cairo.Context(surface)
            sx = px_thm / px * self._scale_x
            sy = py_thm / py * self._scale_y
            cr.scale(sx, sy)
            self.rawpage.render(cr)
            cr.set_operator(cairo.OPERATOR_DEST_OVER)
            cr.set_source_rgb(1.0, 1.0, 1.0)
            cr.paint()
            self._image_surface_thumbnail = surface
        return self._image_surface_thumbnail

    def dump_stat(self):
        import sys
        return (
            self.rawindex,
            sys.getsizeof(self.rawpage),
            sys.getsizeof(self.get_image_surface()),
        )
