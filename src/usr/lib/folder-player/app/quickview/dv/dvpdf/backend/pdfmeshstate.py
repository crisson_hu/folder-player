from gi.repository import Gdk, Gtk

from tool import log_debug
from app.quickview.dv.dvpdf.common.style import Style


class StateLayout:

    def set_mesh(self, mesh):
        self.mesh = mesh
        return self

    def dw_draw(self, widget, cr):
        # log_debug((
        #     widget.get_state_flags().value_nicks,
        #     widget.get_style_context().list_classes()
        # ))
        self.draw_bg(widget, cr)
        self.draw_visible_pages(widget, cr)
        return True

    def draw_bg(self, widget, cr):
        # draw backdrop
        cr.save()
        # Gtk.render_background(
        #     widget.get_style_context(),
        #     cr, 0, 0,
        #     widget.get_allocated_width(),
        #     widget.get_allocated_height()
        # )
        cr.rectangle(
            0, 0,
            self.mesh.window_geometry[2],
            self.mesh.window_geometry[3],
        )
        bgcolor = Style.Constants.BGCOLOR.value
        bgcolor = bgcolor.lstrip('#')
        rgb = tuple(int(bgcolor[i:i+2], 16) for i in (0, 2, 4))
        Gdk.cairo_set_source_rgba(cr, Gdk.RGBA(
            rgb[0]/255.0, rgb[1]/255.0, rgb[2]/255.0, 1.0
        ))
        cr.fill()
        cr.stroke()
        cr.restore()
        return True

    @staticmethod
    def render_single_page(widget, cr, ppage):
        # draw border
        (rpx, rpy) = ppage.render_page_size
        cr.save()
        ctx = widget.get_style_context()
        Gtk.render_background(ctx, cr, 0, 0, rpx, rpy)
        Gtk.render_frame(ctx, cr, 0, 0, rpx, rpy)
        cr.restore()

        # draw PDF page
        surface = ppage.get_image_surface()
        cr.save()
        surface.set_device_offset(
            - Style.Border.left,
            - Style.Border.top
        )
        cr.set_source_surface(surface, 0, 0)
        cr.paint()
        cr.restore()
        return

    @staticmethod
    def render_single_page_thumbnail(widget, cr, ppage):
        # draw border
        (rpx, rpy) = ppage.render_page_size_thumbnail
        cr.save()
        ctx = widget.get_style_context()
        Gtk.render_background(ctx, cr, 0, 0, rpx, rpy)
        Gtk.render_frame(ctx, cr, 0, 0, rpx, rpy)
        cr.restore()

        # draw PDF page
        surface = ppage.get_image_surface_thumbnail()
        cr.save()
        surface.set_device_offset(
            - Style.Border.left,
            - Style.Border.top
        )
        cr.set_source_surface(surface, 0, 0)
        cr.paint()
        cr.restore()
        return


class StateLayoutVertical(StateLayout):

    def draw_visible_pages(self, widget, cr):
        # translate
        for i in range(len(self.mesh.visible_range)):
            pindex = self.mesh.visible_range[i]
            self._dr_page(
                widget,
                cr,
                pindex,
                order=(i if not self.mesh.layout_dual else int(i / 2))
            )
        return True

    def _dr_page(self, widget, cr, pindex, order=0):
        border = Style.Border
        ppage = self.mesh.get_page(pindex)
        if ppage is None:
            return
        (rpx, rpy) = ppage.render_page_size

        # START
        cr.save()
        tx = (
            self.mesh.canvas_side
            if self.mesh.canvas_side > border.spacing else
            border.spacing
        )
        if self.mesh.layout_dual and pindex % 2 == 1:
            tx += rpx + border.spacing
        ty = (
            self.mesh.canvas_origin
            + border.spacing
            + order * (rpy + border.spacing)
        )
        cr.translate(tx, ty)
        # log_debug((pindex, tx, ty))

        self.render_single_page(widget, cr, ppage)

        # END
        cr.restore()
        return True

    def _sanity_check(self):
        mesh = self.mesh
        vrange = mesh.visible_range
        if len(vrange) == 0:
            return
        if len(vrange) != vrange[-1] - vrange[0] + 1:
            pindex = vrange[0]
            log_debug('[ERROR] length mismatch: {} vs {}'.format(
                len(vrange), vrange[-1] - vrange[0] + 1
            ))
            log_debug(vrange)
            vrange.clear()
            a = [pindex]
            if mesh.layout_dual and pindex + 1 < mesh.n_pages:
                a.append(pindex + 1)
            vrange.append(a)
            log_debug(vrange)
            return

        if mesh.layout_dual:
            pindex = vrange[0]
            if pindex % 2 != 0:
                a = [pindex - 1, pindex]
                log_debug(('align starting page:', vrange, a))
                vrange.clear()
                vrange.append(a)
                return
            pindex = vrange[-1]
            if len(vrange) % 2 != 0 and pindex + 1 < mesh.n_pages:
                a = vrange[:] + [pindex + 1]
                log_debug(('align end pages:', vrange, a))
                vrange.clear()
                vrange.append(a)
        return

    def adjust(self):
        mesh = self.mesh
        vrange = mesh.visible_range
        use_dbg = False
        if mesh.get_page(0) is None:
            log_debug('Empty document: NO WAY to forward')
            return

        self._sanity_check()

        pindex = 0
        if len(vrange) == 0:
            ppage = mesh.get_page(pindex)
            if ppage is not None:
                a = [pindex]
                if mesh.layout_dual and pindex + 1 < mesh.n_pages:
                    pindex += 1
                    a.append(pindex)
                vrange.append(a)
                if use_dbg:
                    log_debug(('append:', a))

        # 1 Check Origin
        # case 1: scroll down
        # remove top page if not visible
        limit = 1 if not mesh.layout_dual else 2
        while len(vrange) > limit:
            # NOTE avoid empty.
            pindex = vrange[0]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpy + Style.Border.spacing
            if mesh.canvas_origin + pext < 0:
                a = [pindex]
                mesh.canvas_origin += pext
                if (mesh.layout_dual and pindex + 1 in vrange):
                    pindex += 1
                    a.append(pindex)
                vrange.remove(a)
                if use_dbg:
                    log_debug(('remove:', a))
            else:
                break

        # 1 Check Origin
        # case 2: scroll up
        # add page if exposed
        pindex = vrange[0]
        while pindex >= 0:
            if mesh.canvas_origin > 0:
                ppage = mesh.get_page(pindex - 1)
                if ppage is None:
                    break
                pindex -= 1
                a = [pindex]
                (rpx, rpy) = ppage.render_page_size
                pext = rpy + Style.Border.spacing
                mesh.canvas_origin -= pext
                if (
                        mesh.layout_dual and
                        mesh.get_page(pindex - 1)
                ):
                    pindex -= 1
                    a.insert(0, pindex)
                vrange.insert(0, a)
                if use_dbg:
                    log_debug(('insert:', a))
            else:
                break

        # The 1st page: avoid scrolling up above the 1st page.
        if pindex == 0:
            if mesh.canvas_origin > 0:
                # Don't return here, because the window geometry would
                # usually be changed shortly, should let further
                # actions being performed to add more pages.
                mesh.canvas_origin = 0
                if use_dbg:
                    log_debug('adjust & return: hit top')

        # 2 Check Bottom
        height = mesh.window_geometry[3]
        if use_dbg:
            log_debug(mesh.window_geometry)
        length = mesh.canvas_origin
        for i in range(
                0,
                len(vrange),
                1 if not mesh.layout_dual else 2
        ):
            pindex = vrange[i]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpy + Style.Border.spacing
            length += pext
            # NOTE: FIXME
            # On window create, the window height is not correct.
            # So it's better to use the screen height and remove less pages.
            #
            # if length > mesh._dw.get_screen().get_height():
            #
            # NOW: things seem good.
            if length > height:
                limit = 1 if not mesh.layout_dual else 2
                a = vrange[i + limit:]
                vrange.remove(a)
                if use_dbg:
                    log_debug(('remove:', a, length, height))
                break

        pindex = vrange[-1]
        while length < height:
            ppage = mesh.get_page(pindex + 1)
            if ppage is None:
                break
            pindex += 1
            a = [pindex]
            if (
                    mesh.layout_dual and
                    mesh.get_page(pindex + 1)
            ):
                pindex += 1
                a.append(pindex)
            vrange.append(a)
            if use_dbg:
                log_debug(('append:', a))
            (rpx, rpy) = ppage.render_page_size
            pext = rpy + Style.Border.spacing
            length += pext

        # The last page: avoid scrolling down the last page.
        pindex = vrange[-1]
        if pindex == mesh.n_pages - 1:
            length = mesh.canvas_origin
            # 1st: make sum.
            for i in range(
                    0,
                    len(vrange),
                    1 if not mesh.layout_dual else 2
            ):
                pindex = vrange[i]
                ppage = mesh.get_page(pindex)
                (rpx, rpy) = ppage.render_page_size
                pext = rpy + Style.Border.spacing
                length += pext
            # 2nd: check.
            if length + Style.Border.spacing < height:
                if mesh.canvas_origin < 0:
                    mesh.canvas_origin += (
                        height - length - Style.Border.spacing
                    )
                    pindex = vrange[0]
                    while pindex >= 0:
                        # Try to scroll backward.
                        if mesh.canvas_origin > 0:
                            ppage = mesh.get_page(pindex - 1)
                            if ppage is None:
                                if use_dbg:
                                    log_debug(
                                        'cannot scroll backward,'
                                        'leave space at the end of canvas'
                                    )
                                mesh.canvas_origin = 0
                                break
                            pindex -= 1
                            a = [pindex]
                            (rpx, rpy) = ppage.render_page_size
                            pext = rpy + Style.Border.spacing
                            mesh.canvas_origin -= pext
                            if (
                                    mesh.layout_dual and
                                    mesh.get_page(pindex - 1)
                            ):
                                pindex -= 1
                                a.insert(0, pindex)
                            vrange.insert(0, a)
                            if use_dbg:
                                log_debug(('insert:', a))
                        else:
                            break
                else:
                    mesh.canvas_origin = 0
                if use_dbg:
                    log_debug(('adjust & return: hit bottom', vrange))
                return

        if use_dbg:
            log_debug(vrange)
        return


class StateLayoutSingle(StateLayout):

    def draw_visible_pages(self, widget, cr):
        # translate
        for i in range(len(self.mesh.visible_range)):
            pindex = self.mesh.visible_range[i]
            self._dr_page(
                widget,
                cr,
                pindex,
                order=(i if not self.mesh.layout_dual else int(i / 2))
            )
            break
        return True

    def _dr_page(self, widget, cr, pindex, order=0):
        border = Style.Border
        ppage = self.mesh.get_page(pindex)
        if ppage is None:
            return
        (rpx, rpy) = ppage.render_page_size

        # START
        cr.save()
        tx = (
            self.mesh.canvas_side
            if self.mesh.canvas_side > border.spacing + border.left else
            border.spacing + border.left
        ) - border.left
        if self.mesh.layout_dual and pindex % 2 == 1:
            tx += rpx + border.spacing
        ty = self.mesh.canvas_origin + order * (
            rpy + border.spacing
        ) + border.spacing
        cr.translate(tx, ty)
        # log_debug((pindex, tx, ty))

        self.render_single_page(widget, cr, ppage)

        # END
        cr.restore()
        return True

    def adjust(self):
        return


class StateLayoutL2R(StateLayout):

    def draw_visible_pages(self, widget, cr):
        # translate
        for i in range(len(self.mesh.visible_range)):
            pindex = self.mesh.visible_range[i]
            self._dr_page(widget, cr, pindex, order=i)
        return True

    def _dr_page(self, widget, cr, pindex, order=0):
        border = Style.Border
        ppage = self.mesh.get_page(pindex)
        if ppage is None:
            return
        (rpx, rpy) = ppage.render_page_size

        # START
        cr.save()
        tx = (
            self.mesh.canvas_origin
            + border.spacing
            + order * (rpx + border.spacing)
        )
        ty = (
            self.mesh.canvas_side
            if self.mesh.canvas_side > border.spacing else
            border.spacing
        )
        cr.translate(tx, ty)
        # log_debug((pindex, tx, ty))

        self.render_single_page(widget, cr, ppage)

        # END
        cr.restore()
        return True

    def _sanity_check(self):
        mesh = self.mesh
        vrange = mesh.visible_range
        if len(vrange) == 0:
            return
        if len(vrange) != vrange[-1] - vrange[0] + 1:
            pindex = vrange[0]
            log_debug('[ERROR] length mismatch: {} vs {}'.format(
                len(vrange), vrange[-1] - vrange[0] + 1
            ))
            log_debug(vrange)
            vrange.clear()
            a = [pindex]
            vrange.append(a)
            log_debug(vrange)
            return
        return

    def adjust(self):
        use_dbg = True
        use_dbg = False
        mesh = self.mesh
        vrange = mesh.visible_range
        extent = mesh.window_geometry[2]
        if use_dbg:
            log_debug((
                mesh.window_geometry,
                mesh._dw.get_screen().get_width(),
                mesh._dw.get_screen().get_height(),
            ))
        if mesh.get_page(0) is None:
            log_debug('Empty document: NO WAY to forward')
            return

        self._sanity_check()

        pindex = 0
        if len(vrange) == 0:
            ppage = mesh.get_page(pindex)
            if ppage is not None:
                a = [pindex]
                vrange.append(a)
                if use_dbg:
                    log_debug(('append:', a))

        # 1 Check Origin
        # case 1: scroll forward
        # remove front page if invisible
        while len(vrange) > 1:
            # NOTE avoid empty
            pindex = vrange[0]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            if mesh.canvas_origin + pext < 0:
                a = [pindex]
                vrange.remove(a)
                mesh.canvas_origin += pext
                if use_dbg:
                    log_debug(('remove:', a))
            else:
                break

        # 1 Check Origin
        # case 2: scroll backward
        # add page if extra space exposed
        pindex = vrange[0]
        while pindex >= 0:
            if mesh.canvas_origin > 0:
                ppage = mesh.get_page(pindex - 1)
                if ppage is None:
                    # See following code to deal with the 1st page.
                    break
                (rpx, rpy) = ppage.render_page_size
                pext = rpx + Style.Border.spacing
                mesh.canvas_origin -= pext
                pindex -= 1
                a = [pindex]
                vrange.insert(0, a)
                if use_dbg:
                    log_debug(('insert:', a))
            else:
                break

        # The 1st page: avoid scrolling backward beyond it.
        pindex = vrange[0]
        if pindex == 0:
            if mesh.canvas_origin > 0:
                # Don't return here, because the window geometry would
                # usually be changed shortly, should let further
                # actions being performed to add more pages.
                mesh.canvas_origin = 0
                if use_dbg:
                    log_debug('hit front')

        # 2 Check End
        # Remove invisible trailing pages.
        position = mesh.canvas_origin
        for i in range(len(vrange)):
            pindex = vrange[i]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            position += pext
            if position > extent:
                a = vrange[i + 1:]
                vrange.remove(a)
                if use_dbg:
                    log_debug(('remove:', a, position, extent))
                break

        # Add trailing pages to fill extra space.
        pindex = vrange[-1]
        while position < extent:
            ppage = mesh.get_page(pindex + 1)
            if ppage is None:
                # See last page checking code.
                break
            pindex += 1
            a = [pindex]
            vrange.append(a)
            if use_dbg:
                log_debug(('append:', a))
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            position += pext

        # The last page: avoid scrolling beyond the last page
        pindex = vrange[-1]
        if pindex == mesh.n_pages - 1:
            position = mesh.canvas_origin
            # 1st: make sum.
            for i in range(len(vrange)):
                pindex = vrange[i]
                ppage = mesh.get_page(pindex)
                (rpx, rpy) = ppage.render_page_size
                pext = rpx + Style.Border.spacing
                position += pext
            # 2nd: check.
            if position + Style.Border.spacing < extent:
                if mesh.canvas_origin < 0:
                    mesh.canvas_origin += (
                        extent - position - Style.Border.spacing
                    )
                    pindex = vrange[0]
                    while pindex >= 0:
                        # Try to scroll backward
                        if mesh.canvas_origin > 0:
                            ppage = mesh.get_page(pindex - 1)
                            if ppage is None:
                                if use_dbg:
                                    log_debug(
                                        'cannot scroll backward,'
                                        'leave space at the end of canvas'
                                    )
                                mesh.canvas_origin = 0
                                break
                            pindex -= 1
                            a = [pindex]
                            (rpx, rpy) = ppage.render_page_size
                            pext = rpx + Style.Border.spacing
                            mesh.canvas_origin -= pext
                            vrange.insert(0, a)
                            if use_dbg:
                                log_debug(('insert:', a))
                        else:
                            break
                else:
                    mesh.canvas_origin = 0
                if use_dbg:
                    log_debug(('adjust & return: hit bottom', vrange))
                return

        if use_dbg:
            log_debug(vrange)
        return


class StateLayoutR2L(StateLayout):

    def draw_visible_pages(self, widget, cr):
        # translate
        for i in range(len(self.mesh.visible_range)):
            pindex = self.mesh.visible_range[i]
            self._dr_page(widget, cr, pindex, order=i)
        return True

    def _dr_page(self, widget, cr, pindex, order=0):
        border = Style.Border
        ppage = self.mesh.get_page(pindex)
        if ppage is None:
            return
        (rpx, rpy) = ppage.render_page_size

        # START
        cr.save()
        width = self.mesh.window_geometry[2]
        tx = (
            width
            + self.mesh.canvas_origin
            - (order + 1) * (rpx + border.spacing)
        )
        ty = (
            self.mesh.canvas_side
            if self.mesh.canvas_side > border.spacing else
            border.spacing
        )
        cr.translate(tx, ty)
        # log_debug((pindex, tx, ty))

        self.render_single_page(widget, cr, ppage)

        # END
        cr.restore()
        return True

    def _sanity_check(self):
        mesh = self.mesh
        vrange = mesh.visible_range
        if len(vrange) == 0:
            return
        if len(vrange) != vrange[-1] - vrange[0] + 1:
            pindex = vrange[0]
            log_debug('[ERROR] length mismatch: {} vs {}'.format(
                len(vrange), vrange[-1] - vrange[0] + 1
            ))
            log_debug(vrange)
            vrange.clear()
            a = [pindex]
            vrange.append(a)
            log_debug(vrange)
            return
        return

    def adjust(self):
        use_dbg = True
        use_dbg = False
        mesh = self.mesh
        vrange = mesh.visible_range
        extent = mesh.window_geometry[2]
        if use_dbg:
            log_debug((
                mesh.window_geometry,
                mesh._dw.get_screen().get_width(),
                mesh._dw.get_screen().get_height(),
            ))
        if mesh.get_page(0) is None:
            log_debug('Empty document: NO WAY to forward')
            return

        self._sanity_check()

        pindex = 0
        if len(vrange) == 0:
            ppage = mesh.get_page(pindex)
            if ppage is not None:
                a = [pindex]
                vrange.append(a)
                if use_dbg:
                    log_debug(('append:', a))

        # 1 Check Origin
        # case 1: scroll forward
        # remove front page if invisible
        while len(vrange) > 1:
            # NOTE avoid empty
            pindex = vrange[0]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            if mesh.canvas_origin - pext > 0:
                a = [pindex]
                vrange.remove(a)
                mesh.canvas_origin -= pext
                if use_dbg:
                    log_debug(('remove:', a))
            else:
                break

        # 1 Check Origin
        # case 2: scroll backward
        # add page if extra space exposed
        pindex = vrange[0]
        while pindex >= 0:
            if mesh.canvas_origin < 0:
                ppage = mesh.get_page(pindex - 1)
                if ppage is None:
                    # See following code to deal with the 1st page.
                    break
                (rpx, rpy) = ppage.render_page_size
                pext = rpx + Style.Border.spacing
                mesh.canvas_origin += pext
                pindex -= 1
                a = [pindex]
                vrange.insert(0, a)
                if use_dbg:
                    log_debug(('insert:', a))
            else:
                break

        # The 1st page: avoid scrolling backward beyond it.
        pindex = vrange[0]
        if pindex == 0:
            if mesh.canvas_origin < 0:
                # Don't return here, because the window geometry would
                # usually be changed shortly, should let further
                # actions being performed to add more pages.
                mesh.canvas_origin = 0
                if use_dbg:
                    log_debug('hit front')

        # 2 Check End
        # Remove invisible trailing pages.
        position = mesh.canvas_origin
        for i in range(len(vrange)):
            pindex = vrange[i]
            ppage = mesh.get_page(pindex)
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            position -= pext
            if extent + position < 0:
                a = vrange[i + 1:]
                vrange.remove(a)
                if use_dbg:
                    log_debug(('remove:', a, position, extent))
                break

        # Add trailing pages to fill extra space.
        pindex = vrange[-1]
        while extent + position > 0:
            ppage = mesh.get_page(pindex + 1)
            if ppage is None:
                break
            pindex += 1
            a = [pindex]
            vrange.append(a)
            if use_dbg:
                log_debug(('append:', a))
            (rpx, rpy) = ppage.render_page_size
            pext = rpx + Style.Border.spacing
            position -= pext

        # The last page: avoid scrolling beyond the last page
        pindex = vrange[-1]
        if pindex == mesh.n_pages - 1:
            position = mesh.canvas_origin
            # 1st: make sum.
            for i in range(len(vrange)):
                pindex = vrange[i]
                ppage = mesh.get_page(pindex)
                (rpx, rpy) = ppage.render_page_size
                pext = rpx + Style.Border.spacing
                position -= pext
            # 2nd: check.
            if extent + position > Style.Border.spacing:
                if mesh.canvas_origin > 0:
                    mesh.canvas_origin -= (
                        extent + position - Style.Border.spacing
                    )
                    pindex = vrange[0]
                    while pindex >= 0:
                        # Try to scroll backward
                        if mesh.canvas_origin < 0:
                            ppage = mesh.get_page(pindex - 1)
                            if ppage is None:
                                if use_dbg:
                                    log_debug(
                                        'cannot scroll backward,'
                                        'leave space at the end of canvas'
                                    )
                                mesh.canvas_origin = 0
                                break
                            pindex -= 1
                            a = [pindex]
                            (rpx, rpy) = ppage.render_page_size
                            pext = rpx + Style.Border.spacing
                            mesh.canvas_origin += pext
                            vrange.insert(0, a)
                            if use_dbg:
                                log_debug(('insert:', a))
                        else:
                            break
                else:
                    mesh.canvas_origin = 0
                if use_dbg:
                    log_debug(('adjust & return: hit bottom', vrange))
                return

        if use_dbg:
            log_debug(vrange)
        return
