from dbuslite import store_lite
from dbuslite.components import QuickView as LayoutConstant
from tool import log_debug
from .pdfpage import PdfPage
from .pdfmeshstate import (
    StateLayoutVertical,
    StateLayoutSingle,
    StateLayoutL2R,
    StateLayoutR2L,
)
from .pdfmeshrange import LayoutList
from app.quickview.dv.dvpdf.common.style import Style


class PdfMesh:

    def __init__(self, parent, draw_widget):
        self._parent = parent
        self._rawdoc = parent.poppler_document
        self._rawdoc_prop = parent.file_prop_pdf
        self._dw = draw_widget
        self._toplevel = self._dw.get_toplevel()
        self.setup()
        self.visible_range = LayoutList().set_parent(self)
        self._toplevel.connect(
            'configure-event',
            self.configure_event_hdl
        )
        self._dw.connect('draw', self.dw_draw)
        self._dw.connect('realize', self.realized_hdl)
        return

    def realized_hdl(self, *args):
        self.state_transition()
        return True

    def adjust_queue_draw(self):
        self.op.adjust()
        self._dw.queue_draw()
        return True

    def setup(self):
        # setup style
        self._dw.get_style_context().add_class(
            Style.Constants.CLASS_MAIN_CANVAS.value
        )
        self._dw.get_style_context().add_class(
            Style.Constants.CLASS_DOCUMENT_PAGE.value
        )
        Style.css_setup()
        # setup state_ops
        self.op_vert = StateLayoutVertical().set_mesh(self)
        self.op_one = StateLayoutSingle().set_mesh(self)
        self.op_l2r = StateLayoutL2R().set_mesh(self)
        self.op_r2l = StateLayoutR2L().set_mesh(self)
        # canvas param
        self.canvas_origin = 0
        return True

    @property
    def state(self):
        return store_lite.quick_view_layout_mode

    def dw_draw(self, *args):
        return self.op.dw_draw(*args)

    def state_transition(self):
        log_debug((self.state, self._parent.orientation.value_nick))
        self.op = (
            self.op_vert if self.state == LayoutConstant.LAYOUT_ONECOLUMN else
            self.op_vert if self.state == LayoutConstant.LAYOUT_DUALCOLUMN else
            self.op_one if self.state == LayoutConstant.LAYOUT_SINGLEPAGE else
            self.op_l2r if self.state == LayoutConstant.LAYOUT_LEFT2RIGHT else
            self.op_r2l if self.state == LayoutConstant.LAYOUT_RIGHT2LEFT else
            self.op_vert
        )
        self._parent.orientation_update()
        self.compute_geometry()
        self.adjust_queue_draw()
        return True

    def configure_event_hdl(self, w, ev):
        self.compute_geometry()
        self.adjust_queue_draw()
        # NOTE: return False so that other handlers can be called.
        return False

    def compute_geometry(self):
        self.canvas_side = self._compute_canvas_side()
        return True

    def _compute_canvas_side(self):
        (x, y, w, h) = self.window_geometry
        (rpx, rpy) = self.get_page(0).render_page_size
        if self.layout_vertical:
            val = int((
                w - 2 * rpx - Style.Border.spacing if self.layout_dual else
                w - rpx
            ) / 2)
        else:
            val = ((h - rpy) / 2)
        val = val if val > 0 else 0
        return val

    @property
    def layout_dual(self):
        return self.state == LayoutConstant.LAYOUT_DUALCOLUMN

    @property
    def layout_vertical(self):
        return self.state in (
            LayoutConstant.LAYOUT_ONECOLUMN,
            LayoutConstant.LAYOUT_DUALCOLUMN,
            LayoutConstant.LAYOUT_SINGLEPAGE,
        )

    @property
    def window_geometry(self):
        (x, y, w, h) = self._dw.get_toplevel().get_window().get_geometry()
        (dx, dy, dw, dh) = self._dw.get_window().get_geometry()
        h -= dy
        return (x, y, w, h)

    def scroll_origin(
            self,
            step=True,
            delta_step=False,
            delta_val=0,
            large_step=False,
            forward=True,
            page=False
    ):
        if page:
            if forward:
                self.scroll_page(1)
            else:
                self.scroll_page(-1)
            return True
        pace = (
            int(self.window_geometry[3]/2) if large_step else
            delta_val if delta_step else
            20 if step else
            20
        )
        if forward:
            self.canvas_origin -= pace
        else:
            self.canvas_origin += pace
        self.adjust_queue_draw()
        return True

    def scroll_side(
            self,
            step=True,
            delta_step=False,
            delta_val=0,
            large_step=False,
            forward=True,
            page=False
    ):
        if page:
            if forward:
                self.scroll_page(1)
            else:
                self.scroll_page(-1)
            return True
        pace = (
            int(self.window_geometry[2]/2) if large_step else
            delta_val if delta_step else
            20 if step else
            20
        )
        if forward:
            self.canvas_side -= pace
        else:
            self.canvas_side += pace
        self.adjust_queue_draw()
        return True

    @property
    def screen_dpi(self):
        if not hasattr(self, '_screen_dpi'):
            from math import hypot
            screen = self._dw.get_screen()
            w = screen.get_width()
            h = screen.get_height()
            dp = hypot(w, h)
            if dp == 0:
                return 96
            w = screen.get_monitor_width_mm(0)
            h = screen.get_monitor_height_mm(0)
            di = hypot(w, h)
            if di == 0:
                return 96
            dpi = int(dp / di)
            if dpi < 96:
                dpi = 96
            self._screen_dpi = dpi
        return self._screen_dpi

    @property
    def scale_x(self):
        return self.screen_dpi / 72.0

    @property
    def scale_y(self):
        return self.screen_dpi / 72.0

    @property
    def cache_page(self):
        if not hasattr(self, '_cache_page'):
            self._cache_page = {}
        return self._cache_page

    def scroll_page(self, np):
        pindex = self.visible_range[0]
        new_pindex = pindex + np
        # When new_pindex == -1, it will show the 1st page,
        # so `-1` is a valid value.
        if new_pindex < -1 or new_pindex >= self.n_pages:
            # log_debug('wrong index np: {}'.format(new_pindex))
            return
        (rpx, rpy) = self.get_page(pindex).render_page_size
        pext = (
            rpy if self.layout_vertical else rpx
        ) + Style.Border.spacing
        if self.state == LayoutConstant.LAYOUT_RIGHT2LEFT:
            self.canvas_origin += (np * pext)
        else:
            self.canvas_origin -= (np * pext)
        self.adjust_queue_draw()
        return

    @property
    def n_pages(self):
        if not hasattr(self, '_n_pages'):
            self._n_pages = self._rawdoc_prop.n_pages
        return self._n_pages

    def get_page(self, rawindex):
        if self.cache_page.get(rawindex):
            return self.cache_page.get(rawindex)
        if rawindex < 0 or rawindex >= self.n_pages:
            return None

        ppage = PdfPage(
            self._rawdoc, self._rawdoc_prop, rawindex,
            scale_x=self.scale_x,
            scale_y=self.scale_y
        )
        self.cache_page[rawindex] = ppage
        return ppage

    def get_surface(self, rawindex):
        return self.get_page(rawindex).get_image_surface()

    def update_info(self):
        self._parent.update_info()
        return
