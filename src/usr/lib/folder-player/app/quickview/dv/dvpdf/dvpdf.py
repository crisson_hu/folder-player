from pathlib import Path
from gi.repository import GLib, Gdk, Gtk, Poppler

from dbuslite import store_lite
from dbuslite.components import QuickView as LayoutConstant
from tool import HotKey, log_debug, FileProp, FilePropPdf
from app.quickview.bar import BarEditor
from ..dvbase import DialogViewBase
from .backend.pdfmesh import PdfMesh
from .fancytrack import (
    TrackHBarL2R,
    TrackHBarR2L,
    TrackVBar
)


class DialogViewPdf(DialogViewBase):

    __gtype_name__ = 'DialogViewPdf'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connect('key-press-event', self._on_key_press)
        self.mesh = None
        self.vrange_bak = []
        self.sb_visible = True
        return

    def setup_widgets(self):
        self._overlay = Gtk.Overlay()
        self.add(self._overlay)
        self.main_vb = Gtk.VBox()
        self._overlay.add(self.main_vb)

        self._ebar = BarEditor().shift_visible(visible=False) \
                                .set_save_action(self.action_save) \
                                .set_more_action(self.action_more) \
                                .set_info_action(self.action_info) \
                                .set_range_action(self.action_range) \
                                .set_fit_action(self.action_fit) \
                                .set_layout_action(self.action_layout)
        self.main_vb.add(self._ebar)
        self._ebar.set_vexpand(False)
        self._ebar.set_valign(Gtk.Align.START)
        self._ebar.show_all()
        self.main_vb.child_set_property(self._ebar, 'expand', False)

        self.rv_top = Gtk.Revealer()
        self.sb_top = TrackHBarL2R()
        self.rv_top.add(self.sb_top)
        self.main_vb.add(self.rv_top)
        self.main_vb.child_set_property(self.rv_top, 'expand', False)

        self.vw_hb = Gtk.HBox()
        self.main_vb.add(self.vw_hb)
        self.main_vb.child_set_property(self.vw_hb, 'expand', True)

        self.rv_side = Gtk.Revealer()
        self.sb_side = TrackVBar()
        self.rv_side.add(self.sb_side)
        self._tv = Gtk.Viewport()
        self._init_pan(self._tv)
        self.vw_hb.add(self._tv)
        self.vw_hb.add(self.rv_side)
        self.vw_hb.child_set_property(self.rv_side, 'expand', False)

        self.rv_top.set_reveal_child(True)
        self.rv_top.set_no_show_all(True)
        self.rv_side.set_reveal_child(True)
        self.rv_side.set_no_show_all(True)

        self.connect('realize', self._on_realized)
        return

    def _on_realized(self, widget):
        self._setup_cursor_normal(widget)
        self._setup_mask(widget)
        return True

    def _setup_cursor(self, widget, cursor):
        win = widget.get_window()
        win.set_cursor(cursor)
        return True

    def _setup_cursor_drag(self, *args):
        cursor = Gdk.Cursor(Gdk.CursorType.FLEUR)
        self._setup_cursor(self, cursor)
        return True

    def _setup_cursor_normal(self, *args):
        cursor = Gdk.Cursor(Gdk.CursorType.FLEUR)
        cursor = Gdk.Cursor(Gdk.CursorType.HAND2)
        cursor = Gdk.Cursor(Gdk.CursorType.HAND1)
        cursor = Gdk.Cursor(Gdk.CursorType.ARROW)
        self._setup_cursor(self, cursor)
        return True

    def set_pathitem(self, *args, **kwargs):
        super().set_pathitem(*args, **kwargs)
        self._ebar.refresh_tooltip()
        f = 'file://{}'.format(self._pi)
        self._poppler_document = Poppler.Document.new_from_file(f, None)
        self._file_prop_pdf = FilePropPdf(self.poppler_document)
        self.mesh = PdfMesh(self, self._tv)
        for w in (self.sb_top, self.sb_side):
            w.set_mesh(self.mesh)
        self.update_info()
        return self

    def action_info(self, argv, *args):
        tip = '{}{}'.format(
            FileProp.get_stat(self._pi),
            '\n\n' + self.file_prop_pdf.stat_info if self.file_prop_pdf else ''
        )
        argv[0].set_tooltip_text(tip)
        return True

    def update_info(self):
        GLib.idle_add(self._upinfo_cb)
        return True

    def _upinfo_cb(self):
        begin = None
        end = None
        # sanity check
        if not hasattr(self.mesh, 'visible_range'):
            log_debug('[visible_range] not available')
            return
        if len(self.mesh.visible_range) == 0:
            log_debug('[visible_range] empty: {}-{}'.format(begin, end))
            return
        if self.vrange_bak == self.mesh.visible_range[:]:
            # log_debug('[visible_range] DUP: {}'.format(self.vrange_bak))
            return

        self.vrange_bak = self.mesh.visible_range[:]
        begin = self.mesh.visible_range[0] + 1
        end = self.mesh.visible_range[-1] + 1

        txt = '{}{}/{}'.format(
            begin if begin is not None else '',
            ' - {}'.format(end) if end is not None else '',
            self.file_prop_pdf.n_pages,
        )
        self._ebar._brange.update_range(txt)
        log_debug((
            '{}/{}'.format(txt, len(self.mesh.visible_range),),
            self.mesh.visible_range,
        ))

        if self.orientation == Gtk.Orientation.VERTICAL:
            self.sb_side.set_range(1, self.file_prop_pdf.n_pages)
            self.sb_side.set_value(end)
        else:
            self.sb_top.set_range(1, self.file_prop_pdf.n_pages)
            self.sb_top.set_value(end)

        return

    def goto_page(self, pn):
        n = len(self.mesh.visible_range)
        self.mesh.visible_range.set_value(list(range(pn))[-n:])
        self.mesh.adjust_queue_draw()
        return True

    def action_range(self, *args):
        self.sb_visible_toggle()
        return True

    def action_fit(self, *args):
        log_debug(args)
        return True

    def action_layout(self, *args):
        self.mesh.state_transition()
        return True

    @property
    def poppler_document(self):
        if not hasattr(self, '_poppler_document'):
            self._poppler_document = None
        return self._poppler_document

    @property
    def file_prop_pdf(self):
        if not hasattr(self, '_file_prop_pdf'):
            self._file_prop_pdf = None
        return self._file_prop_pdf

    @property
    def orientation(self):
        if not hasattr(self, '_orientation'):
            self.orientation_update()
        return self._orientation

    def orientation_update(self):
        state = store_lite.quick_view_layout_mode
        self._orientation = (
            Gtk.Orientation.VERTICAL if state in (
                LayoutConstant.LAYOUT_ONECOLUMN,
                LayoutConstant.LAYOUT_DUALCOLUMN,
            ) else
            Gtk.Orientation.HORIZONTAL if state in (
                LayoutConstant.LAYOUT_LEFT2RIGHT,
                LayoutConstant.LAYOUT_RIGHT2LEFT,
            ) else
            Gtk.Orientation.VERTICAL
        )
        self._setup_pan()
        self._setup_sb()
        return

    def _init_pan(self, widget):
        self.pgest_v = Gtk.GesturePan.new(widget, Gtk.Orientation.VERTICAL)
        self.pgest_h = Gtk.GesturePan.new(widget, Gtk.Orientation.HORIZONTAL)
        return

    def _setup_pan(self):
        for w in (self.pgest_v, self.pgest_h):
            for fn in (self._on_gesture_pan, self._on_gesture_end):
                try:
                    w.disconnect_by_func(fn)
                except TypeError as e:
                    pass
                except Exception as e:
                    log_debug('unknown error')
                finally:
                    pass

        w = (
            self.pgest_v if self.orientation == Gtk.Orientation.VERTICAL else
            self.pgest_h
        )
        w.connect('pan', self._on_gesture_pan, w)
        w.connect('end', self._on_gesture_end, w)
        return

    def _setup_sb(self):
        if self.orientation == Gtk.Orientation.VERTICAL:
            self.rv_top.set_no_show_all(True)
            self.rv_top.set_visible(False)
            self.rv_side.set_no_show_all(not self.sb_visible)
            self.rv_side.set_visible(self.sb_visible)
            if self.sb_visible:
                self.rv_side.show_all()
        else:
            state = store_lite.quick_view_layout_mode
            self.sb_top.set_direction(
                self.sb_top.Constants.DIR_HORIZ_L2R if
                state == LayoutConstant.LAYOUT_LEFT2RIGHT else
                self.sb_top.Constants.DIR_HORIZ_R2L
            )
            self.rv_top.set_no_show_all(not self.sb_visible)
            self.rv_top.set_visible(self.sb_visible)
            if self.sb_visible:
                self.rv_top.show_all()
            self.rv_side.set_no_show_all(True)
            self.rv_side.set_visible(False)
        return

    def sb_visible_toggle(self):
        self.sb_visible = not self.sb_visible
        self._setup_sb()
        return

    @property
    def pan_offset(self):
        if not hasattr(self, '_pan_offset'):
            self._pan_offset = 0
        return self._pan_offset

    @pan_offset.setter
    def pan_offset(self, val):
        self._pan_offset = val
        return

    def _on_gesture_pan(self, gesture, direction, offset, widget):
        (is_pan, delta_offset) = self._pan_helper(gesture)
        if is_pan:
            # log_debug((is_pan, delta_offset))
            self.mesh.scroll_origin(delta_step=True, delta_val=delta_offset)
        gesture.set_state(Gtk.EventSequenceState.CLAIMED)
        return True

    def _on_gesture_end(self, gesture, sequence, widget):
        (is_pan, delta_offset) = self._pan_helper(gesture)
        if is_pan:
            # log_debug((is_pan, delta_offset))
            self.mesh.scroll_origin(delta_step=True, delta_val=delta_offset)
            self.pan_offset = 0
        gesture.set_state(Gtk.EventSequenceState.CLAIMED)
        return True

    def _pan_helper(self, gesture):
        (is_active, x, y) = gesture.get_offset()
        is_pan = False
        delta_offset = 0
        if is_active and gesture.get_orientation() == self.orientation:
            is_pan = True
            axis = (
                y if self.orientation == Gtk.Orientation.VERTICAL else
                x
            )
            delta_offset = - (axis - self.pan_offset)
            self.pan_offset = axis
        return (is_pan, delta_offset)

    def _setup_mask(self, widget):
        win = widget.get_window()
        # mask = (
        #     win.get_events()
        #     | Gdk.EventMask.POINTER_MOTION_MASK
        #     | Gdk.EventMask.SCROLL_MASK
        # )
        mask = Gdk.EventMask.ALL_EVENTS_MASK
        win.set_events(mask)

        for e in (
                'scroll-event',
                'button-press-event',
                'button-release-event',
                'touch-event',
                'drag-begin',
                'drag-data-delete',
                'drag-data-get',
                'drag-data-received',
                'drag-drop',
                'drag-end',
                'drag-failed',
                'drag-leave',
                'drag-motion',
        ):
            widget.connect(e, self._on_gesture_misc)

        return True

    def _on_gesture_misc(self, widget, ev):
        msg = 'event: {}'.format(ev.type.value_nick,)

        if ev.type == Gdk.EventType.SCROLL:
            msg += ', {} {}, ({:.4f}, {:.4f})'.format(
                ev.direction.real,
                ev.direction.value_nick,
                ev.delta_x,
                ev.delta_y,
            )
            delta = ev.delta_x + ev.delta_y
            if self.mesh.state == LayoutConstant.LAYOUT_RIGHT2LEFT:
                delta = ev.delta_x - ev.delta_y
            delta *= self.mesh.screen_dpi
            self.mesh.scroll_origin(delta_step=True, delta_val=delta)
        elif ev.type == Gdk.EventType.TOUCHPAD_SWIPE:
            msg += ''
        elif ev.type == Gdk.EventType.TOUCH_BEGIN:
            msg += ''
        elif ev.type == Gdk.EventType.TOUCH_UPDATE:
            msg += ''
        elif ev.type == Gdk.EventType.TOUCH_END:
            msg += ''
        elif ev.type == Gdk.EventType.BUTTON_PRESS:
            msg += ''
            self._setup_cursor_drag()
        elif ev.type == Gdk.EventType.BUTTON_RELEASE:
            msg += ''
            self._setup_cursor_normal()

        # log_debug(msg)
        return True

    def _on_key_press(self, widget, ekey):
        kv = ekey.keyval
        ks = ekey.state
        (k_ctrl, k_alt, k_shift) = HotKey.get_ctrl_alt_shift(kv, ks)
        if kv == Gdk.KEY_Escape or (kv in (Gdk.KEY_W, Gdk.KEY_w) and k_ctrl):
            self.close()
            return True
        elif (
                kv in (Gdk.KEY_P, Gdk.KEY_p) or
                HotKey.is_backspace(kv) or
                HotKey.is_page_up(kv)
        ):
            msg = 'kv page up'
            self.mesh.scroll_origin(
                step=False,
                forward=False,
                page=True,
                large_step=HotKey.is_backspace(kv)
            )
            return True
        elif (
                kv in (Gdk.KEY_N, Gdk.KEY_n) or
                HotKey.is_space(kv) or
                HotKey.is_page_down(kv)
        ):
            msg = 'kv page down'
            self.mesh.scroll_origin(
                step=False,
                forward=True,
                page=True,
                large_step=HotKey.is_space(kv)
            )
            return True
        elif kv in (Gdk.KEY_I, Gdk.KEY_i):
            self.dump_stat()
            return True
        elif (
                HotKey.is_up(kv) or
                kv in (Gdk.KEY_K, Gdk.KEY_k)
        ):
            msg = 'kv UP'
            if self.orientation == Gtk.Orientation.VERTICAL:
                self.mesh.scroll_origin(step=True, forward=False)
            else:
                self.mesh.scroll_side(step=True, forward=False)
            return True
        elif (
                HotKey.is_down(kv) or
                HotKey.is_activate(kv) or
                kv in (Gdk.KEY_J, Gdk.KEY_j)
        ):
            msg = 'kv DOWN'
            if self.orientation == Gtk.Orientation.VERTICAL:
                self.mesh.scroll_origin(step=True, forward=True)
            else:
                self.mesh.scroll_side(step=True, forward=True)
            return True
        elif (
                HotKey.is_left(kv) or
                kv in (Gdk.KEY_H, Gdk.KEY_h)
        ):
            msg = 'kv UP'
            if self.orientation == Gtk.Orientation.VERTICAL:
                self.mesh.scroll_side(step=True, forward=False)
            else:
                self.mesh.scroll_origin(step=True, forward=False)
            return True
        elif (
                HotKey.is_right(kv) or
                kv in (Gdk.KEY_L, Gdk.KEY_l)
        ):
            msg = 'kv DOWN'
            if self.orientation == Gtk.Orientation.VERTICAL:
                self.mesh.scroll_side(step=True, forward=True)
            else:
                self.mesh.scroll_origin(step=True, forward=True)
            return True

        return False

    def dump_stat(self):
        import sys
        a = []
        for i in range(self._file_prop_pdf.n_pages):
            ppage = self.mesh.cache_page.get(i)
            if ppage is None:
                continue
            a.append(ppage.dump_stat())
        log_debug(a)
        return
