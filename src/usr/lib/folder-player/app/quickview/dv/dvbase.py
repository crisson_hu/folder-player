from gi.repository import Gtk

from tool import log_debug, APP_WMCLASS, _, FileProp
from base import DialogBase


class DialogViewBase(DialogBase):

    __gtype_name__ = 'DialogViewBase'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.window_init_maximized = True
        self._pi = None
        self._pi_addr = None
        self.setup_widgets()
        return

    def set_mainview(self, mainview):
        super().set_mainview(mainview)
        '''Override parent's set_mainview()'''
        self.set_transient_for(None)
        self.set_modal(False)
        self.set_destroy_with_parent(True)
        self.set_skip_pager_hint(False)
        self.set_skip_taskbar_hint(False)
        self.set_wmclass(APP_WMCLASS, APP_WMCLASS)
        self.window_move()
        if self.window_init_maximized:
            self.maximize()
        return self

    @property
    def moved(self):
        if not hasattr(self, '_moved'):
            self._moved = False
        return self._moved

    @moved.setter
    def moved(self, move):
        self._moved = move
        return

    def window_move(self, *args):
        if not self._mainwindow:
            return
        if self.moved:
            return
        self.moved = True
        screen = self.get_screen()
        ww = screen.get_width()
        wh = screen.get_height()
        self.resize(0.7 * ww, 0.85 * wh)
        self.set_position(Gtk.WindowPosition.CENTER)
        return

    def action_info(self, argv, *args):
        try:
            tip = FileProp.get_stat(self._pi)
            argv[0].set_tooltip_text(tip)
        except FileNotFoundError:
            pass
        return True

    def action_save(self, *args):
        log_debug(args)
        return True

    def action_fit(self, *args):
        log_debug(args)
        return True

    def action_layout(self, *args):
        log_debug(args)
        return True

    def action_more(self, *args):
        log_debug(args)
        return True

    def set_pathitem(self, pathitem=None, address=None, pre_save=False):
        if not pathitem and not address:
            raise ValueError('Should pass in NON-None value')
        self._pi_addr = address if address is not None else self._pi_addr
        self._pi = (
            pathitem if None in (pathitem, self._pi_addr) else
            self._pi_addr / pathitem
        )
        if pathitem is None:
            self.set_title(_('Create Text File') + '....')
            self.is_can_modify = True
        else:
            self.set_title(self._pi.name)
        return self
