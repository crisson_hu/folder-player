from gi.repository import Gtk, GtkSource

from tool import log_debug, _
from base import WidgetBaseMixin


class TextWidget(GtkSource.View, WidgetBaseMixin):

    __gtype_name__ = 'TextWidget'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fn = None
        self.mngr = GtkSource.LanguageManager.get_default()
        self._bar = None
        self.style_context_add_class()
        self.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        # margin
        self._set_margin()
        self._set_style()
        return

    def set_text(self, text=''):
        bf = self.get_buffer()
        bf.set_text(text)

        if not self.fn:
            return
        lang = self.mngr.guess_language(self.fn)
        if not lang:
            lang = TextLanguage()
        self.set_language(lang.get_id())
        return

    def set_language(self, lang_id):
        lang = self.mngr.get_language(lang_id)
        bf = self.get_buffer()
        bf.set_language(lang)
        if not lang:
            lang = TextLanguage()
        if not self._ebar:
            return
        txt = '{}  [ {} ]'.format(lang.get_name(), lang.get_id(),)
        log_debug(txt)
        self._ebar._blanguage.set_lang(txt)
        return

    def set_ebar(self, ebar):
        self._ebar = ebar
        self._ebar._blanguage.set_tv(self)
        return self

    @staticmethod
    def guess_language(filename):
        return GtkSource.LanguageManager.get_default().guess_language(filename)

    @staticmethod
    def get_language_list():
        a = [(TextLanguage().get_id(), TextLanguage().get_name())]
        mngr = GtkSource.LanguageManager.get_default()
        skip_id = 'def'
        for i in mngr.get_language_ids():
            if i == skip_id:
                continue
            a.append((i, mngr.get_language(i).get_name()))
        return a

    def set_filename(self, fn):
        self.fn = str(fn)
        return self

    def get_full_text(self):
        tb = self.get_buffer()
        return tb.get_text(tb.get_start_iter(), tb.get_end_iter(), False)

    def _set_margin(self):
        margin = 5
        self.set_left_margin(margin)
        self.set_right_margin(margin)
        self.set_top_margin(margin + 5)
        self.set_bottom_margin(margin + 5)
        return

    def _set_style(self):
        self.set_show_line_numbers(True)
        self.set_show_right_margin(True)
        self.set_highlight_current_line(True)
        self.set_tab_width(8)
        self.set_indent_width(8)
        return

    def set_editable(self, edit=True):
        super().set_editable(edit)
        self.set_cursor_visible(edit)
        # On edit, place cursor at the beginning.
        tb = self.get_buffer()
        tb.place_cursor(tb.get_start_iter())
        return


class TextLanguage(GtkSource.Language):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def get_name(self):
        return _('Plain Text')

    def get_id(self):
        return 'Plain Text'
