from gi.repository import Gdk, Gtk

from tool import _
from base import DialogBase
from app.icons import IconButton


class DialogFileName(DialogBase):

    __gtype_name__ = 'DialogFileName'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title(_('Make Directory'))
        self.set_deletable(False)
        self.set_decorated(False)
        self.set_resizable(False)
        self.connect('key-press-event', self._on_key_press)
        return

    def set_cb(self, cb=None):
        if cb is None:
            return
        self._cb = cb
        return self

    def _setup_widgets(self):
        vbox = Gtk.VBox()
        self.add(vbox)

        label = Gtk.Label()
        label.set_text('{} ...'.format(_('Save As')))
        vbox.add(label)

        hbox = Gtk.HBox()
        vbox.add(hbox)

        self._entry = Gtk.Entry()
        hbox.add(self._entry)
        w = self._mainwindow.get_size()[0] * 2 / 3
        self._entry.set_property('width-request', w)

        _cancel = IconButton().set_icon('dialog/cancel.png')
        hbox.add(_cancel)
        hbox.child_set_property(_cancel, 'expand', False)
        _cancel.connect('primary-single-click', self._close)

        _confirm = IconButton().set_icon('dialog/confirm.png')
        hbox.add(_confirm)
        hbox.child_set_property(_confirm, 'expand', False)
        _confirm.connect('primary-single-click', self._do_cb)

        return self

    def set_mainwindow(self, main_win):
        super().set_mainwindow(main_win)
        self._setup_widgets()
        return self

    def _on_key_press(self, widget, ekey):
        kv = ekey.keyval
        if kv == Gdk.KEY_Escape:
            self._close()
            return True
        if kv in (Gdk.KEY_Return, Gdk.KEY_ISO_Enter, Gdk.KEY_KP_Enter,):
            if self._entry.get_text():
                self._do_cb()
                return True
        return False

    def _close(self, *args):
        self.close()
        return True

    def _do_cb(self, *args):
        if not self._cb:
            return True
        self._cb(
            self._pathitem / self._entry.get_text() if self._pathitem
            else self._entry.get_text()
        )
        self.close()
        return True
