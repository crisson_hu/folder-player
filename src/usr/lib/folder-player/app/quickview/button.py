from pathlib import Path
from gi.repository import Gdk, Gtk

from tool import _
from dbuslite import store_lite
from dbuslite.components import QuickView
from base import ButtonBaseMixin, WidgetBaseMixin
from app.theme import set_prelight


class ButtonAction(Gtk.EventBox, WidgetBaseMixin, ButtonBaseMixin):

    __gtype_name__ = 'ButtonAction'
    __gtype_diabled_name__ = 'ButtonActionDisabled'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.icon = None
        self._action = None
        self.label = Gtk.Label()
        self.set_can_focus(False)
        self.set_can_default(False)
        self.set_property('margin-left', 5)
        self.set_property('margin-right', 5)

        self.index = -1

        self.gmp = Gtk.GestureMultiPress.new(self)
        self.gmp.set_button(0)
        self.gmp.set_exclusive(True)
        self._ges_sid = self.gmp.connect(
            'pressed', self._on_press, *args
        )

        self.set_cursor_hand1()

        return

    def do_enter_notify_event(self, ev):
        set_prelight(self, True)
        set_prelight(self.label, True)
        return

    def do_leave_notify_event(self, ev):
        set_prelight(self, False)
        set_prelight(self.label, False)
        return

    def set_index(self, index):
        self.index = index
        return self

    def set_text(self, label):
        if self.label not in self.get_children():
            self.add(self.label)
            self.label.set_property('margin', 0)
            self.label.set_property('margin-left', 10)
            self.label.set_property('margin-right', 10)
        self.label.set_markup(label)
        self.set_tooltip_markup(label)
        return self

    def set_icon(self, icon=None):
        if not icon:
            return
        for c in self.get_children():
            self.remove(c)
        p = str(
            Path(__file__).parent.parent /
            'icons' / 'res' /
            icon
        )
        self.icon = Gtk.Image.new_from_file(p)
        self.add(self.icon)
        self.show_all()
        return self

    def set_action(self, action):
        self._action = action
        return self

    def connect(self, sig_name, cb, *args):
        if sig_name != 'pressed':
            sig_id = super().connect(sig_name, cb, *args)
        else:
            sig_id = self._ges_sid
        return sig_id

    @property
    def enable(self):
        if not hasattr(self, '_enable'):
            self._enable = True
            self.get_style_context().remove_class(self.__gtype_diabled_name__)
        return self._enable

    @enable.setter
    def enable(self, enable=True):
        self._enable = enable
        sc = self.get_style_context()
        name = self.__gtype_diabled_name__
        if self._enable:
            if sc.has_class(name):
                sc.remove_class(name)
        else:
            if not sc.has_class(name):
                sc.add_class(name)
        return


class ButtonActionSave(ButtonAction):

    def _on_press(self, gesture, n_press, x, y):
        if not self.enable:
            # NO-OP
            return True
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 1:
            if self._action:
                self._action((gesture, n_press, x, y))
        return True


class ButtonActionInfo(ButtonAction):

    def refresh_tooltip(self):
        if self._action:
            self._action((self,))
        return True

    def _on_press(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 1:
            if self._action:
                self._action((self, gesture, n_press, x, y))
        return True


class ButtonActionLanguage(ButtonAction):

    def set_lang(self, txt):
        self.set_text(txt)
        self.set_no_show_all(False)
        self.show_all()
        return

    def set_tv(self, tv):
        self._tv = tv
        return

    def _on_press(self, gesture, n_press, x, y):
        screen, rx, ry = gesture.get_device().get_position()
        self.menu_lang.popup(
            rx - x,
            ry - y + self.get_allocated_height(),
        )
        return True

    @property
    def menu_lang(self):
        if not hasattr(self, '_menu_lang'):
            from app.menus.menuquickviewlang import MenuQuickViewLang
            self._menu_lang = MenuQuickViewLang().set_button(self)
        return self._menu_lang

    def update_lang(self, lang_id):
        self._tv.set_language(lang_id)
        return True


class ButtonActionRange(ButtonAction):

    def update_range(self, txt):
        self.set_text(txt)
        self.set_no_show_all(False)
        self.show_all()
        return

    def _on_press(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 1:
            if self._action:
                self._action((self, gesture, n_press, x, y))
        return True


class ButtonActionFit(ButtonAction):

    __gtype_name__ = 'ButtonActionFit'
    __extra_style_classes__ = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__extra_style_classes__.append(ButtonAction.__gtype_name__)
        self.style_context_add_class()
        self._update_icon()
        return

    def _on_press(self, gesture, n_press, x, y):
        screen, rx, ry = gesture.get_device().get_position()
        self.menu_fit.popup(
            rx - x,
            ry - y + self.get_allocated_height(),
        )
        return True

    @property
    def menu_fit(self):
        if not hasattr(self, '_menu_repeat'):
            from app.menus.menuquickviewfit import MenuQuickViewFit
            self._menu_fit = MenuQuickViewFit().set_button(self)
        return self._menu_fit

    def _update_icon(self):
        fit = store_lite.quick_view_fit_mode
        self.set_icon('dialog/fit/{}'.format(
            'fitnone.png' if fit == QuickView.FIT_NONE else
            'fitwidth.png' if fit == QuickView.FIT_WIDTH else
            'fitheight.png' if fit == QuickView.FIT_HEIGHT else
            'fitpage.png' if fit == QuickView.FIT_PAGE else
            'fitnone.png'
        ))
        self.set_tooltip_markup(_(fit))
        return self

    def update_fit_mode(self):
        self._update_icon()
        if self._action:
            self._action()
        return True


class ButtonActionLayout(ButtonAction):

    __gtype_name__ = 'ButtonActionLayout'
    __extra_style_classes__ = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__extra_style_classes__.append(ButtonAction.__gtype_name__)
        self.style_context_add_class()
        self._update_icon()
        return

    def _on_press(self, gesture, n_press, x, y):
        screen, rx, ry = gesture.get_device().get_position()
        self.menu_layout.popup(
            rx - x,
            ry - y + self.get_allocated_height(),
        )
        return True

    @property
    def menu_layout(self):
        if not hasattr(self, '_menu_layout'):
            from app.menus.menuquickviewlayout import MenuQuickViewLayout
            self._menu_layout = MenuQuickViewLayout().set_button(self)
        return self._menu_layout

    def _update_icon(self):
        layout = store_lite.quick_view_layout_mode
        self.set_icon('dialog/layout/{}'.format(
            'onecolumn.png' if layout == QuickView.LAYOUT_ONECOLUMN else
            'dualcolumn.png' if layout == QuickView.LAYOUT_DUALCOLUMN else
            'singlepage.png' if layout == QuickView.LAYOUT_SINGLEPAGE else
            'left2right.png' if layout == QuickView.LAYOUT_LEFT2RIGHT else
            'right2left.png' if layout == QuickView.LAYOUT_RIGHT2LEFT else
            'onecolumn.png'
        ))
        self.set_tooltip_markup(_(layout))
        return self

    def update_layout_mode(self):
        self._update_icon()
        if self._action:
            self._action()
        return True
