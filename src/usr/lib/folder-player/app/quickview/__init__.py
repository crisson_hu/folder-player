from .quickview import QuickView, show_quick_view

__all__ = [
    'QuickView',
    'show_quick_view',
]
