from app.constants import Constants
# from tool import log_debug
from .dv import (
    DialogViewText,
    DialogViewPdf,
    DialogViewImg,
    DialogViewProperty,
)
from .textwidget import TextWidget


class QuickView:

    __gtype_name__ = 'QuickView'

    @property
    def ui(self):
        if not hasattr(self, '_ui'):
            self._ui = None
        return self._ui

    @ui.setter
    def ui(self, view):
        if type(view) not in (
                DialogViewText,
                DialogViewPdf,
                DialogViewImg,
                DialogViewProperty,
        ):
            raise TypeError('bad view type {}'.format(view))
        if hasattr(self, '_ui'):
            raise RuntimeError('already set view {}'.format(self._ui))

        self._ui = view
        # class_name = str(type(view)).split('\'')[-2].split('.')[-1]
        # log_debug('set view: {}'.format(class_name))
        return

    @property
    def text_mode(self):
        if not hasattr(self, '_text_mode'):
            self._text_mode = False
        return self._text_mode

    @text_mode.setter
    def text_mode(self, txt_mode):
        self._text_mode = txt_mode
        return

    def set_pathitem(self, pathitem=None, address=None, pre_save=False):
        fn = str(pathitem).lower() if pathitem else ''
        self.ui = (
            DialogViewText() if self.text_mode else
            DialogViewText() if (
                fn == "" or
                fn.endswith('.txt') or
                TextWidget.guess_language(fn)
            ) else
            DialogViewPdf() if fn.endswith('.pdf') else
            DialogViewImg() if any(
                fn.endswith(i) for i in Constants.FileSuffix.IMAGES
            ) else
            DialogViewProperty()
        )
        self.ui.set_pathitem(
            pathitem=pathitem, address=address, pre_save=pre_save
        )
        return self

    def set_mainview(self, mainview):
        self.ui.set_mainview(mainview)
        return self

    def set_text_mode(self, txt_mode):
        self.text_mode = txt_mode
        return self

    def show(self):
        self.ui.show()
        return self


def show_quick_view(text_mode=False, pathitem=None, mainview=None):
    win = (
        QuickView()
        .set_text_mode(text_mode)
        .set_pathitem(pathitem)
        .set_mainview(mainview)
        .show()
    )
    win.is_can_modify = False
    return show_quick_view
