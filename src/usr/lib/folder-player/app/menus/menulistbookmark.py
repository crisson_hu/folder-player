from pathlib import Path
from gi.repository import Gtk, Gdk

from tool import _, log_debug
from dbuslite import store_lite
from app.dialogs import DialogMoveCopyTo
from base import MenuBase
from .menubookmarkop import BookmarkOp


class MenuListBookmark(MenuBase):

    __gtype_name__ = 'MenuListBookmark'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def show(self):
        self.show_all()
        return

    def _setup(self):
        mi = Gtk.MenuItem('{}....'.format(_('Input Path')))
        mi.connect('activate', self._do_op, None)
        mi.show()
        self.append(mi)

        mi = Gtk.SeparatorMenuItem()
        mi.show()
        self.append(mi)

        use_sep = False
        if (
                store_lite.last_dir != store_lite.last_longest_dir and
                store_lite.last_longest_dir not in store_lite.bookmark
        ):
            use_sep = True
            i = store_lite.last_longest_dir + '/'
            mi = Gtk.MenuItem(i)
            mi.connect('activate', self._do_op, i)
            mi.show()
            self.add(mi)

        if use_sep:
            mi = Gtk.SeparatorMenuItem()
            mi.show()
            self.append(mi)

        if len(store_lite.bookmark) > 0:
            store_lite.bookmark.sort()
            for i in store_lite.bookmark:
                mi = Gtk.MenuItem(i + '/')
                mi.connect('activate', self._do_op, i)
                mi.show()
                self.add(mi)

        return self

    def set_op(self, op):
        self._op = op
        self._setup()
        return self

    def _do_op(self, widget, i):
        if self._op in (BookmarkOp.OP_COPY, BookmarkOp.OP_MOVE):
            DialogMoveCopyTo()\
                .set_mainview(self._mainview) \
                .set_pathitem(self._pathitem) \
                .set_pathitem_dest(Path(i) if i else None) \
                .set_op(self._op) \
                .show()
        else:
            log_debug('invalid OP ' + self._op)
        return True

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def set_pathitem(self, pathitem):
        self._pathitem = pathitem
        return self

    @property
    def x(self):
        if not hasattr(self, '_x'):
            self._x = -1
        return self._x

    @x.setter
    def x(self, _x):
        self._x = _x
        return

    @property
    def y(self):
        if not hasattr(self, '_y'):
            self._y = -1
        return self._y

    @y.setter
    def y(self, _y):
        self._y = _y
        return

    @property
    def device(self):
        if not hasattr(self, '_device'):
            self._device = None
        return self._device

    @device.setter
    def device(self, dev):
        self._device = dev
        return

    def popup(self, x, y, device=None):
        self.x = x
        self.y = y
        self.device = device
        if self.device:
            _screen, gx, gy = device.get_position()
            # Move menu with `offset` to avoid error click.
            offset = 4
            self.x = gx + offset
            self.y = gy + offset
        super().popup(
            None,
            None,

            # position func, e.g. None,
            lambda p1, p2, p3, p4: (self.x, self.y, False),

            None,

            # ev.button,
            1,
            # ev.time
            Gtk.get_current_event_time()
        )
        return self
