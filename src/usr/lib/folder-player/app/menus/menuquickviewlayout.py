from gi.repository import Gdk, Gtk

from dbuslite import store_lite
from dbuslite.components import QuickView as QuickViewConstant
from tool import _, log_debug
from .menuimageitem import MenuImageItem
from base import MenuBase


class MenuQuickViewLayout(MenuBase):

    __gtype_name__ = 'MenuQuickViewLayout'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.button = None
        return

    def _clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self._clear()

        val = QuickViewConstant.LAYOUT_ONECOLUMN
        item = MenuImageItem.new_with_label(
            _(val),
            icon='dialog/layout/onecolumn.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        val = QuickViewConstant.LAYOUT_DUALCOLUMN
        item = MenuImageItem.new_with_label(
            _(val),
            icon='dialog/layout/dualcolumn.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        item = Gtk.SeparatorMenuItem()
        item.show_all()
        self.append(item)

        val = QuickViewConstant.LAYOUT_SINGLEPAGE
        item = MenuImageItem.new_with_label(
            _(val),
            icon='dialog/layout/singlepage.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        item = Gtk.SeparatorMenuItem()
        item.show_all()
        self.append(item)

        val = QuickViewConstant.LAYOUT_LEFT2RIGHT
        item = MenuImageItem.new_with_label(
            _(val),
            icon='dialog/layout/left2right.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        val = QuickViewConstant.LAYOUT_RIGHT2LEFT
        item = MenuImageItem.new_with_label(
            _(val),
            icon='dialog/layout/right2left.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        return

    def _activate_mi(self, widget, mode):
        log_debug(mode)
        store_lite.quick_view_layout_mode = mode
        self.button.update_layout_mode()
        return True

    def set_button(self, btn):
        self.button = btn
        return self

    def popup(self, x, y):
        self._setup()
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (
                x,
                y,
                False
            ),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return
