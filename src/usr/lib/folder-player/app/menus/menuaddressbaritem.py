from pathlib import Path
from gi.repository import Gdk, Gtk

from tool import _, log_debug
from dbuslite import store_lite
from base import MenuBase
from app.dialogs import (
    DialogMkdir,
    DialogMkfile,
    DialogRFC,
)
from app.quickview import QuickView
from .menuimageitem import MenuImageItem, MenuImageItemBookmark


class MenuAddressBarItem(MenuBase):

    __gtype_name__ = 'MenuAddressBarItem'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_address(self, address):
        self._address = address
        self._setup()
        return self

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def _setup(self):
        item = Gtk.MenuItem.new_with_label(_('More Operations'))
        item.set_submenu(self._submenu_ops())
        item.show()
        self.append(item)

        d = list(i.name for i in filter(
            lambda x: not str(x.name).startswith('.') and x.is_dir(),
            Path(self._address).glob('*')
        ))
        if not d:
            return

        item = Gtk.SeparatorMenuItem()
        item.show()
        self.append(item)

        d.sort(key=str.lower)
        for p in d:
            bm = MenuImageItemBookmark.BM_NONE
            name = str(Path(self._address) / p)
            if name in store_lite.bookmark:
                bm = MenuImageItemBookmark.BM
            else:
                for i in store_lite.bookmark:
                    if i.startswith(name):
                        bm = MenuImageItemBookmark.BM_SUBDIR
                        break

            item = MenuImageItemBookmark().new_with_label(p + '/', bm=bm)
            item.connect('activate', self._show_directory, p)
            item.set_tooltip_text(_('Open Sub Folder:') + p)
            item.show()
            self.append(item)

        return

    def _submenu_ops(self):
        menu = Gtk.Menu()

        mi = Gtk.MenuItem.new_with_label(_('Create Folder') + '....')
        mi.connect('activate', self._ops_mkdir)
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label(_('Create Empty File') + '....')
        mi.connect('activate', self._ops_mkfile)
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label(_('Create Text File') + '....')
        mi.connect('activate', self._ops_mk_txt_file)
        mi.show()
        menu.append(mi)

        mi = Gtk.SeparatorMenuItem()
        mi.show()
        menu.append(mi)

        self.mi_add = MenuImageItemBookmark().new_with_label(
            '', bm=MenuImageItemBookmark.BM
        )
        self.mi_add.connect('activate', self._add_bookmark)
        self.mi_add.show()
        menu.append(self.mi_add)

        self.mi_rm = MenuImageItemBookmark().new_with_label(
            '', bm=MenuImageItemBookmark.BM_NONE
        )
        self.mi_rm.connect('activate', self._rm_bookmark)
        self.mi_rm.show()
        menu.append(self.mi_rm)

        mi = Gtk.SeparatorMenuItem()
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label(_('Open Terminal Here') + '....')
        mi.connect('activate', self._ops_openterminal)
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label(_('Copy Location'))
        mi.connect('activate', self._ops_copy_location)
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label(_('Refresh'))
        mi.connect('activate', self._ops_refresh)
        mi.show()
        menu.append(mi)

        mi = Gtk.SeparatorMenuItem()
        mi.show()
        menu.append(mi)

        mi = Gtk.MenuItem.new_with_label('RFC....')
        mi.connect('activate', self._ops_rfc)
        mi.show()
        menu.append(mi)

        return menu

    def _add_bookmark(self, *args):
        if self._address not in store_lite.bookmark:
            store_lite.bookmark.append(self._address)
            store_lite.bookmark.sort()
            self._mainview.list_directory()
        return True

    def _rm_bookmark(self, *args):
        if self._address in store_lite.bookmark:
            store_lite.bookmark.remove(self._address)
            self._mainview.list_directory()
        return True

    def popup(self, x, y):
        do_add = False if self._address in store_lite.bookmark else True
        fmt = '<span{}>{}</span>'
        color = ' foreground="lightgray"'
        self.mi_add.set_sensitive(do_add)
        self.mi_add.set_markup(
            fmt.format(color if not do_add else '', _('Add Bookmark'))
        )
        self.mi_rm.set_sensitive(not do_add)
        self.mi_rm.set_markup(
            fmt.format(color if do_add else '', _('Remove Bookmark'))
        )

        super().popup(
            None,
            None,

            # position func, e.g. None,
            lambda p1, p2, p3, p4: (x, y, False),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return

    def _show_directory(self, widget, pathitem):
        self._mainview.list_directory(str(Path(self._address) / pathitem))
        return True

    def _ops_mkdir(self, *args):
        DialogMkdir() \
            .set_mainview(self._mainview) \
            .set_pathitem(Path(self._address)).show()
        return True

    def _ops_mkfile(self, *args):
        DialogMkfile() \
            .set_mainview(self._mainview) \
            .set_pathitem(Path(self._address)).show()
        return True

    def _ops_mk_txt_file(self, *args):
        QuickView() \
            .set_pathitem(address=Path(self._address)) \
            .set_mainview(self._mainview) \
            .show()
        return True

    def _ops_openterminal(self, *args):
        import os
        pwd = os.environ['PWD']
        os.chdir(self._address)
        os.system('x-terminal-emulator'.format(self._address))
        os.chdir(pwd)
        return True

    def _ops_copy_location(self, *args):
        clipboard = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(self._address, -1)
        return True

    def _ops_refresh(self, *args):
        self._mainview.list_directory()
        return True

    def _ops_rfc(self, *args):
        DialogRFC() \
            .set_mainview(self._mainview) \
            .set_pathitem(Path(self._address)).show()
        return True
