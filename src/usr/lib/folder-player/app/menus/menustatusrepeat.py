from gi.repository import Gdk, Gtk

from dbuslite import store_lite, Player as PlayerConstant
from tool import _, log_debug
from .menuimageitem import MenuImageItem
from base import MenuBase


class MenuStatusRepeat(MenuBase):

    __gtype_name__ = 'MenuStatusRepeat'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._store = store_lite
        self.button = None
        return

    def _clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self._clear()

        val = PlayerConstant.REPEAT_NONE
        item = MenuImageItem.new_with_label(
            _(val),
            icon='status/repeat-none.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        val = PlayerConstant.REPEAT_CURRENT
        item = MenuImageItem.new_with_label(
            _(val),
            icon='status/repeat-one.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        val = PlayerConstant.REPEAT_SELECTED
        item = MenuImageItem.new_with_label(
            _(val),
            icon='status/repeat-selected.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        val = PlayerConstant.REPEAT_ALL
        item = MenuImageItem.new_with_label(
            _(val),
            icon='status/repeat-all.png'
        )
        item.connect('activate', self._activate_mi, val)
        item.show_all()
        self.append(item)

        return

    def _activate_mi(self, widget, mode):
        log_debug(mode)
        self._store.player_repeat_mode = mode
        self.button.update_repeat_mode()
        return True

    def set_button(self, btn):
        self.button = btn
        return self

    def popup(self, x, y):
        self._setup()
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (
                x,
                y - self.get_allocated_height(),
                False
            ),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return
