from gi.repository import Gtk, Gdk

from tool import _, log_debug
from app.dialogs import (
    DialogRename,
    DialogRemove,
    DialogProperty,
)
from app.quickview import QuickView
from base import MenuBase
from .menulistbookmark import MenuListBookmark
from .menubookmarkop import BookmarkOp


class MenuFileItem(MenuBase):

    __gtype_name__ = 'MenuFileItem'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_filelistitem(self, filelistitem):
        self._filelistitem = filelistitem
        return self

    def set_pathitem(self, pathitem):
        self._pathitem = pathitem
        self._setup()
        return self

    def set_mainview(self, mainview, container=None):
        self._mainview = mainview
        self._container = container
        return self

    def _setup(self):
        suffix = (
            str(self._pathitem).split('.')[-1].lower()
            if str(self._pathitem).find('.') != -1
            else None
        )
        if suffix in (
                'html', 'js', 'md', 'py', 'rs', 'sh', 'txt', 'ts',
        ) or not self._pathitem.is_dir():
            label = '<span font-weight="normal">{}</span>'.format(
                _('Quick View')
            )
            item = Gtk.MenuItem.new_with_label('')
            child = item.get_child()
            child.set_markup(label)
            item.connect('activate', self._show_quickview_dialog)
            item.show()
            self.append(item)

        if self._pathitem.is_dir():
            item = Gtk.MenuItem.new_with_label(_('Copy Location'))
            item.connect('activate', self._ops_copy_location)
            item.show()
            self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Copy Name'))
        item.connect('activate', self._ops_copy_name)
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Copy Name (No Suffix)'))
        item.connect('activate', self._ops_copy_name_nosuffix)
        item.show()
        self.append(item)

        item = Gtk.SeparatorMenuItem()
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Rename'))
        item.connect('activate', self._show_rename_dialog)
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Delete'))
        item.connect('activate', self._show_remove_dialog)
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Copy to') + '....')
        item.connect('activate', self._copy_to_bookmark_dialog)
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Move to') + '....')
        item.connect('activate', self._move_to_bookmark_dialog)
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Property'))
        item.connect('activate', self._show_property_dialog)
        item.show()
        self.append(item)

        return

    def show(self):
        self.show_all()
        return

    @property
    def x(self):
        if not hasattr(self, '_x'):
            self._x = -1
        return self._x

    @x.setter
    def x(self, _x):
        self._x = _x
        return

    @property
    def y(self):
        if not hasattr(self, '_y'):
            self._y = -1
        return self._y

    @y.setter
    def y(self, _y):
        self._y = _y
        return

    @property
    def device(self):
        if not hasattr(self, '_device'):
            self._device = None
        return self._device

    @device.setter
    def device(self, dev):
        self._device = dev
        return

    def popup(self, x, y, device=None):
        self.x = x
        self.y = y
        self.device = device
        super().popup(
            None,
            None,

            # position func, e.g. None,
            lambda p1, p2, p3, p4: (self.x, self.y, False),

            None,

            # ev.button,
            1,
            # ev.time
            Gtk.get_current_event_time()
        )
        return

    def _show_remove_dialog(self, *args):
        DialogRemove() \
            .set_mainview(self._mainview) \
            .set_container(self._container) \
            .set_filelistitem(self._filelistitem) \
            .set_pathitem(self._pathitem).show()
        return True

    def _show_rename_dialog(self, *args):
        DialogRename() \
            .set_mainview(self._mainview) \
            .set_pathitem(self._pathitem) \
            .set_filelistitem(self._filelistitem).show()
        return True

    def _copy_to_bookmark_dialog(self, *args):
        MenuListBookmark() \
            .set_mainview(self._mainview) \
            .set_pathitem(self._pathitem) \
            .set_op(BookmarkOp.OP_COPY) \
            .popup(self.x, self.y, device=self.device).show()
        return True

    def _move_to_bookmark_dialog(self, *args):
        MenuListBookmark() \
            .set_mainview(self._mainview) \
            .set_pathitem(self._pathitem) \
            .set_op(BookmarkOp.OP_MOVE) \
            .popup(self.x, self.y, device=self.device).show()
        return True

    def _show_property_dialog(self, *args):
        DialogProperty() \
            .set_mainview(self._mainview) \
            .set_pathitem(self._pathitem).show()
        return True

    def _show_quickview_dialog(self, *args):
        win = QuickView() \
            .set_pathitem(self._pathitem) \
            .set_mainview(self._mainview) \
            .show()
        win.is_can_modify = False
        return True

    def _ops_copy_location(self, *args):
        clipboard = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(str(self._pathitem), -1)
        return True

    def _ops_copy_name(self, *args):
        clipboard = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(self._pathitem.name, -1)
        return True

    def _ops_copy_name_nosuffix(self, *args):
        clipboard = self.get_clipboard(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(self._pathitem.with_suffix('').name, -1)
        return True
