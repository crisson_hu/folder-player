from gi.repository import Gtk

from base import IconBase


class MenuImageItem(Gtk.MenuItem):

    __gtype_name__ = 'MenuImageItem'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hbox = Gtk.HBox()
        self.add(self.hbox)
        return

    @classmethod
    def new_with_label(cls, label, icon=None):
        nlabel = Gtk.Label()
        nlabel.set_markup(label)
        self = cls()
        self._set_icon(icon)
        self.hbox.pack_start(nlabel, False, False, 0)
        return self

    def _set_icon(self, icon):
        if icon is not None:
            image = IconBase().set_icon(icon)
            self.hbox.pack_start(image, False, False, 0)
        return self


class MenuImageItemBookmark(Gtk.MenuItem):

    __gtype_name__ = 'MenuImageItemBookmark'

    BM = 1
    BM_SUBDIR = 2
    BM_NONE = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def new_with_label(self, label, bm):
        hbox = Gtk.HBox()
        self.add(hbox)

        icon = IconBase().set_icon(
            'bookmark.png' if bm == self.BM else
            'bookmark-subdir.png' if bm == self.BM_SUBDIR else
            'bookmark-none.png'
        )
        hbox.pack_start(icon, False, False, 0)
        icon.set_property('margin', 0)
        icon.set_property('margin-right', 10)

        self.nlabel = Gtk.Label()
        self.nlabel.set_markup(label)
        hbox.pack_start(self.nlabel, False, False, 0)
        return self

    def set_markup(self, mup):
        self.nlabel.set_markup(mup)
        return

    def show(self):
        super().show_all()
        return
