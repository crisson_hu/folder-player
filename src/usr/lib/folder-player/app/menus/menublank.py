from gi.repository import Gdk, Gtk

from tool import _, log_debug
from .menuimageitem import MenuImageItem
from base import MenuBase


class MenuBlank(MenuBase):

    __gtype_name__ = 'MenuBlank'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def _clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self._clear()

        item = Gtk.MenuItem.new_with_label(_('Copy Location'))
        item.connect('activate', self._mainblank.ops_copy_location)
        item.show()
        self.append(item)

        item = Gtk.SeparatorMenuItem()
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Refresh'))
        item.connect('activate', self._mainblank.ops_refresh)
        item.show()
        self.append(item)

        return

    def set_mainblank(self, mainblank=None):
        self._mainblank = mainblank
        return self

    def popup(self, x, y):
        self._setup()
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (x, y, False),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return
