from gi.repository import Gdk, Gtk

from tool import _, get_mount_points
from dbuslite import store_lite
from base import MenuBase


class MenuMainBase(MenuBase):

    __gtype_name__ = 'MenuMainBase'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def setup_signal(self, handler=None):
        self._handler = handler
        self._setup()
        return self


class MenuMainRight(MenuMainBase):

    __gtype_name__ = 'MenuMainRight'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def popup(self, x, y):
        self._setup()
        if len(self.get_children()) == 0:
            return
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (x, y, False),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return

    def clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self.clear()

        mi = Gtk.MenuItem.new_with_label(_('More Operations'))
        mi.set_submenu(self._submenu_ops())
        mi.show()
        self.append(mi)

        mi = Gtk.SeparatorMenuItem()
        mi.show()
        self.append(mi)

        mpt = get_mount_points()
        if mpt:
            for i in mpt:
                mi = Gtk.MenuItem(i)
                mi.connect('activate', self._goto_directory, i)
                mi.show()
                self.add(mi)
            mi = Gtk.SeparatorMenuItem()
            mi.show()
            self.append(mi)

        if len(store_lite.bookmark) > 0:
            store_lite.bookmark.sort()
            for i in store_lite.bookmark:
                mi = Gtk.MenuItem(i + '/')
                mi.connect('activate', self._goto_directory, i)
                mi.show()
                self.add(mi)
        return

    def _goto_directory(self, widget, directory):
        self._mainview.list_directory(directory=directory)
        return True

    def _quit(self, *args):
        self._mainview.mainapp.destroy()

    def _submenu_ops(self):
        menu = Gtk.Menu()

        mi = Gtk.MenuItem.new_with_label(_('Quit'))
        mi.connect('activate', self._quit)
        mi.show()
        menu.append(mi)

        return menu


class MenuMainLeft(MenuMainBase):

    __gtype_name__ = 'MenuMainLeft'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        return

    def _setup(self):
        item = Gtk.MenuItem.new_with_label(_('Main Menu'))
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Apps'))
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(_('Folder and Files'))
        item.show()
        self.append(item)

        return

    def popup(self, x, y):
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (x, y, False),

            None,

            # ev.button,
            Gdk.BUTTON_PRIMARY,

            # ev.time
            Gtk.get_current_event_time()
        )
        return

    def _show_dialog(self, *args):
        return True
