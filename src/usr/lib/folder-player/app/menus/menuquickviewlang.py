from gi.repository import Gdk, Gtk

from tool import _, log_debug
from base import MenuBase
from app.quickview.textwidget import TextLanguage


class MenuQuickViewLang(MenuBase):

    __gtype_name__ = 'MenuQuickViewLang'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.button = None
        return

    def _clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self._clear()

        for (lang_id, lang_name) in self.button._tv.get_language_list():
            txt = '{}  [ {} ]'.format(lang_name, lang_id)
            item = Gtk.MenuItem.new_with_label(txt)
            item.connect('activate', self._activate_mi, lang_id)
            item.show_all()
            self.append(item)

        return

    def _activate_mi(self, widget, lang_id):
        log_debug(lang_id)
        self.button.update_lang(lang_id)
        return True

    def set_button(self, btn):
        self.button = btn
        return self

    def popup(self, x, y):
        self._setup()
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (
                x,
                y,
                False
            ),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return
