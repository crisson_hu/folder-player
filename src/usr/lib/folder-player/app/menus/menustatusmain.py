from gi.repository import Gdk, Gtk

from dbuslite import store_lite
from tool import _, log_debug
from .menuimageitem import MenuImageItem
from base import MenuBase
from app.constants import Constants
from app.dialogs import DialogAbacus, ABACUS_TITLE


class MenuStatusMain(MenuBase):

    __gtype_name__ = 'MenuStatusMain'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._store = store_lite
        return

    def _clear(self):
        for c in self.get_children():
            self.remove(c)
        return True

    def _setup(self):
        self._clear()

        item = Gtk.MenuItem.new_with_label(_('Dictionary'))
        item.connect('activate', self._show_dict)
        item.show()
        self.append(item)

        item = Gtk.SeparatorMenuItem()
        item.show()
        self.append(item)

        item = Gtk.MenuItem.new_with_label(ABACUS_TITLE)
        item.connect('activate', self._show_abacus)
        item.show()
        self.append(item)

        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        return self

    def _show_dict(self, *args):
        self._mainview.mainwin_ui_switch_view(Constants.UiView.RUN_DICT)
        return True

    def _show_abacus(self, *args):
        DialogAbacus().set_mainview(self._mainview).show()
        return True

    def popup(self, x, y):
        self._setup()
        super().popup(
            None,
            None,

            # position func
            # None,
            lambda p1, p2, p3, p4: (
                x,
                y - self.get_allocated_height(),
                False
            ),

            None,

            # ev.button,
            Gdk.BUTTON_SECONDARY,
            # ev.time
            Gtk.get_current_event_time()
        )
        return
