import json
import os
from pathlib import Path
from gi.repository import GLib, Gst

from tool import APP, log_debug, get_fmt_day_hour_minute_second
from .components import (
    Address,
    Window,
    Bookmark,
    Player,
    QuickView,
)


class Store:
    '''Data store for application settings, can be stored on disk as json
    file, and retrieved from disk.'''

    def __init__(self):
        self._all = {}
        self._extras = {}
        self._setup()
        return

    def __repr__(self):
        return str(self._all)

    def _setup(self):
        self.config_dir = str(Path(GLib.get_user_config_dir()) / APP)
        try:
            Path(self.config_dir).mkdir(parents=True)
        except FileExistsError:
            pass
        self.config_file = str(
            Path(self.config_dir) / APP
        )
        self._reload()
        self.save()
        return

    def _reload(self):
        if not Path(self.config_file).is_file():
            return
        with open(self.config_file) as fp:
            try:
                self._all = json.load(fp)
                # log_debug(self._all)
            except json.decoder.JSONDecodeError as e:
                log_debug('[Exception]: JSONDecodeError, ' + str(e))

        return

    def save(self):
        self._query_window_params()
        with open(self.config_file, mode='w') as fp:
            try:
                json.dump(self._all, fp, indent=4)
            except Exception as e:
                log_debug('Exception: ' + str(e))
                log_debug(self._all)
        return

    def set_window(self, win=None):
        self._win = win
        return

    def _home_or_appdir(self):
        import os
        d = os.getenv('HOME')
        if not d:
            d = str(Path(__file__).parent.parent)
        return d

    @property
    def last_dir(self):
        d = self._all.get(Address.LAST_DIR)
        if d is None:
            d = self._home_or_appdir()
            log_debug('initial value: {}'.format(d))
        elif d == '.':
            d = str(Path(d).resolve())
        else:
            d = Path(d)
            # Detect if d is a valid directory.
            while True:
                try:
                    Path(d).resolve()
                    break
                except FileNotFoundError as e:
                    d = d.parent
                    continue

        self._all[Address.LAST_DIR] = str(d)

        return str(d)

    @last_dir.setter
    def last_dir(self, lastdir):
        try:
            self._all[Address.LAST_DIR] = str(Path(lastdir))
        except FileNotFoundError:
            self._all[Address.LAST_DIR] = str(Path(lastdir).parent.resolve())
            log_debug('Path().resolve(): for {}'.format(Path(lastdir).parent))
        except FileNotFoundError:
            self._all[Address.LAST_DIR] = self._home_or_appdir()
            log_debug('initial value[1]: {}'.format(Path('.').resolve()))
        if not self.last_longest_dir.startswith(
                self.last_dir if self.last_dir.endswith(os.sep) else
                self.last_dir + os.sep
        ):
            self._all[Address.LAST_LONGEST_DIR] = self.last_dir
        self.save()
        return

    last_dir.__doc__ = (
        'last_dir is the last displaying directory since quit, and is the '
        'current displaying directory.'
    )

    @property
    def last_longest_dir(self):
        if not self._all.get(Address.LAST_LONGEST_DIR):
            self._all[Address.LAST_LONGEST_DIR] = self.last_dir

        return self._all[Address.LAST_LONGEST_DIR]

    @property
    def window_x(self):
        return self._all.get(Window.LOCATION_X)

    @property
    def window_y(self):
        return self._all.get(Window.LOCATION_Y)

    @property
    def window_width(self):
        return self._all.get(Window.WIDTH)

    @property
    def window_height(self):
        return self._all.get(Window.HEIGHT)

    @property
    def is_maximized(self):
        return self._all.get(Window.IS_MAXIMIZED)

    def _query_window_params(self):
        if not hasattr(self, '_win') or self._win is None:
            return

        if self._win.is_maximized():
            self._all[Window.IS_MAXIMIZED] = True
            # Don't override the former window geometry/location.
            return

        self._all[Window.IS_MAXIMIZED] = False

        (
            self._all[Window.LOCATION_X],
            self._all[Window.LOCATION_Y],
        ) = self._win.get_position()
        (
            self._all[Window.WIDTH],
            self._all[Window.HEIGHT],
        ) = self._win.get_size()
        return

    @property
    def bookmark(self):
        if self._all.get(Bookmark.BOOKMARK) is None:
            self._all[Bookmark.BOOKMARK] = Bookmark()
        return self._all[Bookmark.BOOKMARK]

    @property
    def player(self):
        if (
                self._all.get(Player.PLAYER) is None or
                len(self._all.get(Player.PLAYER)) == 0
        ):
            self._all[Player.PLAYER] = Player()

        return self._all[Player.PLAYER]

    @property
    def player_repeat_mode(self):
        return self.player[Player.REPEAT]

    @player_repeat_mode.setter
    def player_repeat_mode(self, repeat):
        if repeat not in (
                Player.REPEAT_NONE,
                Player.REPEAT_CURRENT,
                Player.REPEAT_SELECTED,
                Player.REPEAT_ALL,
        ):
            raise ValueError('Invalid mode' + str(repeat))
        self.player[Player.REPEAT] = repeat
        return

    @property
    def player_stream_total_length(self):
        return self.player[Player.STREAM_TOTAL_LENGTH]

    @player_stream_total_length.setter
    def player_stream_total_length(self, length):
        self.player[Player.STREAM_TOTAL_LENGTH] = length
        return

    @property
    def player_stream_total_length_second(self):
        return int(self.player[Player.STREAM_TOTAL_LENGTH] / Gst.SECOND)

    @property
    def player_stream_total_length_second_fmt(self):
        return get_fmt_day_hour_minute_second(
            self.player_stream_total_length_second
        )

    @property
    def player_stream_current_position(self):
        return self.player[Player.STREAM_CURRENT_POSITION]

    @player_stream_current_position.setter
    def player_stream_current_position(self, position):
        self.player[Player.STREAM_CURRENT_POSITION] = position
        return

    @property
    def player_stream_current_position_second(self):
        return int(self.player[Player.STREAM_CURRENT_POSITION] / Gst.SECOND)

    @property
    def player_stream_current_position_second_fmt(self):
        return get_fmt_day_hour_minute_second(
            self.player_stream_current_position_second
        )

    @property
    def player_stream_progress_label(self):
        return '{} / {}'.format(
            self.player_stream_current_position_second_fmt,
            self.player_stream_total_length_second_fmt
        )

    @property
    def quickview(self):
        if (
                self._all.get(QuickView.QUICKVIEW) is None or
                len(self._all.get(QuickView.QUICKVIEW)) == 0
        ):
            self._all[QuickView.QUICKVIEW] = QuickView()

        return self._all[QuickView.QUICKVIEW]

    @property
    def quick_view_fit_mode(self):
        return self.quickview[QuickView.FIT]

    @quick_view_fit_mode.setter
    def quick_view_fit_mode(self, fit):
        if fit not in (
                QuickView.FIT_NONE,
                QuickView.FIT_WIDTH,
                QuickView.FIT_HEIGHT,
                QuickView.FIT_PAGE,
        ):
            raise ValueError('Invalid mode' + str(fit))
        self.quickview[QuickView.FIT] = fit
        return

    @property
    def quick_view_layout_mode(self):
        return self.quickview[QuickView.LAYOUT]

    @quick_view_layout_mode.setter
    def quick_view_layout_mode(self, layout):
        if layout not in (
                QuickView.LAYOUT_ONECOLUMN,
                QuickView.LAYOUT_DUALCOLUMN,
                QuickView.LAYOUT_SINGLEPAGE,
                QuickView.LAYOUT_LEFT2RIGHT,
                QuickView.LAYOUT_RIGHT2LEFT,
        ):
            raise ValueError('Invalid mode' + str(layout))
        self.quickview[QuickView.LAYOUT] = layout
        return
