class Address:

    LAST_DIR = 'last-dir'
    LAST_LONGEST_DIR = 'last-longest-dir'


class Window:

    LOCATION_X = 'window-location-x'
    LOCATION_Y = 'window-location-y'
    WIDTH = 'window-width'
    HEIGHT = 'window-height'
    IS_MAXIMIZED = 'is-maximized'


class Bookmark(list):

    BOOKMARK = 'bookmark'


class Player(dict):

    PLAYER = 'player'

    REPEAT = 'repeat'
    # constants
    REPEAT_NONE = 'Repeat None'
    REPEAT_CURRENT = 'Repeat Current'
    REPEAT_SELECTED = 'Repeat Selected'
    REPEAT_ALL = 'Repeat All'

    STREAM_TOTAL_LENGTH = 'stream-total-length'
    STREAM_CURRENT_POSITION = 'stream-current-position'

    def __init__(self):
        self[self.REPEAT] = self.REPEAT_NONE
        self[self.STREAM_TOTAL_LENGTH] = -1
        self[self.STREAM_CURRENT_POSITION] = -1
        return


class QuickView(dict):

    QUICKVIEW = 'quickview'

    FIT = 'fit'
    # constants
    FIT_NONE = 'Fit None'
    FIT_WIDTH = 'Fit Width'
    FIT_HEIGHT = 'Fit Height'
    FIT_PAGE = 'Fit Page'

    LAYOUT = 'layout'
    LAYOUT_ONECOLUMN = 'Layout One Column'
    LAYOUT_DUALCOLUMN = 'Layout Dual Column'
    LAYOUT_SINGLEPAGE = 'Layout Single Page'
    LAYOUT_LEFT2RIGHT = 'Layout Left to Right'
    LAYOUT_RIGHT2LEFT = 'Layout Right to Left'

    def __init__(self):
        self[self.FIT] = self.FIT_NONE
        self[self.LAYOUT] = self.LAYOUT_ONECOLUMN
        return
