from .framework import Store
from .components import Player

store_lite = Store()

__all__ = [
    'store_lite',
    'Player',
]
