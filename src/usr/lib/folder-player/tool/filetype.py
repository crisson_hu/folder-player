from pathlib import Path


class FileType:

    FILE_TYPE = {
        'regular': '',
        'directory': '/',
        'symlink': '@',
        'FIFO': '|',
        'socket': '=',
        'door': '>',
        'executable': '*',
    }

    @classmethod
    def get_file_type_symbol(self, f):
        if f.is_symlink():
            # Note: this test should go before many other functions.
            return self.FILE_TYPE['symlink']
        if f.is_dir():
            return self.FILE_TYPE['directory']
        if f.is_fifo():
            return self.FILE_TYPE['FIFO']
        if f.is_file():
            return self.FILE_TYPE['regular']
        if f.is_socket():
            return self.FILE_TYPE['socket']
        return ''

    @classmethod
    def is_dir(self, f):
        return f.is_dir()
