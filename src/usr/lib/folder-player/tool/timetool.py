def get_fmt_day_hour_minute_second(time):
    duration = time
    seconds = duration % 60
    minutes = int(duration / 60) % 60
    hours = int(duration / 60 / 60) % 24
    days = int(duration / 60 / 60 / 24)
    text = '{}{}{}{}'.format(
        '{}d'.format(days) if days > 0 else '',
        '{:02d}:'.format(hours) if duration >= 3600 else '',
        '{:02d}:'.format(minutes) if duration >= 60 else '',
        '{:02d}'.format(seconds) if duration >= 10 else '{}'.format(seconds),
    )
    return text
