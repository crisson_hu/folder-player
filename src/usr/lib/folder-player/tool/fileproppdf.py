from gi.repository import Poppler

from . import FileProp


class FilePropPdf:

    def __init__(self, doc):
        # Note:
        # example code
        # doc = Poppler.Document.new_from_file('file:///dir/file.pdf', None)
        self._doc = doc
        self.update_prop()
        return

    def update_prop(self):
        self.n_pages = self._doc.get_n_pages()
        self.title = self._doc.get_title()
        self.author = self._doc.get_author()
        self.subject = self._doc.get_subject()
        self.keywords = self._doc.get_keywords()
        self.pdf_version_string = self._doc.get_pdf_version_string()
        self.producer = self._doc.get_producer()
        self.creator = self._doc.get_creator()
        self.creation_date = self._doc.get_creation_date()
        self.modification_date = self._doc.get_modification_date()
        self.is_linearized = self._doc.is_linearized()
        self.page_mode = self._doc.get_page_mode()
        self.permissions = self._doc.get_permissions()
        self.page_layout = self._doc.get_page_layout()
        self.n_attachments = self._doc.get_n_attachments()
        self.metadata = self._doc.get_metadata()

        try:
            self.id = self._doc.get_id()
        except UnicodeDecodeError:
            self.id = None
        self.pdf_version = self._doc.get_pdf_version()
        self.has_attachments = self._doc.has_attachments()
        self.attachments = self._doc.get_attachments()

        # update stat string
        self.stat_info = self.get_stat_info()
        return

    def get_stat_info(self):
        return (
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}\n'
            '{:25s}{}'
        ).format(
            'Pages(#):', self.n_pages,
            'Title:', self.title,
            'Author:', self.author,
            'Subject:', self.subject,
            'Keywords:', self.keywords,
            'Version:', self.pdf_version_string,
            'Producer:', self.producer,
            'Creator:', self.creator,
            'Creation Date:', FileProp._get_st_time(self.creation_date),
            'Modification Date:', FileProp._get_st_time(
                self.modification_date
            ),
            'Linearized:', self.is_linearized,
            'Page Mode:', FilePropPdf.get_page_mode_string(self.page_mode),
            'Permissions:', FilePropPdf.get_permissions_string(
                self.permissions
            ),
            'Page Layout:', FilePropPdf.get_page_layout_string(
                self.page_layout
            ),
            'Attachments(#):', self.n_attachments,
            'Metadata:', self.metadata,
        )

    def get_ts(time_t=-1):
        ''' get time string '''
        import time
        s = 'N/A'
        if time_t != -1:
            s = time.ctime(time_t)
        return s

    def get_page_mode_string(mode):
        s = 'Not found'
        if mode == Poppler.PageMode.UNSET:
            s = 'no specific mode set'
        elif mode == Poppler.PageMode.NONE:
            s = 'neither document outline nor thumbnails visible'
        elif mode == Poppler.PageMode.USE_OUTLINES:
            s = 'document outline visible'
        elif mode == Poppler.PageMode.USE_THUMBS:
            s = 'thumbnails visible'
        elif mode == Poppler.PageMode.FULL_SCREEN:
            s = 'full-screen mode'
        elif mode == Poppler.PageMode.USE_OC:
            s = 'layers panel visible'
        elif mode == Poppler.PageMode.USE_ATTACHMENTS:
            s = 'attachments panel visible'
        return s

    def get_permissions_string(perm):
        s = 'Not found'
        if perm == Poppler.Permissions.OK_TO_PRINT:
            s = 'document can be printer'
        elif perm == Poppler.Permissions.OK_TO_MODIFY:
            s = 'document contents can be modified'
        elif perm == Poppler.Permissions.OK_TO_COPY:
            s = 'document can be copied'
        elif perm == Poppler.Permissions.OK_TO_ADD_NOTES:
            s = 'annotations can added to the document'
        elif perm == Poppler.Permissions.OK_TO_FILL_FORM:
            s = 'interactive form fields can be filled in'
        elif perm == Poppler.Permissions.OK_TO_EXTRACT_CONTENTS:
            s = (
                'extract text and graphics (in support of'
                ' accessibility to users with disabilities or'
                'for other purposes)'
            )
        elif perm == Poppler.Permissions.OK_TO_ASSEMBLE:
            s = (
                'assemble the document (insert, rotate,'
                'or delete pages and create bookmarks'
                'or thumbnail images)'
            )
        elif perm == Poppler.Permissions.OK_TO_PRINT_HIGH_RESOLUTION:
            s = 'document can be printer at high resolution'
        elif perm == Poppler.Permissions.FULL:
            s = 'document permits all operations'
        return s

    def get_page_layout_string(layout):
        s = 'Not found'
        if layout == Poppler.PageLayout.UNSET:
            s = 'no specific layout set'
        elif layout == Poppler.PageLayout.SINGLE_PAGE:
            s = 'one page at a time'
        elif layout == Poppler.PageLayout.ONE_COLUMN:
            s = 'pages in one column'
        elif layout == Poppler.PageLayout.TWO_COLUMN_LEFT:
            s = 'pages in two columns with odd numbered pages on the left'
        elif layout == Poppler.PageLayout.TWO_COLUMN_RIGHT:
            s = 'pages in two columns with odd numbered pages on the right'
        elif layout == Poppler.PageLayout.TWO_PAGE_LEFT:
            s = 'two pages at a time with odd numbered pages on the left'
        elif layout == Poppler.PageLayout.TWO_PAGE_RIGHT:
            s = 'two pages at a time with odd numbered pages on the right'
        return s
