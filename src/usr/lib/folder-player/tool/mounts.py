
def get_mount_points():
    result = []
    with open('/proc/mounts') as fd:
        for line in fd.readlines():
            if line.find('media') > 0:
                result.append(line.split()[1])

    return result
