import threading
from . import log_debug


def list_threads():
    log_debug(threading.enumerate())
