import gi
from gi.repository import Gst, GstPbutils, GLib


class FilePropMedia:

    def __init__(self):
        self.uri = None
        self.uri_cb = None
        self.disc_info_tags_list = []
        self.stream_info_list = []
        self._setup()
        return

    def set_uri(self, uri):
        self.uri = uri
        return self

    def _log(self, *args):
        return
        import pprint
        pprint.pprint(args)
        return

    def _setup(self):
        self._log('GstPbutils.Discoverer():')
        self.discoverer = GstPbutils.Discoverer()
        self.discoverer.connect('discovered', self._on_discovered_cb)
        self.discoverer.connect('finished', self._on_finished_cb)
        return self

    def _on_discovered_cb(self, gdisc, gdiscinfo, e):
        self._log('_on_discovered_cb():')
        uri = gdiscinfo.get_uri()
        result = gdiscinfo.get_result()
        self._log('result: {}'.format(result.value_name))
        if result == GstPbutils.DiscovererResult.BUSY:
            pass
        elif result == GstPbutils.DiscovererResult.ERROR:
            self._log(e)
            self._log('e.code: {}, e.domain: {}, e.message: {}'.format(
                e.code, e.domain, e.message,
            ))
        elif result == GstPbutils.DiscovererResult.MISSING_PLUGINS:
            self._log('Missing Plugins: {}'.format(gdiscinfo.get_misc()))
        elif result == GstPbutils.DiscovererResult.OK:
            self._log('Tags:')
            tags = gdiscinfo.get_tags()
            if tags:
                Gst.TagList.foreach(
                    tags,
                    self._get_tag_foreach,
                    self.disc_info_tags_list,
                )
                self._log(self.disc_info_tags_list)

            self._log('Seekable: {}'.format(
                'yes' if gdiscinfo.get_seekable() else 'no'
            ))

            sinfo = gdiscinfo.get_stream_info()
            self._get_stream_info_recursive(sinfo, self.stream_info_list)
            self._log('Stream Information:')
            self._log(self.stream_info_list)

            if self.uri_cb:
                self.uri_cb()
        elif result == GstPbutils.DiscovererResult.TIMEOUT:
            pass
        elif result == GstPbutils.DiscovererResult.URI_INVALID:
            pass

        return True

    def _get_tag_foreach(self, taglist, tagname, input_list):
        ret, dest = taglist.copy_value(taglist, tagname)
        if ret:
            if isinstance(dest, (str, int)):
                input_list.append((tagname, dest))
            elif isinstance(dest, Gst.DateTime):
                input_list.append((tagname, dest.to_iso8601_string()))
            elif isinstance(dest, Gst.Sample):
                caps = dest.get_caps()
                if caps:
                    if caps.is_fixed():
                        desc = GstPbutils.pb_utils_get_codec_description(caps)
                    else:
                        desc = caps.to_string()
                    input_list.append((tagname + '(Sample)', desc))
                gst_structure = dest.get_info()
                if gst_structure:
                    input_list.append((
                        tagname + '(Sample)',
                        gst_structure.to_string()
                    ))
            else:
                input_list.append((tagname, dest))
        return

    def _get_stream_info_recursive(self, sinfo, input_list):
        caps = sinfo.get_caps()
        if caps:
            if caps.is_fixed():
                desc = GstPbutils.pb_utils_get_codec_description(caps)
            else:
                desc = caps.to_string()
            input_list.append(desc)

        tags = sinfo.get_tags()
        if tags:
            Gst.TagList.foreach(
                tags,
                self._get_tag_foreach,
                input_list,
            )

        sinfo_next = sinfo.get_next()
        if sinfo_next:
            a = []
            self._get_stream_info_recursive(sinfo_next, a)
            input_list.append(a)
        elif isinstance(sinfo, GstPbutils.DiscovererContainerInfo):
            streams = sinfo.get_streams()
            tmp = streams
            for tmp in streams:
                a = []
                self._get_stream_info_recursive(tmp, a)
                input_list.append(a)
        return

    def _on_finished_cb(self, gdisc):
        self.do_discovery_stop()
        return True

    def do_discovery_stop(self):
        self._log('GstPbutils.Discoverer().stop():')
        self.discoverer.stop()
        return True

    def do_discovery(self, done_cb=None):
        self.uri_cb = done_cb
        self.disc_info_tags_list.clear()
        self.stream_info_list.clear()

        self._log('GstPbutils.Discoverer().start():')
        self.discoverer.start()
        self._log('Discovering URI: "{}"'.format(self.uri))
        success = self.discoverer.discover_uri_async(self.uri)
        if not success:
            return self
        return self

    def get_disc_info_tags_str(self):
        s = []
        for i in self.disc_info_tags_list:
            s.append('{:25s}{}'.format(i[0] + ':', i[1]))
        return '\n'.join(s)

    def get_stream_info_str(self):
        s = []
        for i in self.stream_info_list:
            if isinstance(i, str):
                s.append(i)
            elif isinstance(i, (list, tuple)):
                for ii in i:
                    if isinstance(ii, str):
                        s.append(ii)
                    elif isinstance(ii, (list, tuple)):
                        s.append('{:25s}{}'.format(ii[0] + ':', ii[1]))
        return '\n'.join(s)
