import os
from gi.repository import GLib


class FileProp:

    @staticmethod
    def get_stat(pi):
        if not pi:
            return ''
        s = pi.name
        s += '\n\n'
        s += str(pi.parent) + os.path.sep
        s += '\n\n'
        r = pi.stat()
        st_times = ('st_atime', 'st_ctime', 'st_mtime')
        for i in dir(r):
            if i.startswith('st_mode'):
                s = '{}\n{:25s}{:06o}'.format(s, i + ':', getattr(r, i))
            elif i.startswith('st_size'):
                s = '{}\n{:25s}{}'.format(
                    s, i + ':', FileProp._get_st_size(getattr(r, i))
                )
            elif any(i.startswith(a) for a in st_times):
                # Filter out the *_ns items.
                if i not in st_times:
                    continue
                s = '{}\n{:25s}{}'.format(
                    s, i + ':', FileProp._get_st_time(getattr(r, i))
                )
            elif i.startswith('st_'):
                s = '{}\n{:25s}{}'.format(s, i + ':', getattr(r, i))
        return s

    def _get_st_size(st_size):
        r = str(st_size)
        prefix = 3 if len(r) % 3 == 0 else len(r) % 3
        comma = int((len(r) - prefix) / 3)
        suffix = 1 if comma > 0 else 0

        lr = list(r)
        for i in range(comma):
            lr.insert(len(r) - (i + 1) * 3, ',')

        return (
            '{} Byte{}'.format(
                r, '' if r == '0' else 's'
            ) if comma == 0 else
            '{} {} ({} Bytes)'.format(
                int(st_size / (1024 ** comma)),
                'KiB' if comma == 1 else
                'MiB' if comma == 2 else
                'GiB' if comma == 3 else
                'TiB' if comma == 4 else
                'PiB' if comma == 5 else
                '?',
                ''.join(i for i in lr)
            )
        )

    def _get_st_time(st_time):
        '''Use GLib functions'''
        dt = GLib.DateTime.new_from_unix_local(st_time)
        fmt = '%Y/%m/%d %H:%M:%S'
        return dt.to_local().format(fmt)
