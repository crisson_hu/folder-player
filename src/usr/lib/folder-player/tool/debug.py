import logging
from datetime import datetime
import pprint
import traceback
import inspect
import threading


class DebugLogger:

    DEBUG = logging.DEBUG

    def __init__(self):
        self.logger = logging.getLogger('hud.logger')
        self.logger.setLevel(logging.INFO)

        ch = logging.StreamHandler()
        ch.setLevel(self.DEBUG)

        # SWITCH
        self._use_formatter = False
        if self._use_formatter:
            ch.setFormatter(self.formatter)

        self.logger.addHandler(ch)
        return

    def set_level(self, level=logging.INFO):
        self.logger.setLevel(level)

    def log_log(self, level, msg=''):
        if self._use_formatter:
            self.logger.log(level, msg)
        else:
            self.logger.log(
                level,
                '{} {}'.format(
                    self.get_prefix(
                        str_time=True, thread_info=True, function_name=True
                    ),
                    msg
                )
            )

    def log_abnorm(self, msg=''):
        self.log_log(logging.WARNING, msg)

    def log_info(self, msg=''):
        self.log_log(logging.INFO, msg)

    def log_debug(self, msg=''):
        self.log_log(self.DEBUG, msg)

    @property
    def formatter(self):
        if not hasattr(self, '_formatter'):
            self._formatter = logging.Formatter(
                fmt='%(asctime)s<0x%(thread)x> %(message)s'
            )
            self._formatter.formatTime = (
                lambda _x1, _x2, _x3=None:
                datetime.now().strftime(DebugLogger.fmt_str)
            )
        return self._formatter

    @staticmethod
    def fmt_str():
        # format 1
        fmt_str_1 = '%y.%m.%d %H:%M:%S.%f'
        # format 2
        fmt_str_2 = '%H-%M-%S.%f'
        # SWITCH
        _fmt_str = fmt_str_2
        return _fmt_str

    @staticmethod
    def get_prefix(
            str_time=True, thread_info=False, function_name=False
    ):
        s_time = datetime.now().strftime(DebugLogger.fmt_str())
        s_thread = threading.current_thread().ident
        frameinfo = inspect.stack()[3]
        s_func = '{}#{}'.format(frameinfo.function, frameinfo.lineno)

        result = ''
        if str_time:
            result = '{}{:s}'.format(result, s_time)
        if thread_info:
            result = '{}<0x{:x}>'.format(result, s_thread)
        if function_name:
            result = '{}[{:<18s}]'.format(result, s_func)
        return result


logger = DebugLogger()
log_debug = logger.log_debug
log_info = logger.log_info
log_abnorm = logger.log_abnorm


def _print_prefix():
    print(
        '{} '.format(
            DebugLogger.get_prefix(
                str_time=True, thread_info=True, function_name=True
            )
        ),
        end=''
    )
    return


def todo():
    i = 0
    for ln in traceback.format_stack()[:-2]:
        _print_prefix()
        print(' TODO: {}-> {}'.format(
            4 * i * ' ', ln.split('\n')[1].strip('\r\n')
        ))
        i += 1
    return


printstack = todo
