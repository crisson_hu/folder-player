import signal

from .debug import log_debug, log_info
from gi.repository import GLib


def reg_signal(quit_handler=None):

    def signal_action(sig):
        log_info('Caught signal <{}:{}>'.format(sig.name, sig.value))

        quit = True if sig in (1, 2, 9, 15, 19) else False

        if quit and quit_handler:
            log_debug('calling quit_handler....')
            quit_handler()

    def idle_handler(*args):
        log_debug('idle handler activated.')
        GLib.idle_add(
            signal_action, args[0], priority=GLib.PRIORITY_HIGH
        )

    def glib_handler(*args):
        log_debug('Activated.')
        GLib.idle_add(
            signal_action, args[0], priority=GLib.PRIORITY_HIGH
        )

    def glib_sig_reg(sig, priority=GLib.PRIORITY_HIGH):
        unix_signal_add = None

        # `unix_signal_add_full` was once renamed to `unix_signal_add`.
        if hasattr(GLib, 'unix_signal_add'):
            unix_signal_add = GLib.unix_signal_add
        elif hasattr(GLib, 'unix_signal_add_full'):
            unix_signal_add = GLib.unix_signal_add_full
        else:
            log_info('failed, GI too old.')

        if unix_signal_add:
            unix_signal_add(priority, sig, glib_handler, sig)

        #
        # Return `False` so that it's removed from event sources list
        # and won't be called again.
        #
        return False

    SIGS = filter(
        None, (
            getattr(signal, _s, None) for _s in (
                'SIGHUP', 'SIGINT', 'SIGKILL', 'SIGTERM', 'SIGSTOP',
            )
        )
    )
    good = []
    bad = []
    for sig in SIGS:
        try:
            # Python signal handler:
            signal.signal(sig, idle_handler)
        except OSError as e:
            bad.append(sig)
            continue
        else:
            good.append(sig)
            # GLib signal handler:
            GLib.idle_add(glib_sig_reg, sig)
    # log_debug('PASS:"{}".'.format(
    #     '/'.join(sig.name for sig in good)
    # ))
    # log_debug('FAIL:"{}".'.format(
    #     '/'.join(sig.name for sig in bad)
    # ))
