from pathlib import Path
import os
import locale
import gettext

_ = gettext.gettext
APP = 'folder-player'
APP_WMCLASS = 'Folder-Player'
APP_NAME = 'Folder Player'
APP_DIR = Path(__file__).parent.parent
LOCALE_DIR = APP_DIR / 'locale'
locale.setlocale(locale.LC_ALL, '')
locale.bindtextdomain(APP, str(LOCALE_DIR))
gettext.bindtextdomain(APP, str(LOCALE_DIR))
gettext.textdomain(APP)
