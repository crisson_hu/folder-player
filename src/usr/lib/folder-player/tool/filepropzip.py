class FilePropZip:

    @staticmethod
    def get_zip_info(url):
        import zipfile
        zf = zipfile.ZipFile(url)

        ret = []
        s = 'Archive:  {}'.format(url)
        ret.append(s)
        s = '{:^9}  {:^10} {:5}   {}'.format('Length', 'Date', 'Time', 'Name',)
        ret.append(s)
        s = '{}  {} {}   {}'.format('-' * 9, '-' * 10, '-' * 5, '-' * 4,)
        ret.append(s)

        total_size = 0
        total = 0
        for n in zf.infolist():
            dt = '{:04}-{:02}-{:02}'.format(
                n.date_time[0],
                n.date_time[1],
                n.date_time[2],
            )
            tm = '{:02}:{:02}'.format(
                n.date_time[3],
                n.date_time[4],
            )
            s = '{:>9}  {:10} {:5}   {:4}'.format(
                n.file_size, dt, tm, n.filename,
            )
            ret.append(s)
            total_size += n.file_size
            total += 1

        s = '{}{:21}{}'.format('-' * 9, '', '-' * 7,)
        ret.append(s)
        s = '{:>9}{:21s}{} files'.format(total_size, '', total,)
        ret.append(s)

        return '\n'.join(ret)
