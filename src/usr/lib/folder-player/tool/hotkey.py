from gi.repository import Gdk


class HotKey:

    KV_TAB = (
        Gdk.KEY_Tab,
        Gdk.KEY_KP_Tab,
    )
    KV_LEFT = (
        Gdk.KEY_Left,
        Gdk.KEY_KP_Left,
    )
    KV_RIGHT = (
        Gdk.KEY_Right,
        Gdk.KEY_KP_Right,
    )
    KV_UP = (
        Gdk.KEY_Up,
        Gdk.KEY_KP_Up,
    )
    KV_DOWN = (
        Gdk.KEY_Down,
        Gdk.KEY_KP_Down,
    )
    KV_PAGE_UP = (
        Gdk.KEY_Page_Up,
        Gdk.KEY_KP_Page_Up,
    )
    KV_PAGE_DOWN = (
        Gdk.KEY_Page_Down,
        Gdk.KEY_KP_Page_Down,
    )
    KV_ENTER = (
        Gdk.KEY_Return,
        Gdk.KEY_ISO_Enter,
        Gdk.KEY_KP_Enter,
    )
    KV_DELETE = (
        Gdk.KEY_KP_Delete,
        Gdk.KEY_Delete,
    )

    KV_SPACE = (
        Gdk.KEY_KP_Space,
        Gdk.KEY_space,
    )

    KV_BACKSPACE = (
        Gdk.KEY_BackSpace,
    )

    KV_HOME = (
        Gdk.KEY_Home,
        Gdk.KEY_KP_Home,
    )

    KV_END = (
        Gdk.KEY_End,
        Gdk.KEY_KP_End,
    )

    @classmethod
    def get_ctrl_alt_shift(cls, kv, ks):
        k_ctrl = False
        k_alt = False
        k_shift = False

        if ks & Gdk.ModifierType.CONTROL_MASK:
            k_ctrl = True
        if ks & Gdk.ModifierType.MOD1_MASK:
            k_alt = True
        if ks & Gdk.ModifierType.SHIFT_MASK:
            k_shift = True

        if kv == Gdk.KEY_Control_L or kv == Gdk.KEY_Control_R:
            k_ctrl = True
        if kv == Gdk.KEY_Alt_L or kv == Gdk.KEY_Alt_R:
            k_alt = True
        if (
                kv == Gdk.KEY_Shift_L or
                kv == Gdk.KEY_Shift_R or
                kv == Gdk.KEY_Shift_Lock
        ):
            k_shift = True

        return (k_ctrl, k_alt, k_shift)

    @classmethod
    def is_ctrl_plus_key(cls, kv, ks, keys=None):
        if not keys:
            return False
        if not cls.get_ctrl_alt_shift(kv, ks)[0]:
            return False
        if any(kv == k for k in keys):
            return True
        return False

    @classmethod
    def is_tab(cls, kv):
        if any(kv == v for v in cls.KV_TAB):
            return True
        return False

    @classmethod
    def is_left(cls, kv):
        if any(kv == v for v in cls.KV_LEFT):
            return True
        return False

    @classmethod
    def is_right(cls, kv):
        if any(kv == v for v in cls.KV_RIGHT):
            return True
        return False

    @classmethod
    def is_up(cls, kv):
        if any(kv == v for v in cls.KV_UP):
            return True
        return False

    @classmethod
    def is_down(cls, kv):
        if any(kv == v for v in cls.KV_DOWN):
            return True
        return False

    @classmethod
    def is_page_up(cls, kv):
        if any(kv == v for v in cls.KV_PAGE_UP):
            return True
        return False

    @classmethod
    def is_page_down(cls, kv):
        if any(kv == v for v in cls.KV_PAGE_DOWN):
            return True
        return False

    @classmethod
    def is_activate(cls, kv):
        if any(kv == v for v in cls.KV_ENTER):
            return True
        return False

    @classmethod
    def is_delete(cls, kv):
        if any(kv == v for v in cls.KV_DELETE):
            return True
        return False

    @classmethod
    def is_space(cls, kv):
        return kv in cls.KV_SPACE

    @classmethod
    def is_backspace(cls, kv):
        if any(kv == v for v in cls.KV_BACKSPACE):
            return True
        return False

    @classmethod
    def is_home(cls, kv):
        if any(kv == v for v in cls.KV_HOME):
            return True
        return False

    @classmethod
    def is_end(cls, kv):
        if any(kv == v for v in cls.KV_END):
            return True
        return False
