import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('GstPbutils', '1.0')
gi.require_version('GstVideo', '1.0')
gi.require_version('PangoCairo', '1.0')
gi.require_version('Poppler', '0.18')
gi.require_version('Vte', '2.91')

use_gtksource_4 = False
try:
    gi.require_version('GtkSource', '4')
    use_gtksource_4 = True
except ValueError:
    gi.require_version('GtkSource', '3.0')
except Exception as e:
    print(e)

require_version_imported = True
