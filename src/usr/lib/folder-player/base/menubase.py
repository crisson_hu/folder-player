from gi.repository import Gtk

from .widgetbase import WidgetBaseMixin


class MenuBase(Gtk.Menu, WidgetBaseMixin):

    __gtype_name__ = 'MenuBase'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        return
