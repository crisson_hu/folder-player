from pathlib import Path
from gi.repository import Gtk, Gdk, Pango

from tool import log_debug
from .widgetbase import WidgetBaseMixin


class IconBase(Gtk.EventBox, WidgetBaseMixin):

    __gtype_name__ = 'IconBase'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self.gesture_multipress = Gtk.GestureMultiPress.new(self)
        self.gesture_multipress.set_button(0)
        self.gesture_multipress.set_exclusive(True)
        self.set_property('expand', False)
        self.set_property('margin', 0)
        self.set_valign(Gtk.Align.CENTER)

        self.signals = {
            'pressed': self._on_press,
            'primary-single-click': self._on_press_primary_single,
            'primary-double-click': self._on_press_primary_double,
            'secondary-single-click': self._on_press_secondary_single,
            'secondary-double-click': self._on_press_secondary_double,
        }

        self._press_cb = {}

        return

    def set_icon(self, iconfile):
        p = str(
            Path(__file__).parent.parent /
            'app' / 'icons' / 'res' /
            iconfile
        )
        self.icon = Gtk.Image.new_from_file(p)
        # log_debug(p)
        self.add(self.icon)
        return self

    def set_visible(self, visible):
        super().set_visible(visible)
        self.icon.set_visible(visible)
        return

    def set_no_show_all(self, no_show_all):
        super().set_no_show_all(no_show_all)
        self.icon.set_no_show_all(no_show_all)
        return

    def get_no_show_all(self):
        return self.icon.get_no_show_all()

    def connect(self, sig_name, cb, *args):
        if sig_name in self.signals:
            self._press_cb[sig_name] = cb
            sig_id = self.gesture_multipress.connect(
                'pressed', self.signals[sig_name], *args
            )
        else:
            sig_id = super().connect(sig_name, cb, *args)

        return sig_id

    def _on_press(self, gesture, n_press, x, y):
        return self._press_cb['pressed'](gesture, n_press, x, y)

    def _on_press_primary_single(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 1:
            return self._press_cb['primary-single-click'](
                gesture, n_press, x, y
            )
        return True

    def _on_press_primary_double(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_PRIMARY and n_press == 2:
            return self._press_cb['primary-double-click'](
                gesture, n_press, x, y
            )
        return True

    def _on_press_secondary_single(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_SECONDARY and n_press == 1:
            return self._press_cb['secondary-single-click'](
                gesture, n_press, x, y
            )
        return True

    def _on_press_secondary_double(self, gesture, n_press, x, y):
        button = gesture.get_current_button()
        if button == Gdk.BUTTON_SECONDARY and n_press == 2:
            return self._press_cb['secondary-double-click'](
                gesture, n_press, x, y
            )
        return True

    def do_enter_notify_event(self, ev):
        context = self.get_style_context()
        context.set_state(Gtk.StateFlags(
            context.get_state() | Gtk.StateFlags.PRELIGHT
        ))
        return True

    def do_leave_notify_event(self, ev):
        context = self.get_style_context()
        context.set_state(Gtk.StateFlags(
            context.get_state() & (~Gtk.StateFlags.PRELIGHT)
        ))
        return True
