from gi.repository import Gtk

from tool import APP_WMCLASS
from .widgetbase import WidgetBaseMixin


class DialogBase(Gtk.Window, WidgetBaseMixin):

    __gtype_name__ = 'DialogBase'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        self._mainview = None
        self._mainwindow = None
        self._pathitem = None
        return

    def set_mainview(self, mainview):
        self._mainview = mainview
        self.set_mainwindow(mainview.mainwin if mainview else None)
        return self

    def set_mainwindow(self, mainwindow):
        '''If not calling set_mainview(), call set_mainwindow() instead.'''
        self._mainwindow = mainwindow
        self.set_transient_for(self._mainwindow)
        self.set_modal(True)
        self.set_destroy_with_parent(True)
        self.set_skip_pager_hint(True)
        self.set_skip_taskbar_hint(True)
        self.set_wmclass(APP_WMCLASS, APP_WMCLASS)
        return self

    def show(self):
        super().show_all()
        return self

    def do_destroy(self, *args):
        # FIXME
        # this function avoids following error:
        #     (folder-player:17240): GLib-GObject-WARNING **:
        #     /build/glib2.0-XBSKIw/glib2.0-2.48.2/./gobject/gsignal.c:2636:
        #     instance '0x347d790' has no handler with id '3872'
        return True
