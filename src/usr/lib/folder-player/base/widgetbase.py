from gi.repository import Gdk, Gtk

from tool import log_debug


class ButtonBaseMixin:

    def set_cursor_hand1(self):
        self.connect('realize', self._on_realize)

    def _on_realize(self, *args):
        w = self.get_window()
        if w:
            w.set_cursor(Gdk.Cursor(Gdk.CursorType.HAND1))
        return True


class WidgetBaseMixin:

    def style_context_add_class(self):
        self.get_style_context().add_class(self.__gtype_name__)
        if (
                hasattr(self, '__extra_style_classes__') and
                isinstance(self.__extra_style_classes__, list)
        ):
            for i in self.__extra_style_classes__:
                self.get_style_context().add_class(i)
        return
