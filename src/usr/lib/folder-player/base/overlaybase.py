from gi.repository import Gtk

from .widgetbase import WidgetBaseMixin


class DialogOverlayBackdrop(Gtk.VBox, WidgetBaseMixin):

    __gtype_name__ = 'DialogOverlayBackdrop'
    __extra_style_classes__ = ['DialogOverlayBackdrop']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        return


class DialogOverlay(Gtk.VBox, WidgetBaseMixin):

    __gtype_name__ = 'DialogOverlay'
    __extra_style_classes__ = ['DialogOverlay']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_context_add_class()
        return
