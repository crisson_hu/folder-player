SHELL = /bin/bash
LC_ALL = C
LANG = C
LANGUAGE = C

APP = folder-player
APP_PKG = ${APP}*.deb
BUILD_OUT_DIR = ..
BUILD_OUT_FILE = ${BUILD_OUT_DIR}/${APP_PKG}
STORE_DIR = .
STORE_PKG_DIR = ${STORE_DIR}/pkg
STORE_FILE = ${STORE_PKG_DIR}/${APP_PKG}

gen-locale:
	msgfmt src/locale/messages.po -o src/usr/lib/${APP}/locale/zh/LC_MESSAGES/${APP}.mo

plugins:
	cd src/ext/plugins/; make install

plugins-clean:
	cd src/ext/plugins/; make clean

install: gen-locale plugins
	mkdir -p debian/tmp/
	cp -r src/usr/ debian/tmp/

clean: plugins-clean
	find . -name __pycache__ -type d -exec rm -r {} \; -print -prune
	ls -lsF ${BUILD_OUT_DIR}/${APP}_*

check-code-style:
	@printf '[style-check]: start ....\n'
	@find . -name '*.py' -print | xargs tools/pycodestyle -v
	@tools/pycodestyle -v src/usr/lib/folder-player/folder-player
	@printf '[style-check]: \033[01;32m SUCCESS \033[00m\n'

build-deb-package:
	debuild --no-tgz-check -I -us -uc
	make copy-deb
	debclean

reinstall-deb:
	sudo dpkg -i ${BUILD_OUT_FILE}

local-run: check-code-style gen-locale plugins
	env LC_ALL=C.UTF-8 ./src/folder-player/folder-player -d

local-test:
	@cd src/ext/plugins/; make test -s

.PHONY: gen-locale plugins install plugins-clean clean check-code-style build-deb-package copy-deb reinstall-deb local-run local-test
